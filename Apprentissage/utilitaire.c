#include <SDL2/SDL.h>
#include <stdio.h>

#include "utilitaire.h"

/*--------------------------------------------------------------------------------------------------
Termine le programme et ferme la SDL.
Paramètres :    ok          -   indique si la fermeture est normale (1) ou résulte d'une erreur (0)
                msg         -   message à afficher si erreur
                window      -   fenêtre active
                renderer    -  renderer actif
--------------------------------------------------------------------------------------------------*/

void end_sdl(char ok,
            char const* msg,
            SDL_Window* window,
            SDL_Renderer* renderer)
{
  	char msg_formated[255];                                                         
  	int l;                                                                          

  	if (!ok)
	{                                                        												// Cas d'erreur
        strncpy(msg_formated, msg, 250);                                              
        l = strlen(msg_formated);                                                     
        strcpy(msg_formated + l, " : %s\n");                                          

        SDL_Log(msg_formated, SDL_GetError());                                        
  	}                                                                               

  	if (renderer != NULL)
	{                                           															// Destruction si nécessaire du renderer
    	SDL_DestroyRenderer(renderer);                     													// Attention : on suppose que les NULL sont maintenus !!
    	renderer = NULL;
  	}
  	if (window != NULL)  
	{                                           															// Destruction si nécessaire de la fenêtre
    	SDL_DestroyWindow(window);                          												// Attention : on suppose que les NULL sont maintenus !!
    	window= NULL;
  	}

    	SDL_Quit();                                                                     

  	if (!ok)
	{                                                        												// On quitte si cela ne va pas                
    	exit(EXIT_FAILURE);                                                           
  	}                                                                               
}




/*--------------------------------------------------------------------------------------------------
Créé la grille de cellules initiale.
Retourne la grille.
Paramètres :    wCell   -   largeur d'une cellule
                hCell   -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/

cellule** creerGrilleCuisine(int wCell,
                            int hCell)
    {
        cellule** grille = malloc(8*sizeof(cellule*));                                                      // La grille sera 8*8                            
        int i, j;
        for(i=0;i<8;i++)
        {
            grille[i] = malloc(8*sizeof(cellule));
            for(j=0;j<8;j++)
            {
                grille[i][j].rect.x = wCell * j;
                grille[i][j].rect.y = hCell * i;
                grille[i][j].rect.w = wCell;                                                                // Les cellules ont toutes la même taille
                grille[i][j].rect.h = hCell;
                
                grille[i][j].recompense = 0;                                                                // Initialement toutes les récompenses sont à 0

                grille[i][j].haut = 0;                                                                      // Au début, les espérances de récompense sont nulles
                grille[i][j].bas = 0;
                grille[i][j].gauche = 0;
                grille[i][j].droite = 0;
            }
        }

        grille[0][2].recompense = -1;                                                                       // Les meubles donnent une récompense de -1
        grille[0][3].recompense = -1;
        grille[0][4].recompense = -1;
        grille[0][5].recompense = -1;
        
        grille[2][0].recompense = -1;
        grille[2][1].recompense = -1;
        grille[3][1].recompense = -1;

        grille[2][7].recompense = -1;
        grille[3][7].recompense = -1;
        grille[4][7].recompense = -1;

        grille[3][4].recompense = -1;
        grille[4][3].recompense = -1;
        grille[4][4].recompense = -1;
        grille[5][3].recompense = -1;
        grille[5][4].recompense = -1;

        grille[6][0].recompense = -1;
        grille[6][1].recompense = -1;

        grille[6][7].recompense = -1;
        grille[7][6].recompense = -1;
        grille[7][7].recompense = -1;

        grille[7][4].recompense = 5;                                                                        // La sortie donne une récompense de 5


        return grille;
    }



/*--------------------------------------------------------------------------------------------------
Créé la grille de cellules initiale de la salle.
Retourne la grille.
Paramètres :    wCell   -   largeur d'une cellule
                hCell   -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/

cellule** creerGrilleSalle(int wCell,
                            int hCell)
{
    cellule** grille = malloc(8*sizeof(cellule*));                                                      // La grille sera 8*8                            
    int i, j;
    for(i=0;i<8;i++)
    {
        grille[i] = malloc(8*sizeof(cellule));
        for(j=0;j<8;j++)
        {
            grille[i][j].rect.x = wCell * j;
            grille[i][j].rect.y = hCell * i;
            grille[i][j].rect.w = wCell;                                                                // Les cellules ont toutes la même taille
            grille[i][j].rect.h = hCell;
            
            grille[i][j].recompense = 0;                                                                // Initialement toutes les récompenses sont à 0

            grille[i][j].haut = 0;                                                                      // Au début, les espérances de récompense sont nulles
            grille[i][j].bas = 0;
            grille[i][j].gauche = 0;
            grille[i][j].droite = 0;
        }
    }

    grille[0][0].recompense = -1;                                                                       // Les meubles donnent une récompense de -1
    grille[0][1].recompense = -1;
    grille[0][2].recompense = -1;
    grille[0][3].recompense = -1;
    grille[1][0].recompense = -1;
    grille[1][1].recompense = -1;
    grille[1][2].recompense = -1;
    grille[1][3].recompense = -1;
    
    grille[4][1].recompense = -1;
    grille[4][2].recompense = -1;
    grille[5][1].recompense = -1;
    grille[5][2].recompense = -1;
    grille[6][1].recompense = -1;
    grille[6][2].recompense = -1;
    grille[7][1].recompense = -1;
    grille[7][2].recompense = -1;

    grille[2][5].recompense = -1;
    grille[3][5].recompense = -1;
    grille[4][5].recompense = -1;
    grille[5][5].recompense = -1;

    grille[7][4].recompense = -1;
    grille[7][5].recompense = -1;
    grille[7][6].recompense = -1;
    grille[7][7].recompense = -1;

    grille[0][4].recompense = 5;                                                                        // La sortie donne une récompense de 5


    return grille;
}
#ifndef RENFORCEMENT_H
#define RENFORCEMENT_H

#include <SDL2/SDL.h>
#include "utilitaire.h"

#define LONGMAX 50000

typedef struct enregistrement
{
    SDL_Rect etats[LONGMAX];
    double recompenses[LONGMAX];
    int actions[LONGMAX];
    int curseur;
} enregistrement;

int fini(cellule**, SDL_Rect, int, int);
double meilleureRecompense(cellule**, SDL_Rect*, int, int);
short int meilleureAction(cellule**, SDL_Rect*, int, int);
int nextStep(cellule**, enregistrement*, SDL_Rect*, int, int, double);
void spawn (SDL_Renderer*, cellule**, SDL_Rect*, SDL_Rect *, SDL_Rect*, SDL_Texture*, int, int, int);
void spawnSalle (cellule**, SDL_Rect*, int);
void apprentissageCuisine(SDL_Renderer*, cellule**, SDL_Rect, SDL_Rect*, SDL_Rect*, SDL_Rect*, SDL_Rect*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, int*, int, int, int, Mix_Music*, Mix_Music*, Mix_Music*, SDL_bool*);
void apprentissageSalle(SDL_Renderer*, cellule**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect*, SDL_Rect*, SDL_Rect*, SDL_Rect *, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, int*, int, int, int, int, Mix_Music*, Mix_Music*, Mix_Music*, SDL_bool*);
void majRecompenses(cellule** , enregistrement* , int, int);

#endif
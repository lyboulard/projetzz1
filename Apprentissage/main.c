/*-----------------------------------------------------------------------------
Projet de fin d'année - ZZ1 - Isima
BOULARD Lyloo - MARCELET Paul - MUNGONGO Arien

-The TeleChubbies-

Création d'un jeu utilisant l'apprentissage par renforcement
Tinky-Winky doit ramener sa commande au client en trouvant un chemin depuis la
cuisine jusqu'à la salle du restaurant, tout en évitant les obstacles.
L'utilisateur peut choisir l'aliment commandé, ce qui demandera à Tinky-Winky
d'emprunter des chemins différents

-----------------------------------------------------------------------------*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include <time.h>

#include "utilitaire.h"
#include "aside.h"
#include "renforcement.h"
#include "affichage.h"

int main(int argc, char** argv)
{
    srand(time(NULL));
    
    (void)argc;
    (void)argv;

    int wwindow, hwindow;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);             // Initialisation de la SDL
    if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);


  	SDL_DisplayMode screen;
	SDL_GetCurrentDisplayMode(0, &screen);


  	/* ---------------Création de la fenêtre--------------- */

  	window = SDL_CreateWindow("TeleChubbies",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 0.9*screen.h - ( (int) 0.9*screen.h % 8),
                            0.9*screen.h - ( (int) 0.9*screen.h % 8),
                            SDL_WINDOW_OPENGL);
  	if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    SDL_GetWindowSize(window, &wwindow, &hwindow);


  	/* ---------------Création du renderer--------------- */

  	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  	if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    SDL_SetRenderDrawColor(renderer, 240, 240, 240, 255);
    SDL_RenderClear(renderer);


    /* ---------------Chargement des musiques--------------- */

    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);

    Mix_Music* ambiance = Mix_LoadMUS("./data/musique.mp3");
    Mix_Music* boop = Mix_LoadMUS("./data/choixCommande.mp3");
    Mix_Music* verre = Mix_LoadMUS("./data/verre.mp3");
    Mix_Music* assiette = Mix_LoadMUS("./data/assiette.mp3");

    
    /* ---------------Découpage des sprites--------------- */
    
    /* Sprites du perso Tinky */
    SDL_Rect Tinky[12];
   
    //Animation Descend
    Tinky[0].x = 130;
    Tinky[0].y = 0;
    Tinky[0].w = 59;
    Tinky[0].h = 67;
    
    Tinky[1].x = 67;
    Tinky[1].y = 1;
    Tinky[1].w = 57;
    Tinky[1].h = 67;

    Tinky[2].x = 195;
    Tinky[2].y = 1;
    Tinky[2].w = 57;
    Tinky[2].h = 67;

    //Animation gauche
    Tinky[3].x = 140;
    Tinky[3].y = 67;
    Tinky[3].w = 33;
    Tinky[3].h = 38;
    
    Tinky[4].x = 76;
    Tinky[4].y = 68;
    Tinky[4].w = 38;
    Tinky[4].h = 67;

    Tinky[5].x = 204;
    Tinky[5].y = 68;
    Tinky[5].w = 37;
    Tinky[5].h = 66;

    //Animation droite
    Tinky[6].x = 145;
    Tinky[6].y = 135;
    Tinky[6].w = 34;
    Tinky[6].h = 68;
    
    Tinky[7].x = 77;
    Tinky[7].y = 136;
    Tinky[7].w = 38;
    Tinky[7].h = 67;

    Tinky[8].x = 206;
    Tinky[8].y = 136;
    Tinky[8].w = 37;
    Tinky[8].h = 66;

    //Animation Monte
    Tinky[9].x = 130;
    Tinky[9].y = 203;
    Tinky[9].w = 59;
    Tinky[9].h = 67;
    
    Tinky[10].x = 67;
    Tinky[10].y = 204;
    Tinky[10].w = 57;
    Tinky[10].h = 67;

    Tinky[11].x = 195;
    Tinky[11].y = 203;
    Tinky[11].w = 57;
    Tinky[11].h = 67;
    


    /* Sprite des aliments */
    SDL_Rect aliments[17];

    //Vin
    aliments[0].x = 586;
    aliments[0].y = 131;
    aliments[0].w = 11;
    aliments[0].h = 24;

    //Bière
    aliments[1].x = 573;
    aliments[1].y = 266;
    aliments[1].w = 18;
    aliments[1].h = 19;

    //Cocktail
    aliments[2].x = 521;
    aliments[2].y = 321;
    aliments[2].w = 17;
    aliments[2].h = 15;

    //Café
    aliments[3].x = 573;
    aliments[3].y = 371;
    aliments[3].w = 19;
    aliments[3].h = 18;

    //Sushi
    aliments[4].x = 53;
    aliments[4].y = 404;
    aliments[4].w = 20;
    aliments[4].h = 13;
    
    //Frites
    aliments[5].x = 78;
    aliments[5].y = 315;
    aliments[5].w = 15;
    aliments[5].h = 21;

    //Poulet
    aliments[6].x = 183;
    aliments[6].y = 319;
    aliments[6].w = 24;
    aliments[6].h = 18;

    //Hamburger
    aliments[7].x = 79;
    aliments[7].y = 344;
    aliments[7].w = 24;
    aliments[7].h = 19;

    //Langouste
    aliments[8].x = 104;
    aliments[8].y = 366;
    aliments[8].w = 24;
    aliments[8].h = 23;

    //Gateau
    aliments[9].x = 81;
    aliments[9].y = 551;
    aliments[9].w = 20;
    aliments[9].h = 18;

    //Croissant
    aliments[10].x = 80;
    aliments[10].y = 580;
    aliments[10].w = 20;
    aliments[10].h = 13;

    //Glace
    aliments[11].x = 54;
    aliments[11].y = 599;
    aliments[11].w = 21;
    aliments[11].h = 12;

    //Banane
    aliments[12].x = 495;
    aliments[12].y = 580;
    aliments[12].w = 21;
    aliments[12].h = 13;

    //Orange
    aliments[13].x = 391;
    aliments[13].y = 477;
    aliments[13].w = 16;
    aliments[13].h = 16;

    //Framboise
    aliments[14].x = 521;
    aliments[14].y = 478;
    aliments[14].w = 14;
    aliments[14].h = 15;

    //Brocoli
    aliments[15].x = 287;
    aliments[15].y = 367;
    aliments[15].w = 24;
    aliments[15].h = 22;

    //Aubergine
    aliments[16].x = 410;
    aliments[16].y = 400;
    aliments[16].w = 14;
    aliments[16].h = 14;



    /* Sprites Ustensiles */
    SDL_Rect ustensiles[2];

    //Rouleaux
    ustensiles[0].x = 265;
    ustensiles[0].y = 657;
    ustensiles[0].w = 14;
    ustensiles[0].h = 15;
    
    //Planche de découpage
    ustensiles[1].x = 238;
    ustensiles[1].y = 631;
    ustensiles[1].w = 17;
    ustensiles[1].h = 14;



    /* Sprites meubles salle*/
    SDL_Rect meublesSalle[20];

    //Table verticale
    meublesSalle[0].x = 82;
    meublesSalle[0].y = 404;
    meublesSalle[0].w = 29;
    meublesSalle[0].h = 76;

    //Table horizontale
    meublesSalle[1].x = 0;
    meublesSalle[1].y = 404;
    meublesSalle[1].w = 80;
    meublesSalle[1].h = 44;

    //Chaise haut
    meublesSalle[2].x = 224;
    meublesSalle[2].y = 2;
    meublesSalle[2].w = 16;
    meublesSalle[2].h = 30;
    
    //Chaise bas
    meublesSalle[3].x = 192;
    meublesSalle[3].y = 2;
    meublesSalle[3].w = 16;
    meublesSalle[3].h = 30;

    //Chaise gauche
    meublesSalle[4].x = 209;
    meublesSalle[4].y = 65;
    meublesSalle[4].w = 16;
    meublesSalle[4].h = 30;

    //Chaise droite
    meublesSalle[5].x = 209;
    meublesSalle[5].y = 2;
    meublesSalle[5].w = 16;
    meublesSalle[5].h = 30;

    //Plante
    meublesSalle[6].x = 0;
    meublesSalle[6].y = 687;
    meublesSalle[6].w = 15;
    meublesSalle[6].h = 33;

    //Tapis
    meublesSalle[7].x = 223;
    meublesSalle[7].y = 1392;
    meublesSalle[7].w = 65;
    meublesSalle[7].h = 47;


    


    /* Tuiles au sol */
    SDL_Rect blocs[2];

    //Tuile cuisine
    blocs[0].x = 2019;                                                                                                     // Une tuile au sol
    blocs[0].y = 690;
    blocs[0].w = 89;
    blocs[0].h = 89;

    //Tuile salle
    blocs[1].x = 1753;
    blocs[1].y = 824;
    blocs[1].w = 89;
    blocs[1].h = 89;



    /* Sprites sélection tables */
    SDL_Rect selectTables[4];

    //Table 1
    selectTables[0].x = 375;
    selectTables[0].y = 103;
    selectTables[0].w = 155;
    selectTables[0].h = 112;

    //Table 2
    selectTables[1].x = 375;
    selectTables[1].y = 340;
    selectTables[1].w = 155;
    selectTables[1].h = 112;

    //Table 3
    selectTables[2].x = 375;
    selectTables[2].y = 572;
    selectTables[2].w = 155;
    selectTables[2].h = 112;

    //Table 4
    selectTables[3].x = 375;
    selectTables[3].y = 803;
    selectTables[3].w = 155;
    selectTables[3].h = 112;


    /* Sprites piano, pianiste et main*/

    SDL_Rect piano, pianiste, main;

    piano.x = 0;
    piano.y = 0;
    piano.w = 85;
    piano.h = 48;

    pianiste.x = 1150;
    pianiste.y = 425;
    pianiste.w = 425;
    pianiste.h = 525;

    main.x = 0;
    main.y = 0;
    main.w = 70;
    main.h = 60;


    /* ---------------Chargement des textures--------------- */

    SDL_Texture* textureNotepad = IMG_LoadTexture(renderer, "./data/notepad.png");
    if(textureNotepad == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture *textureMenu = IMG_LoadTexture(renderer, "./data/menu.png");
    if (textureMenu == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture* texturePlancher=IMG_LoadTexture(renderer, "./data/carrelage.png");
    if (texturePlancher == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture *textureMeuble1 = IMG_LoadTexture(renderer, "./data/placard_hori.png");
    if (textureMeuble1 == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture *textureMeuble2 = IMG_LoadTexture(renderer, "./data/evier.png");
    if (textureMeuble2 == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture *textureMeuble3 = IMG_LoadTexture(renderer, "./data/table_placard_angle.png");
    if (textureMeuble3 == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture *textureMeuble4 = IMG_LoadTexture(renderer, "./data/table_angle.png");
    if (textureMeuble4 == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture *textureMeuble5 = IMG_LoadTexture(renderer, "./data/table.png");
    if (textureMeuble5 == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture *textureMeuble6 = IMG_LoadTexture(renderer, "./data/table_6.png");
    if (textureMeuble6 == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture *textureTinky = IMG_LoadTexture(renderer, "./data/PabloTinky.png");
    if (textureTinky == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture *textureAliments = IMG_LoadTexture(renderer, "./data/food.png");
    if (textureAliments == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture *textureBulle = IMG_LoadTexture(renderer, "./data/bubble2.png");
    if (textureBulle == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture* textureFurniture = IMG_LoadTexture(renderer, "./data/furnitureSalle.png");
    if (textureFurniture == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture* textureSelectTables = IMG_LoadTexture(renderer, "./data/selectTables.png");
    if (textureSelectTables == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture* texturePiano = IMG_LoadTexture(renderer, "./data/piano.png");
    if (texturePiano == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture* texturePianiste = IMG_LoadTexture(renderer, "./data/pianiste.png");
    if (texturePianiste == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Texture* textureMain = IMG_LoadTexture(renderer, "./data/main.png");
    if (textureMain == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }



    

    int wCell = wwindow/8;                                                                                          // Largeur et hauteur d'une cellule
    int hCell = hwindow/8;                                                                                          // La grille sera 8*8
    
    SDL_Rect serveur;
    serveur.w = wCell; serveur.h = hCell;

    
    
    
    cellule** grilleCuisine = creerGrilleCuisine(wCell, hCell);                                                     // Création de l'environnement de cuisine
    cellule** grilleSalle = creerGrilleSalle(wCell, hCell);                                                         // Création de l'environnement de salle
   
    menu(window, renderer, textureMenu, wwindow, hwindow);

    int indiceAliment = 0;
    int indiceTable = 0;
    int score = 0;
    SDL_bool bool_music = SDL_FALSE;

    while(indiceAliment != -1)
    {
        commande(window, renderer,
                aliments, selectTables,
                textureAliments, textureNotepad, textureMenu, textureSelectTables,
                &indiceAliment, &indiceTable,
                wwindow, hwindow, boop);
        
        if(indiceAliment != -1)
        {
            apprentissageCuisine(renderer, grilleCuisine,
                                serveur, blocs, Tinky, aliments, ustensiles,
                                texturePlancher, textureMeuble1, textureMeuble2, textureMeuble3, textureMeuble4, textureMeuble5, textureMeuble6,
                                textureAliments, textureBulle, textureTinky,
                                &score, indiceAliment, wCell, hCell,
                                verre, assiette, ambiance, &bool_music);
    
            apprentissageSalle(renderer, grilleSalle,
                                serveur, piano, pianiste, main, blocs, Tinky, meublesSalle, aliments,
                                texturePlancher, textureFurniture, textureTinky, textureAliments, texturePiano, texturePianiste, textureMain,
                                &score, indiceAliment, indiceTable, wCell, hCell,
                                verre, assiette, ambiance, &bool_music);
        }
    }



    SDL_DestroyTexture(textureMeuble1);
    SDL_DestroyTexture(textureMeuble2);
    SDL_DestroyTexture(textureMeuble3);
    SDL_DestroyTexture(textureMeuble4);
    SDL_DestroyTexture(textureMeuble5);
    SDL_DestroyTexture(textureMeuble6);
    SDL_DestroyTexture(textureAliments);
    SDL_DestroyTexture(textureBulle);
    SDL_DestroyTexture(textureTinky);
    SDL_DestroyTexture(textureNotepad);
    SDL_DestroyTexture(textureSelectTables);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();


    return EXIT_SUCCESS;

}
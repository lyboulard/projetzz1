#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>

#include "aside.h"
#include "utilitaire.h"

/*--------------------------------------------------------------------------------------------------
Affiche l'écran de commande du plat.
Retourne l'entier associé à l'aliment choisi.
Paramètres :    window              -   fenêtre active
                renderer            -   renderer actif
                aliments            -   tableau contenant les SDL_Rect indiquant les dimensions et
                                    emplacements des différents aliments dans leur texture
                selectTables        -   contient les emplacements des sprites de sélection de table
                textureAliments     -   contient les sprites des aliments
                textureNotepad      -   contient le sprite du notepad
                textureMenu         -   contient le sprite du menu
                textureSelectTables -   contient les sprites de sélection de table
                indiceAliment       -   indice de l'aliment choisi
                indiceTable         -   indice de la table choisie
                wwindow             -   largeur de la fenêtre
                hwindow             -   hauteur de la fenêtre
                boop                -   bruitage de selection de la commande
--------------------------------------------------------------------------------------------------*/

void commande(SDL_Window* window,
            SDL_Renderer* renderer,
            SDL_Rect* aliments,
            SDL_Rect* selectTables,
            SDL_Texture* textureAliments,
            SDL_Texture* textureNotepad,
            SDL_Texture* textureMenu,
            SDL_Texture* textureSelectTables,
            int* indiceAliment,
            int* indiceTable,
            int wwindow,
            int hwindow,
            Mix_Music* boop)
{
    SDL_Rect posTexte, posTexte2, rectNotepad, rectMenu, zoneWindowMenu, zoneWindowNotepad, zoneWindowAliment, zoneWindowTables;

    TTF_Font* font = TTF_OpenFont("./data/Airthan-Age.ttf", 100);
    if (font == NULL) {end_sdl(0, "Can't load font", window, renderer);}
    SDL_Color color = {253, 108, 168, 255};

    SDL_RenderClear(renderer);

    // ---------------Placement du background--------------- 

    rectMenu.x = 0; rectMenu.y = 0;                                                                         // Sélection dans la texture
    rectMenu.w = 1000; rectMenu.h = 1000;

    zoneWindowMenu.x = 0; zoneWindowMenu.y = 0;                                                             // Positionnement
    zoneWindowMenu.w = wwindow; zoneWindowMenu.h = hwindow;
    SDL_RenderCopy(renderer, textureMenu, &rectMenu, &zoneWindowMenu);
    
    // ---------------Placement du carnet de notes--------------- 

    rectNotepad.x = 0; rectNotepad.y = 0;                                                                   // Sélection dans la texture
    rectNotepad.w = 4961; rectNotepad.h = 7016;

    zoneWindowNotepad.x = 0.25*wwindow; zoneWindowNotepad.y = 0.05*hwindow;                                 // Positionnement
    zoneWindowNotepad.w = 0.5*wwindow; zoneWindowNotepad.h = 0.9*hwindow;
    SDL_RenderCopy(renderer, textureNotepad, &rectNotepad, &zoneWindowNotepad);
    
    // ---------------Texte 1--------------- 

    SDL_Surface* texte = TTF_RenderText_Blended(font, "Commande :", color);
    if (texte == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, texte);                              // Transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    
    SDL_QueryTexture(text_texture, NULL, NULL, &posTexte.w, &posTexte.h);                                   // Choix de la position du texte
    posTexte.x = 0.5*(wwindow - posTexte.w); posTexte.y = 0.17*hwindow;

    SDL_RenderCopy(renderer, text_texture, NULL, &posTexte);                                                // On applique au renderer


    // ---------------Texte 2--------------- 

    SDL_Surface* texte2 = TTF_RenderText_Blended(font, "Quitter", color);
    if (texte2 == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text2_texture = SDL_CreateTextureFromSurface(renderer, texte2);                              // Transfert de la surface à la texture
    if (text2_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    
    SDL_QueryTexture(text2_texture, NULL, NULL, &posTexte2.w, &posTexte2.h);                                   // Choix de la position du texte
    posTexte2.x = 0.5*(wwindow - posTexte2.w); posTexte2.y = 0.83*hwindow;

    SDL_RenderCopy(renderer, text2_texture, NULL, &posTexte2);      


    // ---------------Aliment---------------

    zoneWindowAliment.w = 5*aliments[*indiceAliment].w; zoneWindowAliment.h = 5*aliments[*indiceAliment].h;   // On place l'aliment actuellement selectionné
    zoneWindowAliment.x = 0.5*(wwindow - zoneWindowAliment.w); zoneWindowAliment.y = 0.35*hwindow;
    SDL_RenderCopy(renderer, textureAliments, &aliments[*indiceAliment], &zoneWindowAliment);

    // ---------------Table---------------

    zoneWindowTables.w = selectTables[*indiceTable].w; zoneWindowTables.h = selectTables[*indiceTable].h;   // On place l'aliment actuellement selectionné
    zoneWindowTables.x = 0.5*(wwindow - zoneWindowTables.w); zoneWindowTables.y = 0.65*hwindow;
    SDL_RenderCopy(renderer, textureSelectTables, &selectTables[*indiceTable], &zoneWindowTables);



    SDL_RenderPresent(renderer);
    

    SDL_Event event;
    SDL_bool run = SDL_TRUE;
    while (run)
    {
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT :
                    run = SDL_FALSE;
                break;

                case SDL_KEYDOWN :
                    switch(event.key.keysym.sym)
                    {    
                        case 13 :                                                                           // Touche entrée
                            run = SDL_FALSE;
                        break;

                        default:
                        break;
                    }
                break;

                case SDL_MOUSEBUTTONDOWN:
                    if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
                    {
                        if(event.motion.x > zoneWindowAliment.x &&                                          // Clic sur l'aliment
                            event.motion.x < zoneWindowAliment.x + zoneWindowAliment.w &&
                            event.motion.y > zoneWindowAliment.y &&
                            event.motion.y < zoneWindowAliment.y + zoneWindowAliment.h)
                        {
                            *indiceAliment = (*indiceAliment+1)%17;                                         // L'aliment suivant est à présent sélectionné
                            Mix_PlayMusic(boop, 0);                                                         // Bruitage

                            zoneWindowAliment.w = 5*aliments[*indiceAliment].w;                             // Réajustement de la nouvelle position et des nouvelles dimensions
                            zoneWindowAliment.h = 5*aliments[*indiceAliment].h;
                            zoneWindowAliment.x = 0.5*(wwindow - zoneWindowAliment.w);
                            zoneWindowAliment.y = 0.35*hwindow;

                            SDL_RenderClear(renderer);
                            SDL_RenderCopy(renderer, textureMenu, &rectMenu, &zoneWindowMenu);
                            SDL_RenderCopy(renderer, textureNotepad, &rectNotepad, &zoneWindowNotepad);
                            SDL_RenderCopy(renderer, text_texture, NULL, &posTexte);
                            SDL_RenderCopy(renderer, text2_texture, NULL, &posTexte2);
                            SDL_RenderCopy(renderer, textureAliments, &aliments[*indiceAliment], &zoneWindowAliment);
                            SDL_RenderCopy(renderer, textureSelectTables, &selectTables[*indiceTable], &zoneWindowTables);

                            SDL_RenderPresent(renderer);
                        }


                        if(event.motion.x > zoneWindowTables.x &&                                          // Clic sur la table
                            event.motion.x < zoneWindowTables.x + zoneWindowTables.w &&
                            event.motion.y > zoneWindowTables.y &&
                            event.motion.y < zoneWindowTables.y + zoneWindowTables.h)
                        {
                            *indiceTable = (*indiceTable+1)%4;                                              // La table suivante est à présent sélectionnée
                            Mix_PlayMusic(boop, 0);                                                         // Bruitage

                            zoneWindowTables.w = selectTables[*indiceTable].w;                              // Réajustement de la nouvelle position et des nouvelles dimensions
                            zoneWindowTables.h = selectTables[*indiceTable].h;
                            zoneWindowTables.x = 0.5*(wwindow - zoneWindowTables.w);
                            zoneWindowTables.y = 0.65*hwindow;

                            SDL_RenderClear(renderer);
                            SDL_RenderCopy(renderer, textureMenu, &rectMenu, &zoneWindowMenu);
                            SDL_RenderCopy(renderer, textureNotepad, &rectNotepad, &zoneWindowNotepad);
                            SDL_RenderCopy(renderer, text_texture, NULL, &posTexte);
                            SDL_RenderCopy(renderer, text2_texture, NULL, &posTexte2);
                            SDL_RenderCopy(renderer, textureAliments, &aliments[*indiceAliment], &zoneWindowAliment);
                            SDL_RenderCopy(renderer, textureSelectTables, &selectTables[*indiceTable], &zoneWindowTables);

                            SDL_RenderPresent(renderer);
                        }


                        if(event.motion.x > posTexte2.x &&                                                  // Clic sur Quitter
                            event.motion.x < posTexte2.x + posTexte2.w &&
                            event.motion.y > posTexte2.y &&
                            event.motion.y < posTexte2.y + posTexte2.h)
                        {
                            *indiceAliment = -1;
                            run = SDL_FALSE;
                        }

                        
                    }
                break;

                default:
                break;
            }
        }

    }

    SDL_FreeSurface(texte2);
    SDL_FreeSurface(texte);
    SDL_DestroyTexture(text_texture);
    SDL_DestroyTexture(text2_texture);
    TTF_CloseFont(font);
}




/*--------------------------------------------------------------------------------------------------
Affiche l'écran d'accueil du jeu.
Paramètres :    window      -   fenêtre active
                renderer    -   renderer actif
                textureMenu -   contient le sprite du menu
                wwindow     -   largeur de la fenêtre
                hwindow     -   hauteur de la fenêtre
--------------------------------------------------------------------------------------------------*/

void menu(SDL_Window* window,
        SDL_Renderer* renderer,
        SDL_Texture* textureMenu,
        int wwindow,
        int hwindow)
{
    SDL_Rect pos, rectMenu, zoneWindowMenu;

    TTF_Font* font = TTF_OpenFont("./data/Airthan-Age.ttf", 100);
    if (font == NULL) {end_sdl(0, "Can't load font", window, renderer);}
    SDL_Color color = {253, 108, 168, 255};


    // ---------------Placement du background--------------- 

    rectMenu.x = 0; rectMenu.y = 0;                                                                   // Sélection dans la texture
    rectMenu.w = 1000; rectMenu.h = 1000;

    zoneWindowMenu.x = 0; zoneWindowMenu.y = 0;                                 // Positionnement
    zoneWindowMenu.w = wwindow; zoneWindowMenu.h = hwindow;
    SDL_RenderCopy(renderer, textureMenu, &rectMenu, &zoneWindowMenu);
    
    // ---------------1er texte--------------- 
    SDL_Surface* texte = TTF_RenderText_Blended(font, "Bienvenue au TeleChubbies !", color);
    if (texte == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, texte);                              // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);                                             // Choix de la position du texte
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.2*hwindow;

    SDL_RenderCopy(renderer, text_texture, NULL, &pos);                                                     // On applique au renderer

    //---------------2e texte--------------- 
    texte = TTF_RenderText_Blended(font, "Attention : notre serveur est tout nouveau ici", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.35*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);

    //---------------3e texte--------------- 
    texte = TTF_RenderText_Blended(font, "et risque de se perdre un peu dans la cuisine...", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.5*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);

    //---------------4e texte--------------- 
    texte = TTF_RenderText_Blended(font, "Nous nous excusons pour l'attente.", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.65*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);

    //---------------5e texte--------------- 
    texte = TTF_RenderText_Blended(font, "Bon appetit !", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.8*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);


    SDL_RenderPresent(renderer);


    SDL_bool run = SDL_TRUE;
    while (run)
    {
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT :
                    run = SDL_FALSE;
                break;

                case SDL_KEYDOWN :
                    switch(event.key.keysym.sym)
                    {    
                        case 13 :                                                                           // Touche entrée
                            run = SDL_FALSE;
                        break;
                    }
                break;
            }
        }
    }

    SDL_RenderClear(renderer);
    SDL_FreeSurface(texte);
    SDL_DestroyTexture(text_texture);
}
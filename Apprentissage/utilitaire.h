#ifndef UTILITAIRE_H
#define UTILITAIRE_H

#define MAX(x, y) ((x>y)? x:y)
#define MIN(x, y) ((x<y)? x:y)

typedef struct cellule
{
    SDL_Rect rect;                                                      // Contient les dimensions et emplacement de la cellule
    
    int recompense;                                                     // Récompense de la cellule

    double haut;                                                        // Espérance des récompenses dans toutes les directions
    double bas;
    double gauche;
    double droite;
} cellule;

enum action {HAUT, BAS, GAUCHE, DROITE};                                // Le personnage peut se déplacer selon les 4 directions

void end_sdl(char, char const*, SDL_Window*, SDL_Renderer*);
cellule** creerGrilleCuisine(int, int);
cellule** creerGrilleSalle(int, int);

#endif
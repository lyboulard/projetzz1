#include <stdio.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include "renforcement.h"
#include "utilitaire.h"
#include "affichage.h"

/*--------------------------------------------------------------------------------------------------
Détermine si le personnage a terminé son parcours.
Retourne 1 si oui, 0 si non.
Paramètres :    grille      -   grille représentant l'environnement
                serveur     -   contient les dimensions et emplacement du personnage
                wCell       -   largeur d'une cellule
                hCell       -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/

int fini(cellule** grille,
        SDL_Rect serveur,
        int wCell,
        int hCell)
{
    return grille[serveur.y / hCell][serveur.x / wCell].recompense == 5;
}


/*--------------------------------------------------------------------------------------------------
Trouve la meilleure espérance de récompense à partir d'un état
Retourne l'espérance trouvée
Paramètres :    grille      -   grille représentant l'environnement
                serveur     -   contient les dimensions et emplacement du personnage
                wCell       -   largeur d'une cellule
                hCell       -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/
double meilleureRecompense(cellule** grille,
                        SDL_Rect* serveur,
                        int wCell,
                        int hCell)
{
    double max = grille[serveur->y / hCell][serveur->x / wCell].haut;                                       // serveur->y / hCell convertit une position en indice

    if(max < grille[serveur->y / hCell][serveur->x / wCell].bas)
    {
        max = grille[serveur->y / hCell][serveur->x / wCell].bas;
    }
    if(max < grille[serveur->y / hCell][serveur->x / wCell].gauche)
    {
        max = grille[serveur->y / hCell][serveur->x / wCell].gauche;
    }
    if(max < grille[serveur->y / hCell][serveur->x / wCell].droite)
    {
        max = grille[serveur->y / hCell][serveur->x / wCell].droite;
    }

    return max;
}




/*--------------------------------------------------------------------------------------------------
Détermine la meilleure action à partir d'un état.
Retourne l'entier représentant l'action.
Paramètres :    grille      -   grille représentant l'environnement
                serveur     -   contient les dimensions et emplacement du personnage
                wCell       -   largeur d'une cellule
                hCell       -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/
short int meilleureAction(cellule** grille,
                        SDL_Rect* serveur,
                        int wCell,
                        int hCell)
{
    double max = meilleureRecompense(grille, serveur, wCell, hCell);                                        // La meilleure action est celle qui correspond à la meilleure récompense
    
    if(max == grille[serveur->y / hCell][serveur->x / wCell].haut)
    {
        return HAUT;
    }
    if(max == grille[serveur->y / hCell][serveur->x / wCell].bas)
    {
        return BAS;
    }
    if(max == grille[serveur->y / hCell][serveur->x / wCell].gauche)
    {
        return GAUCHE;
    }
    return DROITE;
}




/*--------------------------------------------------------------------------------------------------
Détermine l'état suivant et s'y rend.
Paramètres :    grille      -   grille représentant l'environnement
                enr         -   contient l'enregistrement de la run
                serveur     -   contient les dimensions et emplacement du personnage
                wCell       -   largeur d'une cellule
                hCell       -   hauteur d'une cellule
                eps         -   détermine la part d'exploration/exploitation
--------------------------------------------------------------------------------------------------*/

int nextStep(cellule** grille,
            enregistrement* enr,
            SDL_Rect* serveur,
            int wCell,
            int hCell,
            double eps)
{
    short int action;
    if(rand()%100 < eps)                                                                                    // Exploration
    {
        action = rand()%4;
    }
    else                                                                                                    // Exploitation
    {
        action = meilleureAction(grille, serveur, wCell, hCell);
    }

    int serveurOldi = serveur->y / hCell;                                                                   // Anciennes coordonnés du personnage
    int serveurOldj = serveur->x / wCell;

    enr->etats[enr->curseur].x = serveur->x; enr->etats[enr->curseur].y = serveur->y; // Enregistrement des états


    if(action == HAUT)
    {
        serveur->y = grille[MAX(serveurOldi-1, 0)][serveurOldj].rect.y;
        enr->recompenses[enr->curseur] = grille[serveur->y / hCell][serveurOldj].recompense;                                             // Récompense de l'état d'arrivée
    }
    if(action == BAS)
    {
        serveur->y = grille[MIN(serveurOldi+1, 7)][serveurOldj].rect.y;
        enr->recompenses[enr->curseur] = grille[serveur->y / hCell][serveurOldj].recompense;
    }
    if(action == GAUCHE)
    {
        serveur->x = grille[serveurOldi][MAX(serveurOldj-1, 0)].rect.x;
        enr->recompenses[enr->curseur] = grille[serveurOldi][serveur->x / wCell].recompense;
    }
    if(action == DROITE)
    {
        serveur->x = grille[serveurOldi][MIN(serveurOldj+1, 7)].rect.x;
        enr->recompenses[enr->curseur] = grille[serveurOldi][serveur->x / wCell].recompense;
    }

    enr->actions[enr->curseur] = action; // Enregistrement des actions

    return action;
}




/*--------------------------------------------------------------------------------------------------
Fait apparaître Pablo Tinky au bon endroit de la cuisine.
Paramètres :    renderer        -   SDL_Renderer actif
                grilleCuisine   -   grille représentant l'environnement
                aliments        -   contient les dimensions et emplacements des aliments
                ustensiles      -   contient les dimensions et emplacements des ustensiles
                serveur         -   contient les dimensions et emplacement du personnage
                textureAliments -   contient les aliments
                wCell           -   largeur d'une cellule
                hCell           -   hauteur d'une cellule
                indiceAlim      -   indice de l'aliment choisi
--------------------------------------------------------------------------------------------------*/

void spawn (SDL_Renderer* renderer,
            cellule** grille,
            SDL_Rect* aliments,
            SDL_Rect* ustensiles,
            SDL_Rect * serveur, 
            SDL_Texture* textureAliments,
            int wCell,
            int hCell,
            int indiceAlim)
{
    SDL_Rect * grilleItem = afficheAliments(renderer, grille, aliments, ustensiles, textureAliments, wCell, hCell);
    
    if (indiceAlim <= 3 || indiceAlim == 9 || indiceAlim == 11) //Spawn en bas
    {
        serveur->x = grilleItem[indiceAlim].x - 0.25*wCell;
        serveur->y = grilleItem[indiceAlim].y - 0.25*hCell + hCell;
    }
    
    else if(indiceAlim == 4 || indiceAlim == 6 || indiceAlim == 8 || indiceAlim == 10) //Spawn à droite
    {
        serveur->x = grilleItem[indiceAlim].x - 0.25*wCell + wCell;
        serveur->y = grilleItem[indiceAlim].y - 0.25*hCell;
    }

    else if(indiceAlim == 5 || indiceAlim == 7 || (indiceAlim >= 12 && indiceAlim <= 14)) //Spawn à gauche
    {
        serveur->x = grilleItem[indiceAlim].x - 0.25*wCell - wCell;
        serveur->y = grilleItem[indiceAlim].y - 0.25*hCell;
    }

    else if(indiceAlim == 15 || indiceAlim == 16) //Spawn en haut
    {
        serveur->x = grilleItem[indiceAlim].x - 0.25*wCell;
        serveur->y = grilleItem[indiceAlim].y - 0.25*hCell - hCell;
    }
    
}




/*--------------------------------------------------------------------------------------------------
Fait apparaître Pablo Tinky au bon endroit de la salle.
Paramètres :    grilleSalle     -   grille représentant l'environnement
                serveur         -   contient les dimensions et emplacement du personnage
                indiceTable     -   indice de la table choisie
--------------------------------------------------------------------------------------------------*/

void spawnSalle (cellule** grilleSalle,
                SDL_Rect * serveur, 
                int indiceTable)
{    
    if (indiceTable == 0) //Spawn en bas
    {
        serveur->x = grilleSalle[2][2].rect.x;
        serveur->y = grilleSalle[2][2].rect.y;
    }
    
    else if(indiceTable == 1) //Spawn à droite
    {
        serveur->x = grilleSalle[5][3].rect.x;
        serveur->y = grilleSalle[5][3].rect.y;
    }

    else if(indiceTable == 2) //Spawn à gauche
    {
        serveur->x = grilleSalle[3][4].rect.x;
        serveur->y = grilleSalle[3][4].rect.y;
    }

    else if(indiceTable == 3) //Spawn en haut
    {
        serveur->x = grilleSalle[6][5].rect.x;
        serveur->y = grilleSalle[6][5].rect.y;
    }
    
}




/*--------------------------------------------------------------------------------------------------
Met à jour les espérances des états.
Paramètres :    grille  -   grille représentant l'environnement
                enr     -   contient l'enregistrement de la run
                wCell   -   largeur d'une cellule
                hCell   -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/

void majRecompenses(cellule** grille,
                    enregistrement* enr,
                    int wCell,
                    int hCell)
{
    double M;

    switch(enr->actions[enr->curseur])
    {
        case 0:
            grille[enr->etats[enr->curseur-1].y / hCell][enr->etats[enr->curseur-1].x / wCell].haut += 0.1*(5 - grille[enr->etats[enr->curseur-1].y / hCell][enr->etats[enr->curseur-1].x / wCell].haut);
        break;
        case 1:
            grille[enr->etats[enr->curseur-1].y / hCell][enr->etats[enr->curseur-1].x / wCell].bas += 0.1*(5 - grille[enr->etats[enr->curseur-1].y / hCell][enr->etats[enr->curseur-1].x / wCell].bas);
        break;
        case 2:
            grille[enr->etats[enr->curseur-1].y / hCell][enr->etats[enr->curseur-1].x / wCell].gauche += 0.1*(5 - grille[enr->etats[enr->curseur-1].y / hCell][enr->etats[enr->curseur-1].x / wCell].gauche);
        break;
        case 3:
            grille[enr->etats[enr->curseur-1].y / hCell][enr->etats[enr->curseur-1].x / wCell].droite += 0.1*(5 - grille[enr->etats[enr->curseur-1].y / hCell][enr->etats[enr->curseur-1].x / wCell].droite);
        break;
        default :
        break;
    }

    int i;
    for(i = enr->curseur-1; i>-1; i--)
    {
        M = meilleureRecompense(grille, &(enr->etats[i+1]), wCell, hCell);
        switch(enr->actions[i])
        {
            case 0:
                grille[enr->etats[i].y / hCell][enr->etats[i].x / wCell].haut += 0.1*(grille[enr->etats[i+1].y / hCell][enr->etats[i+1].x / wCell].recompense + 0.9*M - grille[enr->etats[i].y / hCell][enr->etats[i].x / wCell].haut);
            break;
            case 1:
                grille[enr->etats[i].y / hCell][enr->etats[i].x / wCell].bas += 0.1*(grille[enr->etats[i+1].y / hCell][enr->etats[i+1].x / wCell].recompense + 0.9*M - grille[enr->etats[i].y / hCell][enr->etats[i].x / wCell].bas);
            break;
            case 2:
                grille[enr->etats[i].y / hCell][enr->etats[i].x / wCell].gauche += 0.1*(grille[enr->etats[i+1].y / hCell][enr->etats[i+1].x / wCell].recompense + 0.9*M - grille[enr->etats[i].y / hCell][enr->etats[i].x / wCell].gauche);
            break;
            case 3:
                grille[enr->etats[i].y / hCell][enr->etats[i].x / wCell].droite += 0.1*(grille[enr->etats[i+1].y / hCell][enr->etats[i+1].x / wCell].recompense + 0.9*M - grille[enr->etats[i].y / hCell][enr->etats[i].x / wCell].droite);
            break;
            default:
            break;
        }
    }
}




/*--------------------------------------------------------------------------------------------------
Exécute l'algorithme d'apprentissage.
Paramètres :    renderer        -   renderer actif
                grilleCuisine   -   grille représentant l'environnement
                serveur         -   contient les dimensions et emplacement du personnage
                blocs           -   indique les dimensions et emplacement des tuiles au sol dans leur
                                texture
                Tinky           -   tableau contenant les SDL_Rect indiquant les dimensions et
                                emplacements des différents sprites du personnage
                aliments        -   tableau contenant les SDL_Rect indiquant les dimensions et
                                emplacements des différents aliments dans leur texture
                ustensiles      -   tableau contenant les SDL_Rect indiquant les dimensions et
                                emplacements des différents ustensiles dans leur texture
                texturePlancher -   contient les sprites des tuiles au sol
                meuble1         -   contient le sprite du 1er meuble
                meuble2         -   contient le sprite du 2e meuble
                meuble3         -   contient le sprite du 3e meuble
                meuble4         -   contient le sprite du 4e meuble
                meuble5         -   contient le sprite du 5e meuble
                meuble6         -   contient le sprite du 6e meuble
                textureAliments -   contient les sprites des aliments
                textureBulle    -   contient le sprite de la bulle
                textureTinky    -   contient les sprites du personnage
                score           -   score de pourboire
                indiceAliment   -   indice de l'aliment commandé
                wCell           -   largeur d'une cellule
                hCell           -   hauteur d'une cellule
                verre           -   bruitage de cassage de verre
                assiette        -   bruitage de cassage d'assiette
--------------------------------------------------------------------------------------------------*/

void apprentissageCuisine(SDL_Renderer * renderer,
                        cellule** grilleCuisine,
                        SDL_Rect serveur,
                        SDL_Rect* blocs,
                        SDL_Rect* Tinky,
                        SDL_Rect* aliments,
                        SDL_Rect* ustensiles,
                        SDL_Texture * texturePlancher,
                        SDL_Texture * meuble1,
                        SDL_Texture * meuble2,
                        SDL_Texture * meuble3, 
                        SDL_Texture * meuble4,
                        SDL_Texture * meuble5,
                        SDL_Texture * meuble6,
                        SDL_Texture* textureAliments,
                        SDL_Texture* textureBulle,
                        SDL_Texture* textureTinky,
                        int* score,
                        int indiceAliment,
                        int wCell,
                        int hCell,
                        Mix_Music* verre,
                        Mix_Music* assiette,
                        Mix_Music* ambiance,
                        SDL_bool * bool_music)
{
    int i, compteur, simulation, action, bruit, oldScore = *score;
    enregistrement enr;

    simulation = 10000;                                                                                      // Nombre de simulations lancées
    SDL_Rect OldTinky;
    OldTinky.w = serveur.w;
    OldTinky.h = serveur.h;
                                                                                        
    for(i=0;i<simulation;i++)
    {                
        compteur = 0;                                                                                       // Nombre de mouvements pour trouver la sortie
        enr.curseur = 0;
        *score = oldScore + 5;

        spawn(renderer, grilleCuisine, aliments, ustensiles, &serveur, textureAliments, wCell, hCell, indiceAliment);
        
        while(!fini(grilleCuisine, serveur, wCell, hCell))
        {
            compteur++;
            OldTinky.x = serveur.x;
            OldTinky.y = serveur.y;

            action = nextStep(grilleCuisine, &enr, &serveur, wCell, hCell, 100 - i*100/simulation);                 // Avance à l'état suivant
            enr.curseur++;

            if (*score > 10 && *bool_music == SDL_FALSE)
            {
                *bool_music = SDL_TRUE;
                Mix_PlayMusic(ambiance, -1);
            }

            if (i == simulation - 1)                                                                        // On affiche uniquement la dernière simulation
            {
                afficheMap(renderer, grilleCuisine,
                            serveur, blocs, Tinky, aliments, ustensiles, &OldTinky,
                            texturePlancher, meuble1, meuble2, meuble3, meuble4, meuble5, meuble6,
                            textureAliments, textureBulle, textureTinky,
                            indiceAliment, *score, wCell, hCell, action);

                if(grilleCuisine[serveur.y/hCell][serveur.x/wCell].recompense < 0)                     // Si on a une récompense négative, bruitage + perte de score
                {
                    *score -= 1;
                    
                    if (!(*bool_music))
                    {
                        bruit = rand()%2;
                        if(bruit)
                        {
                            Mix_PlayMusic(verre, 0);
                        }
                        else
                        {
                            Mix_PlayMusic(assiette, 0);
                        }
                    }
                    //printf("%d %d :", serveur.y / hCell, serveur.x / wCell);
                }
            }
        }

        majRecompenses(grilleCuisine, &enr, wCell, hCell);                                                         // Mise à jour des espérances de la grille
    }
    printf("%d, %d\n", enr.actions[enr.curseur-1], enr.actions[enr.curseur]);
}




/*--------------------------------------------------------------------------------------------------
Exécute l'algorithme d'apprentissage.
Paramètres :    renderer            -   renderer actif
                grilleSalle         -   grille représentant l'environnement
                serveur             -   contient les dimensions et emplacement du personnage
                piano               -   contient les dimensions et emplacement du piano
                pianiste            -   contient les dimensions et emplacement du pianiste
                main                -   contient les dimensions et emplacement de la main
                blocs               -   indique les dimensions et emplacement des tuiles au sol dans
                                    leur texture
                Tinky               -   tableau contenant les SDL_Rect indiquant les dimensions et
                                    emplacements des différents sprites du personnage
                meublesSalle        -   contient les dimensions et emplacements des meubles de la
                                    salle
                aliments            -   tableau contenant les SDL_Rect indiquant les dimensions et
                                    emplacements des différents aliments dans leur texture
                ustensiles          -   tableau contenant les SDL_Rect indiquant les dimensions et
                                    emplacements des différents ustensiles dans leur texture
                texturePlancher     -   contient les sprites des tuiles au sol
                textureFurniture    -   contient les sprites des meubles de la salle
                textureTinky        -   contient les sprites du personnage
                textureAliments     -   contient les sprites des aliments
                texturePiano        -   contient le sprite du piano
                texturePianiste     -   contient le sprite du pianiste
                textureMain         -   contient le sprite de la main
                score               -   score de pourboire
                indiceAliment       -   indice de l'aliment commandé
                indiceTable         -   indice de la table choisie
                wCell               -   largeur d'une cellule
                hCell               -   hauteur d'une cellule
                verre               -   bruitage de cassage de verre
                assiette            -   bruitage de cassage d'assiette
--------------------------------------------------------------------------------------------------*/

void apprentissageSalle(SDL_Renderer* renderer,
                        cellule** grilleSalle,
                        SDL_Rect serveur,
                        SDL_Rect piano,
                        SDL_Rect pianiste,
                        SDL_Rect main,
                        SDL_Rect* blocs,
                        SDL_Rect* Tinky,
                        SDL_Rect* meublesSalle,
                        SDL_Rect * aliments,
                        SDL_Texture* texturePlancher,
                        SDL_Texture* textureFurniture,
                        SDL_Texture* textureTinky,
                        SDL_Texture* textureAliments,
                        SDL_Texture* texturePiano,
                        SDL_Texture* texturePianiste,
                        SDL_Texture* textureMain,
                        int* score,
                        int indiceAliment,
                        int indiceTable,
                        int wCell,
                        int hCell,
                        Mix_Music* verre,
                        Mix_Music* assiette,
                        Mix_Music* ambiance,
                        SDL_bool* bool_music)
{
    int  i, compteur, simulation, action, bruit, oldScore = *score;
    enregistrement enr;
    
    int tabAction[LONGMAX];                                                                                 // Enregistrement des actions inversées
    
    simulation = 10000;                                                                                      // Nombre de simulations lancées
    SDL_Rect OldTinky, tinkySpawn;
                                                                                        
    for(i=0;i<simulation;i++)
    {
        *score = oldScore + 5;
        enr.curseur = 0;
        compteur = 0;                                                                                       // Nombre de mouvements pour trouver la sortie
                      
        spawnSalle(grilleSalle, &serveur, indiceTable);
        tinkySpawn.x = serveur.x; tinkySpawn.y = serveur.y;

        while(!fini(grilleSalle, serveur, wCell, hCell))
        {
            OldTinky.x = serveur.x; OldTinky.y = serveur.y;
            action = nextStep(grilleSalle, &enr, &serveur, wCell, hCell, 100 - i*100/simulation);  
            enr.curseur++;
            
            if (*score > 10 && *bool_music == SDL_FALSE)
            {
                *bool_music = SDL_TRUE;
                Mix_PlayMusic(ambiance, -1);
            }

            if(i == simulation - 1)
            {
                switch(action)                                                                              // Inversion des actions
                {
                    case HAUT :
                        action = BAS;
                    break;

                    case BAS :
                        action = HAUT;
                    break;

                    case GAUCHE :
                        action = DROITE;
                    break;

                    case DROITE :
                        action = GAUCHE;
                    break;

                    default :
                    break;

                }

                tabAction[compteur] = action;

                compteur++;
            }
        }
        majRecompenses(grilleSalle, &enr, wCell, hCell);
    }

    OldTinky = grilleSalle[0][4].rect;
    compteur--;

    affichageSalle(renderer, grilleSalle,
                        serveur, piano, pianiste, main,
                        blocs, Tinky, meublesSalle, aliments, &OldTinky,
                        texturePlancher,
                        textureFurniture,
                        textureTinky,
                        textureAliments, texturePiano, texturePianiste, textureMain,
                        *score, indiceAliment,
                        wCell,
                        hCell,
                        action);

    while (compteur > -1 && !(serveur.x == tinkySpawn.x && serveur.y == tinkySpawn.y))                      // Affichage des déplacements
    {
        switch(tabAction[compteur])
        {
            case HAUT :
                serveur.y = MAX(serveur.y - hCell, 0);         
            break;

            case BAS :
                serveur.y = MIN(serveur.y + hCell, 7*hCell); 
            break;

            case GAUCHE :
                serveur.x = MAX(serveur.x - wCell, 0); 
            break;

            case DROITE :
                serveur.x = MIN(serveur.x + wCell, 7*wCell); 
            break;

            default :
            break;
        }

        affichageSalle(renderer, grilleSalle,
                        serveur, piano, pianiste, main,
                        blocs, Tinky, meublesSalle, aliments, &OldTinky,
                        texturePlancher,
                        textureFurniture,
                        textureTinky,
                        textureAliments, texturePiano, texturePianiste, textureMain,
                        *score, indiceAliment,
                        wCell,
                        hCell,
                        tabAction[compteur]);
        compteur--;
        
        if(grilleSalle[serveur.y/hCell][serveur.x/wCell].recompense < 0)
        {
            *score -= 1;
            
            if (!(*bool_music))
            {
                bruit = rand()%2;
                if(bruit)
                {
                    Mix_PlayMusic(verre, 0);
                }
                else
                {
                    Mix_PlayMusic(assiette, 0);
                }
            }
            
        }
    }

    afficheAlimSurTable (   renderer,
                            grilleSalle,
                            serveur,
                            piano,
                            pianiste,
                            blocs,
                            Tinky,
                            meublesSalle,
                            aliments,
                            &OldTinky,
                            texturePlancher,
                            textureFurniture,
                            textureTinky,
                            textureAliments,
                            texturePiano,
                            texturePianiste,
                            *score,
                            indiceAliment,
                            wCell,
                            hCell);
}
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>
#include <stdio.h>

#include "affichage.h"
#include "utilitaire.h"

/*--------------------------------------------------------------------------------------------------
Affiche le sol.
Paramètres :    renderer        -   renderer actif
                grilleCuisine   -   grille représentant l'environnement
                bloc            -   indique les dimensions et emplacement des tuiles au sol dans leur
                                texture
                texturePlancher        -   contient les sprites des tuiles au sol
--------------------------------------------------------------------------------------------------*/

void afficheFond(SDL_Renderer *renderer,
                cellule **grilleCuisine,
                SDL_Rect bloc,
                SDL_Texture *texturePlancher)
{

    for (int i=0; i<8; i++)
    {
        for (int j=0; j<8; j++)
        {
            SDL_RenderCopy(renderer, texturePlancher, &bloc, &grilleCuisine[i][j].rect);
        }
    }
}




/*--------------------------------------------------------------------------------------------------
Affiche les meubles de la cuisine.
Paramètres :    renderer        -   renderer actif
                textureMeuble1  -   contient le sprite du 1er meuble
                textureMeuble2  -   contient le sprite du 2e meuble
                textureMeuble3  -   contient le sprite du 3e meuble
                textureMeuble4  -   contient le sprite du 4e meuble
                textureMeuble5  -   contient le sprite du 5e meuble
                textureMeuble6  -   contient le sprite du 6e meuble
                wCell           -   largeur d'une cellule
                hCell           -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/

void afficheMeubles(SDL_Renderer *renderer,
                    SDL_Texture * textureMeuble1,
                    SDL_Texture * textureMeuble2,
                    SDL_Texture * textureMeuble3, 
                    SDL_Texture * textureMeuble4,
                    SDL_Texture * textureMeuble5,
                    SDL_Texture * textureMeuble6,
                    int wCell,
                    int hCell)
{

    // ---------------Choix des emplacements et dimensions des meubles--------------- 
    SDL_Rect imgtextureMeuble1, imgtextureMeuble2, imgtextureMeuble3, imgtextureMeuble4, imgmeuble5, imgmeuble6;

    imgtextureMeuble1.x = 0;
    imgtextureMeuble1.y = 0;
    imgtextureMeuble1.w = 240;
    imgtextureMeuble1.h = 47;

    imgtextureMeuble2.x = 0;
    imgtextureMeuble2.y = 0;
    imgtextureMeuble2.w = 28;
    imgtextureMeuble2.h = 210;

    imgtextureMeuble3.x = 0;
    imgtextureMeuble3.y = 0;
    imgtextureMeuble3.w = 67;
    imgtextureMeuble3.h = 72;

    imgtextureMeuble4.x = 11;
    imgtextureMeuble4.y = 11;
    imgtextureMeuble4.w = 66;
    imgtextureMeuble4.h = 74;

    imgmeuble5.x = 0;
    imgmeuble5.y = 0;
    imgmeuble5.w = 83;
    imgmeuble5.h = 28;

    imgmeuble6.x = 10;
    imgmeuble6.y = 0;
    imgmeuble6.w = 68;
    imgmeuble6.h = 41;

    SDL_Rect placetextureMeuble1, placetextureMeuble2, placetextureMeuble3, placetextureMeuble4, placemeuble5, placemeuble6;

    placetextureMeuble1.x = 2 * wCell;
    placetextureMeuble1.y = 0;
    placetextureMeuble1.w = 4 * wCell;
    placetextureMeuble1.h = 1.5 * hCell;

    placetextureMeuble2.x = 7 * wCell;
    placetextureMeuble2.y = 2 * hCell;
    placetextureMeuble2.w = wCell;
    placetextureMeuble2.h = 3 * hCell;

    placetextureMeuble3.x = 0;
    placetextureMeuble3.y = 2 * hCell;
    placetextureMeuble3.w = 2 * wCell;
    placetextureMeuble3.h = 2.5 * hCell;

    placetextureMeuble4.x = 6 * wCell;
    placetextureMeuble4.y = 6 * hCell;
    placetextureMeuble4.w = 2 * wCell;
    placetextureMeuble4.h = 2 * hCell;

    placemeuble5.x = 0;
    placemeuble5.y = 6 * hCell;
    placemeuble5.w = 2 * wCell;
    placemeuble5.h = 1 * hCell;

    placemeuble6.x = 3 * wCell;
    placemeuble6.y = 3 * wCell;
    placemeuble6.w = 2 * wCell;
    placemeuble6.h = 3 * wCell;


    // ---------------Incrustation dans le renderer---------------
    SDL_RenderCopy(renderer, textureMeuble1, &imgtextureMeuble1, &placetextureMeuble1);
    SDL_RenderCopy(renderer, textureMeuble2, &imgtextureMeuble2, &placetextureMeuble2);
    SDL_RenderCopy(renderer, textureMeuble3, &imgtextureMeuble3, &placetextureMeuble3);
    SDL_RenderCopy(renderer, textureMeuble4, &imgtextureMeuble4, &placetextureMeuble4);
    SDL_RenderCopy(renderer, textureMeuble5, &imgmeuble5, &placemeuble5);
    SDL_RenderCopy(renderer, textureMeuble6, &imgmeuble6, &placemeuble6);
}




/*--------------------------------------------------------------------------------------------------
Affiche les aliments à leur place dans la cuisine.
Retourne un tableau comportant les différents emplacements où se trouve un aliment.
Paramètres :    renderer        -   renderer actif
                grilleCuisine   -   grille représentant l'environnement
                aliments        -   tableau contenant les SDL_Rect indiquant les dimensions et
                                emplacements des différents aliments dans leur texture
                ustensiles      -   tableau contenant les SDL_Rect indiquant les dimensions et
                                emplacements des différents ustensiles dans leur texture
                textureAliments -   contient les sprites des aliments
                wCell           -   largeur d'une cellule
                hCell           -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/

SDL_Rect * afficheAliments( SDL_Renderer* renderer,
                            cellule** grilleCuisine,
                            SDL_Rect* aliments,
                            SDL_Rect* ustensiles,
                            SDL_Texture* textureAliments,
                            int wCell,
                            int hCell)
{
    SDL_Rect * grilleItems = malloc(19*sizeof(SDL_Rect));
    
    grilleItems[0] = grilleCuisine[0][2].rect; grilleItems[1] = grilleCuisine[0][3].rect; grilleItems[2] = grilleCuisine[0][4].rect, grilleItems[3] = grilleCuisine[0][5].rect;
    grilleItems[4] = grilleCuisine[3][4].rect; grilleItems[5] = grilleCuisine[4][3].rect; grilleItems[6] = grilleCuisine[4][4].rect; grilleItems[7] = grilleCuisine[5][3].rect;
    grilleItems[8] = grilleCuisine[5][4].rect; grilleItems[9] = grilleCuisine[2][0].rect; grilleItems[10] = grilleCuisine[2][1].rect; grilleItems[11] = grilleCuisine[3][1].rect;
    grilleItems[12] = grilleCuisine[3][7].rect; grilleItems[13] = grilleCuisine[4][7].rect; grilleItems[14] = grilleCuisine[6][7].rect; grilleItems[15] = grilleCuisine[6][0].rect;
    grilleItems[16] = grilleCuisine[6][1].rect; grilleItems[17] = grilleCuisine[7][6].rect; grilleItems[18] = grilleCuisine[7][7].rect;

    int i;
    for(i=0;i<19;i++)
    {
        if(i<17)
        {
            grilleItems[i].w = aliments[i].w * (0.5*hCell / aliments[i].w);
            grilleItems[i].h = 0.5*hCell;                                                                   // Les éléments seront moitié moins hauts que leur case
            grilleItems[i].x = grilleItems[i].x + 0.25*wCell;                                               // Les éléments sont centrés sur la case
            grilleItems[i].y = grilleItems[i].y + 0.25*hCell;
            SDL_RenderCopy(renderer, textureAliments, &aliments[i], &grilleItems[i]);
        }
        else
        {
            grilleItems[i].w = ustensiles[(i+1)%2].w * (0.5*hCell / ustensiles[(i+1)%2].w);
            grilleItems[i].h = 0.5*hCell;
            grilleItems[i].x = grilleItems[i].x + 0.25*wCell;
            grilleItems[i].y = grilleItems[i].y + 0.25*hCell;
            SDL_RenderCopy(renderer, textureAliments, &ustensiles[(i+1)%2], &grilleItems[i]);
        }
    }

    return grilleItems;
}




/*--------------------------------------------------------------------------------------------------
Affiche la bulle de commande.
Paramètres :    renderer        -   renderer actif
                aliments        -   tableau contenant les SDL_Rect indiquant les dimensions et
                                emplacements des différents aliments dans leur texture
                textureAliments -   contient les sprites des aliments
                textureBulle    -   contient le sprite de la bulle
                indiceAliment   -   indice de l'aliment commandé
                wCell           -   largeur d'une cellule
                hCell           -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/

void afficheBulle(SDL_Renderer* renderer,
                SDL_Rect* aliments,
                SDL_Texture* textureAliments,
                SDL_Texture* textureBulle,
                int indiceAliment,
                int wCell,
                int hCell)
{
    SDL_Rect rectBulle, zoneWindowBulle, zoneWindowAliment;

    rectBulle.x = 0; rectBulle.y = 0;                                                                       // Sélection de la bulle
    rectBulle.w = 1280; rectBulle.h = 640;

    zoneWindowBulle.x = 4*wCell; zoneWindowBulle.y = 7*hCell;                                               // Placement de la bulle
    zoneWindowBulle.w = wCell; zoneWindowBulle.h = hCell;

    zoneWindowAliment.x = 4*wCell + 0.2*wCell; zoneWindowAliment.y = 7*hCell + 0.2*hCell;                   // Placement de l'aliment
    zoneWindowAliment.w = 0.5*wCell; zoneWindowAliment.h = aliments[indiceAliment].h * (0.5*hCell/aliments[indiceAliment].h);
    
    SDL_RenderCopy(renderer, textureBulle, &rectBulle, &zoneWindowBulle);
    SDL_RenderCopy(renderer, textureAliments, &aliments[indiceAliment], &zoneWindowAliment);
}




/*--------------------------------------------------------------------------------------------------
Affiche la cuisine.
Paramètres :    renderer        -   renderer actif
                grilleCuisine   -   grille représentant l'environnement
                serveur         -   contient les dimensions et emplacement du personnage
                blocs           -   indique les dimensions et emplacement des tuiles au sol dans leur
                                texture
                Tinky           -   tableau contenant les SDL_Rect indiquant les dimensions et
                                emplacements des différents sprites du personnage
                aliments        -   tableau contenant les SDL_Rect indiquant les dimensions et
                                emplacements des différents aliments dans leur texture
                ustensiles      -   tableau contenant les SDL_Rect indiquant les dimensions et
                                emplacements des différents ustensiles dans leur texture
                OldTinky        - dimensions et emplacement précédents de Pablo Tinky
                texturePlancher        -   contient les sprites des tuiles au sol
                textureMeuble1         -   contient le sprite du 1er meuble
                textureMeuble2         -   contient le sprite du 2e meuble
                textureMeuble3         -   contient le sprite du 3e meuble
                textureMeuble4         -   contient le sprite du 4e meuble
                textureMeuble5         -   contient le sprite du 5e meuble
                textureMeuble6         -   contient le sprite du 6e meuble
                textureAliments -   contient les sprites des aliments
                textureBulle    -   contient le sprite de la bulle
                textureTinky    -   contient les sprites du personnage
                indiceAliment   -   indice de l'aliment commandé
                wCell           -   largeur d'une cellule
                hCell           -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/

void afficheMap(SDL_Renderer *renderer,
                cellule ** grilleCuisine,
                SDL_Rect serveur,
                SDL_Rect* blocs,
                SDL_Rect* Tinky,
                SDL_Rect* aliments,
                SDL_Rect* ustensiles,
                SDL_Rect * OldTinky,
                SDL_Texture * texturePlancher,
                SDL_Texture * textureMeuble1,
                SDL_Texture * textureMeuble2,
                SDL_Texture * textureMeuble3, 
                SDL_Texture * textureMeuble4,
                SDL_Texture * textureMeuble5,
                SDL_Texture * textureMeuble6,
                SDL_Texture* textureAliments,
                SDL_Texture* textureBulle,
                SDL_Texture* textureTinky,
                int indiceAliment,
                int score,
                int wCell,
                int hCell,
                int action)
{
    
    
    int i, freq;                                                                                            // Fréquence à laquelle les sprites vont varier
    freq = 12;
    SDL_Rect posAlim;
    posAlim.x = serveur.x + serveur.w*0.7;
    posAlim.y = serveur.y + serveur.h*0.5;
    posAlim.w = serveur.w/3;
    posAlim.h = serveur.h/3;


    /*--------Score--------*/

    SDL_Rect posTexte;
    char phrase[16];
    sprintf(phrase, "Pourboire : %d", score);

    TTF_Font* font = TTF_OpenFont("./data/Airthan-Age.ttf", 100);
    SDL_Color color = {253, 108, 168, 255};
    
    SDL_Surface* texte = TTF_RenderText_Blended(font, phrase, color);

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, texte);                              // Transfert de la surface à la texture
    
    SDL_QueryTexture(text_texture, NULL, NULL, &posTexte.w, &posTexte.h);                                   // Choix de la position du texte
    posTexte.x = 0.1*wCell; posTexte.y = 7.4*hCell;

    switch(action)
    {
        case BAS :
            i = 1;
            while(OldTinky->y < serveur.y)
            {
                OldTinky->y++;
                posAlim.x = OldTinky->x + serveur.w*0.7;
                posAlim.y = OldTinky->y + serveur.h*0.5;
                
                if(OldTinky->y % (hCell/freq) == 0)
                {
                    i++;
                    if (i ==3) {i = 1;}
                }

                SDL_RenderClear(renderer);
                /* ---------------création du fond--------------- */
                afficheFond(renderer, grilleCuisine, blocs[0], texturePlancher);

                /* ---------------affichage des meubles--------------- */
                afficheMeubles(renderer, textureMeuble1, textureMeuble2, textureMeuble3, 
                            textureMeuble4, textureMeuble5, textureMeuble6, wCell, hCell);

                /* ---------------affichage des aliments--------------*/
                afficheAliments(renderer, grilleCuisine, aliments, ustensiles, textureAliments, wCell, hCell);

                /* --------------affichage de la bulle------------ */
                afficheBulle(renderer, aliments, textureAliments, textureBulle, indiceAliment, wCell, hCell);

                /* ---------------animation de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureTinky, &Tinky[i], OldTinky);

                /* ---------------animation de l'aliment dans la main de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureAliments, &aliments[indiceAliment], &posAlim);

                /*---------------affichage du score-------------------*/
                SDL_RenderCopy(renderer, text_texture, NULL, &posTexte); 
                
                SDL_RenderPresent(renderer);
            }
        break;

        case DROITE :
            i = 7;
            while(OldTinky->x < serveur.x)
            {
                OldTinky->x++;
                posAlim.x = OldTinky->x + serveur.w*0.7;
                posAlim.y = OldTinky->y + serveur.h*0.5;
                
                if(OldTinky->x % (wCell/freq) == 0)
                {
                    i++;
                    if (i == 9) {i = 7;}
                }

                SDL_RenderClear(renderer);
                /* ---------------création du fond--------------- */
                afficheFond(renderer, grilleCuisine, blocs[0], texturePlancher);

                /* ---------------affichage des meubles--------------- */
                afficheMeubles(renderer, textureMeuble1, textureMeuble2, textureMeuble3, 
                            textureMeuble4, textureMeuble5, textureMeuble6, wCell, hCell);

                /* ---------------affichage des aliments--------------*/
                afficheAliments(renderer, grilleCuisine, aliments, ustensiles, textureAliments, wCell, hCell);

                /* --------------affichage de la bulle------------ */
                afficheBulle(renderer, aliments, textureAliments, textureBulle, indiceAliment, wCell, hCell);

                /* ---------------animation de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureTinky, &Tinky[i], OldTinky);

                /* ---------------animation de l'aliment dans la main de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureAliments, &aliments[indiceAliment], &posAlim);

                /*---------------affichage du score-------------------*/
                SDL_RenderCopy(renderer, text_texture, NULL, &posTexte); 
                
                SDL_RenderPresent(renderer);
            }
        break;

        case HAUT :
            i = 10;
            while(OldTinky->y > serveur.y)
            {
                OldTinky->y--;
                posAlim.x = OldTinky->x + serveur.w*0.7;
                posAlim.y = OldTinky->y + serveur.h*0.5;
                
                if(OldTinky->y % (hCell/freq) == 0)
                {
                    i++;
                    if (i == 12) {i = 10;}
                }

                SDL_RenderClear(renderer);
                /* ---------------création du fond--------------- */
                afficheFond(renderer, grilleCuisine, blocs[0], texturePlancher);

                /* ---------------affichage des meubles--------------- */
                afficheMeubles(renderer, textureMeuble1, textureMeuble2, textureMeuble3, 
                            textureMeuble4, textureMeuble5, textureMeuble6, wCell, hCell);

                /* ---------------affichage des aliments--------------*/
                afficheAliments(renderer, grilleCuisine, aliments, ustensiles, textureAliments, wCell, hCell);

                /* --------------affichage de la bulle------------ */
                afficheBulle(renderer, aliments, textureAliments, textureBulle, indiceAliment, wCell, hCell);

                /* ---------------animation de l'aliment dans la main de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureAliments, &aliments[indiceAliment], &posAlim);

                /* ---------------animation de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureTinky, &Tinky[i], OldTinky);

                /*---------------affichage du score-------------------*/
                SDL_RenderCopy(renderer, text_texture, NULL, &posTexte); 
                
                SDL_RenderPresent(renderer);
            }
        break;

        case GAUCHE :
            i = 4;
            while(OldTinky->x > serveur.x)
            {
                OldTinky->x--;
                posAlim.x = OldTinky->x;
                posAlim.y = OldTinky->y + serveur.h*0.5;
                
                if(OldTinky->x % (wCell/freq) == 0)
                {
                    i++;
                    if (i == 6) {i = 4;}
                }

                SDL_RenderClear(renderer);
                /* ---------------création du fond--------------- */
                afficheFond(renderer, grilleCuisine, blocs[0], texturePlancher);

                /* ---------------affichage des meubles--------------- */
                afficheMeubles(renderer, textureMeuble1, textureMeuble2, textureMeuble3, 
                            textureMeuble4, textureMeuble5, textureMeuble6, wCell, hCell);

                /* ---------------affichage des aliments--------------*/
                afficheAliments(renderer, grilleCuisine, aliments, ustensiles, textureAliments, wCell, hCell);

                /* --------------affichage de la bulle------------ */
                afficheBulle(renderer, aliments, textureAliments, textureBulle, indiceAliment, wCell, hCell);

                /* ---------------animation de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureTinky, &Tinky[i], OldTinky);

                /* ---------------animation de l'aliment dans la main de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureAliments, &aliments[indiceAliment], &posAlim);

                /*---------------affichage du score-------------------*/
                SDL_RenderCopy(renderer, text_texture, NULL, &posTexte); 
                
                SDL_RenderPresent(renderer);
            }
        break;

        default :
    
        break;
    }
    
    SDL_FreeSurface(texte);
    SDL_DestroyTexture(text_texture);
    TTF_CloseFont(font);
}




/*--------------------------------------------------------------------------------------------------
Affiche les meubles de la salle.
Paramètres :    renderer            -   renderer actif
                grilleSalle         -   grille représentant l'environnement
                meublesSalle        -   sélection des meubles dans leur texture
                textureFurniture    -   texture contenant les meubles
                wCell               -   largeur d'une cellule
                hCell               -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/

void afficheMeublesSalle(SDL_Renderer* renderer,
                        cellule** grilleSalle,
                        SDL_Rect* meublesSalle,
                        SDL_Texture* textureFurniture,
                        int wCell,
                        int hCell)
{
    SDL_Rect grilleMeubles[20] = {grilleSalle[0][1].rect, grilleSalle[0][2].rect, grilleSalle[3][5].rect, // Tables verticales
                                grilleSalle[5][1].rect, grilleSalle[6][1].rect, grilleSalle[7][5].rect, // Tables horizontales
                                grilleSalle[7][1].rect, grilleSalle[7][2].rect, grilleSalle[5][5].rect, // Chaises haut
                                grilleSalle[4][1].rect, grilleSalle[4][2].rect, grilleSalle[2][5].rect, // Chaises bas
                                grilleSalle[0][3].rect, grilleSalle[1][3].rect, grilleSalle[7][7].rect, // Chaises gauche
                                grilleSalle[7][4].rect, grilleSalle[0][0].rect, grilleSalle[1][0].rect, // Chaises droite
                                grilleSalle[3][0].rect, grilleSalle[3][3].rect}; // Plante et tapis
    
    int i;
    for(i=0;i<3;i++)
    {
        grilleMeubles[i].h = 2*hCell;
        SDL_RenderCopy(renderer, textureFurniture, &meublesSalle[0], &grilleMeubles[i]);
    }
    for(i=3;i<6;i++)
    {
        grilleMeubles[i].w = 2*wCell;
        SDL_RenderCopy(renderer, textureFurniture, &meublesSalle[1], &grilleMeubles[i]);
    }
    for(i=6;i<9;i++)
    {
        grilleMeubles[i].w = 0.75*wCell; grilleMeubles[i].h = 0.75*hCell;
        SDL_RenderCopy(renderer, textureFurniture, &meublesSalle[2], &grilleMeubles[i]);
    }
    for(i=9;i<12;i++)
    {
        grilleMeubles[i].w = 0.75*wCell; grilleMeubles[i].h = 0.75*hCell;
        SDL_RenderCopy(renderer, textureFurniture, &meublesSalle[3], &grilleMeubles[i]);
    }
    for(i=12;i<15;i++)
    {
        grilleMeubles[i].w = 0.75*wCell; grilleMeubles[i].h = 0.75*hCell;
        SDL_RenderCopy(renderer, textureFurniture, &meublesSalle[4], &grilleMeubles[i]);
    }
    for(i=15;i<18;i++)
    {
        grilleMeubles[i].w = 0.75*wCell; grilleMeubles[i].h = 0.75*hCell;
        SDL_RenderCopy(renderer, textureFurniture, &meublesSalle[5], &grilleMeubles[i]);
    }
    grilleMeubles[18].w = meublesSalle[6].w * (0.75*hCell / meublesSalle[6].h); grilleMeubles[18].h = 0.75*hCell;
    SDL_RenderCopy(renderer, textureFurniture, &meublesSalle[6], &grilleMeubles[18]);
    grilleMeubles[19].w = 2*wCell; grilleMeubles[19].h = 2*hCell;
    SDL_RenderCopy(renderer, textureFurniture, &meublesSalle[7], &grilleMeubles[19]);
}




/*--------------------------------------------------------------------------------------------------
Affiche l'aliment sur la table une fois amené.
Paramètres :    renderer            -   renderer actif
                grilleSalle         -   grille représentant l'environnement
                serveur             -   contient les dimensions et emplacement du personnage
                piano               -   contient les dimensions et emplacement du piano
                pianiste            -   contient les dimensions et emplacement du pianiste
                blocs               -   indique les dimensions et emplacement des tuiles au sol dans leur
                                    texture
                Tinky               -   tableau contenant les SDL_Rect indiquant les dimensions et
                                    emplacements des différents sprites du personnage
                meublesSalle        -   contient les dimensions et emplacement des meubles de la salle
                aliments            -   tableau contenant les SDL_Rect indiquant les dimensions et
                                    emplacements des différents aliments dans leur texture
                OldTinky            - dimensions et emplacement précédents de Pablo Tinky
                texturePlancher     -   contient les sprites des tuiles au sol
                textureFurniture    -   contient les sprites des meubles de la salle
                textureTinky        -   contient les sprites du personnage
                textureAliments     -   contient les sprites des aliments
                texturePiano        -   contient le sprite du piano
                texturePianiste     -   contient le sprite du pianiste
                score               -   score de pourboire
                indiceAliment       -   indice de l'aliment commandé
                wCell               -   largeur d'une cellule
                hCell               -   hauteur d'une cellule
--------------------------------------------------------------------------------------------------*/

void afficheAlimSurTable (SDL_Renderer* renderer,
                    cellule** grilleSalle,
                    SDL_Rect serveur,
                    SDL_Rect piano,
                    SDL_Rect pianiste,
                    SDL_Rect* blocs,
                    SDL_Rect* Tinky,
                    SDL_Rect* meublesSalle,
                    SDL_Rect * aliments,
                    SDL_Rect * OldTinky,
                    SDL_Texture* texturePlancher,
                    SDL_Texture* textureFurniture,
                    SDL_Texture* textureTinky,
                    SDL_Texture* textureAliments,
                    SDL_Texture* texturePiano,
                    SDL_Texture* texturePianiste,
                    int score,
                    int indiceAliment,
                    int wCell,
                    int hCell)
{
    SDL_Rect posAlim, posPianiste, posPiano;
    
    /*--------Score--------*/

    SDL_Rect posTexte;
    char phrase[16];
    sprintf(phrase, "Pourboire : %d", score);

    TTF_Font* font = TTF_OpenFont("./data/Airthan-Age.ttf", 100);
    SDL_Color color = {253, 108, 168, 255};
    
    SDL_Surface* texte = TTF_RenderText_Blended(font, phrase, color);

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, texte);                              // Transfert de la surface à la texture
    
    SDL_QueryTexture(text_texture, NULL, NULL, &posTexte.w, &posTexte.h);                                   // Choix de la position du texte
    posTexte.x = 0.1*wCell; posTexte.y = 7.4*hCell;

    if( OldTinky->x == grilleSalle[2][2].rect.x && OldTinky->y == grilleSalle[2][2].rect.y) {posAlim = grilleSalle[1][2].rect;}
            
    if( OldTinky->x == grilleSalle[3][4].rect.x && OldTinky->y == grilleSalle[3][4].rect.y) {posAlim = grilleSalle[3][5].rect;}

    if( OldTinky->x == grilleSalle[5][3].rect.x && OldTinky->y == grilleSalle[5][3].rect.y) {posAlim = grilleSalle[5][2].rect;}

    if( OldTinky->x == grilleSalle[6][5].rect.x && OldTinky->y == grilleSalle[6][5].rect.y) {posAlim = grilleSalle[7][5].rect;}
    
    posAlim.x += 0.5*wCell;

    SDL_RenderClear(renderer);

    /* ---------------création du fond--------------- */
    afficheFond(renderer, grilleSalle, blocs[1], texturePlancher);

    /* ---------------affichage des meubles--------------- */
    afficheMeublesSalle(renderer, grilleSalle, meublesSalle, textureFurniture, wCell, hCell);

    /* ---------------Pianiste----------- */
    if(score > 10)
    {
        posPiano.x = grilleSalle[0][7].rect.x; posPiano.y = grilleSalle[0][7].rect.y + 0.25*hCell;
        posPiano.w = wCell; posPiano.h = hCell;

        posPianiste.x = grilleSalle[0][6].rect.x - 0.25*wCell; posPianiste.y = grilleSalle[0][6].rect.y;
        posPianiste.w = wCell; posPianiste.h = hCell;

        SDL_RenderCopy(renderer, texturePiano, &piano, &posPiano);
        SDL_RenderCopy(renderer, texturePianiste, &pianiste, &posPianiste);
    }

    /* ---------------animation de Pablo Tinky----------- */
    SDL_RenderCopy(renderer, textureTinky, &Tinky[0], OldTinky);

    /* ---------------animation de l'aliment dans la main de Pablo Tinky----------- */
    posAlim.w = serveur.w/3;
    posAlim.h = serveur.h/3;
    SDL_RenderCopy(renderer, textureAliments, &aliments[indiceAliment], &posAlim);

    /*---------------affichage du score-------------------*/
    SDL_RenderCopy(renderer, text_texture, NULL, &posTexte);


    SDL_RenderPresent(renderer);

    SDL_FreeSurface(texte);
    SDL_DestroyTexture(text_texture);
    TTF_CloseFont(font);

    SDL_Delay(1500);
}




/*--------------------------------------------------------------------------------------------------
Affiche la cuisine.
Paramètres :    renderer            -   renderer actif
                grilleSalle         -   grille représentant l'environnement
                serveur             -   contient les dimensions et emplacement du personnage
                piano               -   contient les dimensions et emplacement du piano
                pianiste            -   contient les dimensions et emplacement du pianiste
                main                -   contient les dimensions et emplacement de la main
                blocs               -   indique les dimensions et emplacement des tuiles au sol dans leur
                                    texture
                Tinky               -   tableau contenant les SDL_Rect indiquant les dimensions et
                                    emplacements des différents sprites du personnage
                meublesSalle        -   contient les dimensions et emplacement des meubles de la salle
                aliments            -   tableau contenant les SDL_Rect indiquant les dimensions et
                                    emplacements des différents aliments dans leur texture
                OldTinky            - dimensions et emplacement précédents de Pablo Tinky
                texturePlancher     -   contient les sprites des tuiles au sol
                textureFurniture    -   contient les sprites des meubles de la salle
                textureTinky        -   contient les sprites du personnage
                textureAliments     -   contient les sprites des aliments
                texturePiano        -   contient le sprite du piano
                texturePianiste     -   contient le sprite du pianiste
                textureMain         -   contient le sprite de la main
                score               -   score de pourboire
                indiceAliment       -   indice de l'aliment commandé
                wCell               -   largeur d'une cellule
                hCell               -   hauteur d'une cellule
                action              -   dernière action réalisée par Pablo Tinky
--------------------------------------------------------------------------------------------------*/

void affichageSalle(SDL_Renderer* renderer,
                    cellule** grilleSalle,
                    SDL_Rect serveur,
                    SDL_Rect piano,
                    SDL_Rect pianiste,
                    SDL_Rect main,
                    SDL_Rect* blocs,
                    SDL_Rect* Tinky,
                    SDL_Rect* meublesSalle,
                    SDL_Rect * aliments,
                    SDL_Rect * OldTinky,
                    SDL_Texture* texturePlancher,
                    SDL_Texture* textureFurniture,
                    SDL_Texture* textureTinky,
                    SDL_Texture* textureAliments,
                    SDL_Texture* texturePiano,
                    SDL_Texture* texturePianiste,
                    SDL_Texture* textureMain,
                    int score,
                    int indiceAliment,
                    int wCell,
                    int hCell,
                    int action)
{
    
    int i, j = 0;
    int jj = 0;
    int freq = 12;
    SDL_Rect posAlim, posPiano, posPianiste, posMain1, posMain2;
    posAlim.x = serveur.x + serveur.w*0.7;
    posAlim.y = serveur.y + serveur.h*0.5;
    posAlim.w = serveur.w/3;
    posAlim.h = serveur.h/3;

    /*--------Score--------*/

    SDL_Rect posTexte;
    char phrase[16];
    sprintf(phrase, "Pourboire : %d", score);

    TTF_Font* font = TTF_OpenFont("./data/Airthan-Age.ttf", 100);
    SDL_Color color = {253, 108, 168, 255};
    
    SDL_Surface* texte = TTF_RenderText_Blended(font, phrase, color);

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, texte);                              // Transfert de la surface à la texture
    
    SDL_QueryTexture(text_texture, NULL, NULL, &posTexte.w, &posTexte.h);                                   // Choix de la position du texte
    posTexte.x = 0.1*wCell; posTexte.y = 7.4*hCell;

    switch(action)
    {
        case BAS :
            i = 1;
            while(OldTinky->y < serveur.y)
            {
                j++;
                if(j%2 == 0) {jj++;}                                                                                // Fréquence de mouvement des mains
                OldTinky->y++;
                posAlim.x = OldTinky->x + serveur.w*0.7;
                posAlim.y = OldTinky->y + serveur.h*0.5;
                
                if(OldTinky->y % (hCell/freq) == 0)
                {
                    i++;
                    if (i ==3) {i = 1;}
                }

                SDL_RenderClear(renderer);

                /* ---------------création du fond--------------- */
                afficheFond(renderer, grilleSalle, blocs[1], texturePlancher);

                /* ---------------affichage des meubles--------------- */
                afficheMeublesSalle(renderer, grilleSalle, meublesSalle, textureFurniture, wCell, hCell);

                /* ---------------Pianiste----------- */
                if(score > 10)                                                                                 // Le pianiste est débloqué à 20 pourboires
                {
                    posPiano.x = grilleSalle[0][7].rect.x; posPiano.y = grilleSalle[0][7].rect.y + 0.25*hCell;
                    posPiano.w = wCell; posPiano.h = hCell;

                    posPianiste.x = grilleSalle[0][6].rect.x - 0.25*wCell; posPianiste.y = grilleSalle[0][6].rect.y;
                    posPianiste.w = wCell; posPianiste.h = hCell;

                    posMain1.x = grilleSalle[0][6].rect.x + 0.5*wCell ; posMain1.y = fabs(cos(jj * (3.14/freq)))*0.25*hCell;
                    posMain1.w = main.w; posMain1.h = main.h;

                    posMain2.x = grilleSalle[0][6].rect.x + 0.5*wCell ; posMain2.y = 0.5*hCell - fabs(cos(jj * (3.14/freq)))*0.25*hCell;
                    posMain2.w = main.w; posMain2.h = main.h;

                    SDL_RenderCopy(renderer, texturePiano, &piano, &posPiano);
                    SDL_RenderCopy(renderer, texturePianiste, &pianiste, &posPianiste);
                    SDL_RenderCopy(renderer, textureMain, &main, &posMain1);
                    SDL_RenderCopy(renderer, textureMain, &main, &posMain2);
                }

                /* ---------------animation de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureTinky, &Tinky[i], OldTinky);

                /* ---------------animation de l'aliment dans la main de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureAliments, &aliments[indiceAliment], &posAlim);

                /*---------------affichage du score-------------------*/
                SDL_RenderCopy(renderer, text_texture, NULL, &posTexte);   

                SDL_RenderPresent(renderer);
            }
        break;

        case DROITE :
            i = 7;
            while(OldTinky->x < serveur.x)
            {
                j++;
                if(j%2 == 0) {jj++;}
                OldTinky->x++;
                posAlim.x = OldTinky->x + serveur.w*0.7;
                posAlim.y = OldTinky->y + serveur.h*0.5;
                
                if(OldTinky->x % (wCell/freq) == 0)
                {
                    i++;
                    if (i == 9) {i = 7;}
                }

                SDL_RenderClear(renderer);

                /* ---------------création du fond--------------- */
                afficheFond(renderer, grilleSalle, blocs[1], texturePlancher);

                /* ---------------affichage des meubles--------------- */
                afficheMeublesSalle(renderer, grilleSalle, meublesSalle, textureFurniture, wCell, hCell);

                /* ---------------Pianiste----------- */
                if(score > 10)
                {
                    posPiano.x = grilleSalle[0][7].rect.x; posPiano.y = grilleSalle[0][7].rect.y + 0.25*hCell;
                    posPiano.w = wCell; posPiano.h = hCell;

                    posPianiste.x = grilleSalle[0][6].rect.x - 0.25*wCell; posPianiste.y = grilleSalle[0][6].rect.y;
                    posPianiste.w = wCell; posPianiste.h = hCell;

                    posMain1.x = grilleSalle[0][6].rect.x + 0.5*wCell ; posMain1.y = fabs(cos(jj * (3.14/freq)))*0.25*hCell;
                    posMain1.w = main.w; posMain1.h = main.h;

                    posMain2.x = grilleSalle[0][6].rect.x + 0.5*wCell ; posMain2.y = 0.5*hCell - fabs(cos(jj * (3.14/freq)))*0.25*hCell;
                    posMain2.w = main.w; posMain2.h = main.h;

                    SDL_RenderCopy(renderer, texturePiano, &piano, &posPiano);
                    SDL_RenderCopy(renderer, texturePianiste, &pianiste, &posPianiste);
                    SDL_RenderCopy(renderer, textureMain, &main, &posMain1);
                    SDL_RenderCopy(renderer, textureMain, &main, &posMain2);
                }

                /* ---------------animation de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureTinky, &Tinky[i], OldTinky);

                /* ---------------animation de l'aliment dans la main de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureAliments, &aliments[indiceAliment], &posAlim);

                /*---------------affichage du score-------------------*/
                SDL_RenderCopy(renderer, text_texture, NULL, &posTexte);   

                SDL_RenderPresent(renderer);
            }
        break;

        case HAUT :
            i = 10;
            while(OldTinky->y > serveur.y)
            {
                j++;
                if(j%2 == 0) {jj++;}
                OldTinky->y--;
                posAlim.x = OldTinky->x + serveur.w*0.7;
                posAlim.y = OldTinky->y + serveur.h*0.5;
                
                if(OldTinky->y % (hCell/freq) == 0)
                {
                    i++;
                    if (i == 12) {i = 10;}
                }

                SDL_RenderClear(renderer);

                /* ---------------création du fond--------------- */
                afficheFond(renderer, grilleSalle, blocs[1], texturePlancher);

                /* ---------------affichage des meubles--------------- */
                afficheMeublesSalle(renderer, grilleSalle, meublesSalle, textureFurniture, wCell, hCell);

                /* ---------------Pianiste----------- */
                if(score > 10)
                {
                    posPiano.x = grilleSalle[0][7].rect.x; posPiano.y = grilleSalle[0][7].rect.y + 0.25*hCell;
                    posPiano.w = wCell; posPiano.h = hCell;

                    posPianiste.x = grilleSalle[0][6].rect.x - 0.25*wCell; posPianiste.y = grilleSalle[0][6].rect.y;
                    posPianiste.w = wCell; posPianiste.h = hCell;

                    posMain1.x = grilleSalle[0][6].rect.x + 0.5*wCell ; posMain1.y = fabs(cos(jj * (3.14/freq)))*0.25*hCell;
                    posMain1.w = main.w; posMain1.h = main.h;

                    posMain2.x = grilleSalle[0][6].rect.x + 0.5*wCell ; posMain2.y = 0.5*hCell - fabs(cos(jj * (3.14/freq)))*0.25*hCell;
                    posMain2.w = main.w; posMain2.h = main.h;

                    SDL_RenderCopy(renderer, texturePiano, &piano, &posPiano);
                    SDL_RenderCopy(renderer, texturePianiste, &pianiste, &posPianiste);
                    SDL_RenderCopy(renderer, textureMain, &main, &posMain1);
                    SDL_RenderCopy(renderer, textureMain, &main, &posMain2);
                }

                /* ---------------animation de l'aliment dans la main de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureAliments, &aliments[indiceAliment], &posAlim);

                /* ---------------animation de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureTinky, &Tinky[i], OldTinky);

                /*---------------affichage du score-------------------*/
                SDL_RenderCopy(renderer, text_texture, NULL, &posTexte);   

                SDL_RenderPresent(renderer);
            }
        break;

        case GAUCHE :
            i = 4;
            while(OldTinky->x > serveur.x)
            {
                j++;
                if(j%2 == 0) {jj++;}
                OldTinky->x--;
                posAlim.x = OldTinky->x;
                posAlim.y = OldTinky->y + serveur.h*0.5;
                
                if(OldTinky->x % (wCell/freq) == 0)
                {
                    i++;
                    if (i == 6) {i = 4;}
                }

                SDL_RenderClear(renderer);

                /* ---------------création du fond--------------- */
                afficheFond(renderer, grilleSalle, blocs[1], texturePlancher);

                /* ---------------affichage des meubles--------------- */
                afficheMeublesSalle(renderer, grilleSalle, meublesSalle, textureFurniture, wCell, hCell);

                /* ---------------Pianiste----------- */
                if(score > 10)
                {
                    posPiano.x = grilleSalle[0][7].rect.x; posPiano.y = grilleSalle[0][7].rect.y + 0.25*hCell;
                    posPiano.w = wCell; posPiano.h = hCell;

                    posPianiste.x = grilleSalle[0][6].rect.x - 0.25*wCell; posPianiste.y = grilleSalle[0][6].rect.y;
                    posPianiste.w = wCell; posPianiste.h = hCell;

                    posMain1.x = grilleSalle[0][6].rect.x + 0.5*wCell ; posMain1.y = fabs(cos(jj * (3.14/freq)))*0.25*hCell;
                    posMain1.w = main.w; posMain1.h = main.h;

                    posMain2.x = grilleSalle[0][6].rect.x + 0.5*wCell ; posMain2.y = 0.5*hCell - fabs(cos(jj * (3.14/freq)))*0.25*hCell;
                    posMain2.w = main.w; posMain2.h = main.h;

                    SDL_RenderCopy(renderer, texturePiano, &piano, &posPiano);
                    SDL_RenderCopy(renderer, texturePianiste, &pianiste, &posPianiste);
                    SDL_RenderCopy(renderer, textureMain, &main, &posMain1);
                    SDL_RenderCopy(renderer, textureMain, &main, &posMain2);
                }

                /* ---------------animation de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureTinky, &Tinky[i], OldTinky);

                /* ---------------animation de l'aliment dans la main de Pablo Tinky----------- */
                SDL_RenderCopy(renderer, textureAliments, &aliments[indiceAliment], &posAlim);

                /*---------------affichage du score-------------------*/
                SDL_RenderCopy(renderer, text_texture, NULL, &posTexte);   

                SDL_RenderPresent(renderer);
            }
        break;

        default :
    
        break;
    }

    SDL_FreeSurface(texte);
    SDL_DestroyTexture(text_texture);
    TTF_CloseFont(font);
}
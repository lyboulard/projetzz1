#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include <SDL2/SDL.h>
#include "utilitaire.h"

void afficheFond(SDL_Renderer*, cellule**, SDL_Rect, SDL_Texture*);
void afficheMeubles(SDL_Renderer*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture *, SDL_Texture*, int, int);
SDL_Rect * afficheAliments(SDL_Renderer*, cellule**, SDL_Rect*, SDL_Rect*, SDL_Texture*, int, int);
void afficheBulle(SDL_Renderer*, SDL_Rect*, SDL_Texture*, SDL_Texture*, int, int, int);
void afficheMap(SDL_Renderer*, cellule **, SDL_Rect, SDL_Rect*, SDL_Rect*, SDL_Rect*, SDL_Rect *, SDL_Rect*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, int, int, int, int, int);
void afficheMeublesSalle(SDL_Renderer*, cellule**, SDL_Rect*, SDL_Texture*, int, int);
void affichageSalle(SDL_Renderer*, cellule**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect*, SDL_Rect*, SDL_Rect*, SDL_Rect*, SDL_Rect *, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture* , SDL_Texture*, SDL_Texture*, SDL_Texture*, int, int, int, int, int);
void afficheAlimSurTable (SDL_Renderer*, cellule**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect*, SDL_Rect*, SDL_Rect*, SDL_Rect*, SDL_Rect *, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture* , SDL_Texture*, SDL_Texture*, int, int, int, int);

#endif
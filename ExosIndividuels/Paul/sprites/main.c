#include <SDL2/SDL.h>
//#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <SDL2/SDL_image.h>

#include "code.h"

int main(int argc, char **argv) 
{
  /* Initialisation */

  (void)argc;
  (void)argv;

  SDL_Window* window = NULL;
  SDL_Renderer* renderer = NULL;

  srand(time(NULL));


  /* Initialisation de la SDL */

  if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

  SDL_DisplayMode screen;
  SDL_GetCurrentDisplayMode(0, &screen);
  //printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w, screen.h);


  /* Création de la fenêtre */

  window = SDL_CreateWindow("Feu de cheminée", SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, TAILLECHEMX, TAILLECHEMY, SDL_WINDOW_OPENGL);
  if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);


  /* Création du renderer */

  renderer = SDL_CreateRenderer(window, -1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);


  /* Initialisation de la texture */

  SDL_Texture* bg=NULL;

  //play_with_texture_1(my_texture, window,renderer); 


  /* Insertion de la texture */

  bg = IMG_LoadTexture(renderer, "./img/cheminée_libre_droits.png");

  if(bg == NULL)
  {
      end_sdl(0, "Echec de chargement d'image", window, renderer);
  }


  /* Affichage renderer */

  SDL_RenderPresent(renderer);


  /* Initialisation du tableau des coordonnées du sprite */

  coordonnees tab[TI][TJ];

  remplissage_coord(tab);
  
  affichage_tabl (tab);


  /* Initialisation de la texture feu */

  SDL_Texture* flammes = IMG_LoadTexture(renderer, "./img/flammes_libre_droits2.png");
  if(flammes == NULL)
  {
      end_sdl(0, "Echec de chargement d'image", window, renderer);
  }

  
   /* Feu */

  feu(bg,flammes,tab,window,renderer);





  SDL_Delay(100*60*10);


  SDL_DestroyTexture(bg);
  IMG_Quit();  
  return 0;
}
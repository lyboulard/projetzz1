#include "code.h"



void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer) 
{                              
  char msg_formated[255];                                                         
  int l;                                                                          

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  }                                                                               

  if (renderer != NULL) 
  {                                           											// Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }

  if (window != NULL)   
  {                                           											// Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();                                                                     

  if (!ok) 
  {                                                        // On quitte si cela ne va pas                
    exit(EXIT_FAILURE);                                                           
  }                                                                               
}



void play_with_texture_1(SDL_Texture *my_texture, SDL_Window *window,SDL_Renderer *renderer) 
{
  SDL_Rect 
          source = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(window, &window_dimensions.w,&window_dimensions.h);                    // Récupération des dimensions de la fenêtre

  SDL_QueryTexture(my_texture, NULL, NULL,&source.w, &source.h);       // Récupération des dimensions de l'image

  destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

  SDL_RenderCopy(renderer, my_texture,&source,&destination);                 // Création de l'élément à afficher

}


void remplissage_coord(coordonnees tab[TI][TJ])
{
	//taille du sprite
	float h_sprite=TAILLEFLAMY;
	float w_sprite=TAILLEFLAMX;

	int val=0;

	//remplissage du tableau
	for(int i=0;i<TI;i++)
	{
		for(int j=0;j<TJ;j++)
			{
				tab[i][j].x=j*w_sprite/TJ;
				tab[i][j].y=i*h_sprite/TI;
				tab[i][j].img=val+1;
				val=val+1;
			}
	}
}

void affichage_tabl (coordonnees tab[TI][TJ])
{
	for(int i=0;i<TI;i++)
	{
		for(int j=0;j<TJ;j++)
			{
				printf("%f, %f, %d\n", tab[i][j].x, tab[i][j].y, tab[i][j].img);
			}
	}
}


int aleat(int lim)
{
	return rand()%lim;
}



void feu(SDL_Texture *bg_texture,SDL_Texture *my_texture,coordonnees tab[TI][TJ],SDL_Window *window,SDL_Renderer *renderer) 
{
  SDL_Rect
	  source = {0},                             // Rectangle définissant la zone de la texture à récupérer
	  //window_dimensions = {0},                  // Rectangle définissant la fenêtre, on  n'utilisera que largeur et hauteur
	  destination = {0};                        // Rectangle définissant où la zone_source doit être déposée dans le renderer

  float zoom = 1;   												// zoom, car ces images sont un peu petites
  //float zoom2=0;

  float offset_x = TAILLEFLAMX/TJ,   				// La largeur d'une vignette de l'image, marche car la planche est bien réglée
       	offset_y = TAILLEFLAMY/TI;           // La hauteur d'une vignette de l'image, marche car la planche est bien réglée

  int k=NBIT;

  while(k!=0)
  {
  	printf("%d\n",k );

  	int i=aleat(TI);
  	int j=aleat(TJ);

  	destination.w = offset_x * zoom;            // Largeur du sprite à l'écran
	  destination.h = offset_y * zoom;            // Hauteur du sprite à l'écran
	  destination.x = POSITIONX; 												// Position en x pour l'affichage du sprite
	  destination.y = POSITIONY;  											// Position en y pour l'affichage du sprite

	  source.w=offset_x;
	  source.h=offset_y;
	  source.x=tab[i][j].x;
	  source.y=tab[i][j].y;


  	play_with_texture_1(bg_texture, window, renderer);

  	SDL_RenderCopy(renderer, my_texture, &source, &destination);

  	SDL_RenderPresent(renderer);              // Affichage

  	SDL_Delay(TIME);

  	k--;
  }


	SDL_RenderClear(renderer);                  // Effacer la fenêtre avant de rendre la main
}


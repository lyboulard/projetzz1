#ifndef code_h
#define code_h

#include <SDL2/SDL.h>
//#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <SDL2/SDL_image.h>

typedef struct coordonnees
{
	float x;
	float y;
	int img;
}coordonnees;

#define TI 3
#define TJ 3

#define POSITIONX 240
#define POSITIONY 307

#define TAILLEFLAMX 755
#define TAILLEFLAMY 517

#define TAILLECHEMX 755
#define TAILLECHEMY 693

#define TIME 100*1

#define NBIT 200

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer);

void play_with_texture_1(SDL_Texture *my_texture, SDL_Window *window,SDL_Renderer *renderer);

void remplissage_coord(coordonnees[TI][TJ]);

void affichage_tabl (coordonnees [TI][TJ]);

int aleat(int lim);

void feu(SDL_Texture*,SDL_Texture*,coordonnees[TI][TJ],SDL_Window*,SDL_Renderer*); 




#endif
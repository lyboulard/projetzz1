#include "code.h"

void SDL_ExitError (const char * msg)
{
SDL_Log( "%s - %s\n", msg, SDL_GetError() );
SDL_Quit() ;
exit(EXIT_FAILURE);
}

/*********************************************************************************************************************/
/*                              Programme d'exemple de création de rendu + dessin                                    */
/*********************************************************************************************************************/
void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
             char const* msg,                                       // message à afficher
             SDL_Window* window,                                    // fenêtre à fermer
             SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];                                                         
  int l;                                                                          

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  }                                                                               

  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();                                                                     

  if (!ok) {                                                        // On quitte si cela ne va pas                
    exit(EXIT_FAILURE);                                                           
  }                                                                               
}                                                                                 


void drawinit(SDL_Renderer* renderer) {                                 // Je pense que vous allez faire moins laid :)
  
  SDL_Rect rectangle;                                                             
  SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);             // couleurs sur 3 paramètres, le dernier : 0 = transparent ; 255 = opaque
  rectangle.x = 0;                                                  // x haut gauche du rectangle
  rectangle.y = 0;                                                  // y haut gauche du rectangle
  rectangle.w = TAILLEW;                                                // sa largeur (w = width)
  rectangle.h = TAILLEH;                                                // sa hauteur (h = height)

  SDL_RenderFillRect(renderer, &rectangle);                         

  
}

//fonction gerant l'aléatoire
int aleat(int lim)
{
	//initialisation de l'aléatoire
	//srand(time(NULL));

	//génération du nb
	int a = rand()%lim;

	return a;
}


//fonction de dessin
void dessin(int type, int x, int y, SDL_Renderer* renderer)
{
	SDL_Rect rectangle;
	int a=aleat(TAILLEW/2);
	int b=aleat(TAILLEH/2);
	switch(type)
	{
		case 1:
			                                                             
		  SDL_SetRenderDrawColor(renderer, aleat(255), aleat(255), aleat(255), aleat(255));             
		  rectangle.x = x;                                                  // x haut gauche du rectangle
		  rectangle.y = y;                                                  // y haut gauche du rectangle
		  rectangle.w = aleat(TAILLEW/2);                                     // sa largeur (w = width)
		  rectangle.h = aleat(TAILLEH/2);
		  if (SDL_RenderDrawRect (renderer, &rectangle) != 0)
			{
				SDL_ExitError("Erreur lors du tracé du rectangle");
			}
			break;

		case 2:                                                           
		  SDL_SetRenderDrawColor(renderer, aleat(255), aleat(255), aleat(255), aleat(255));             
		  rectangle.x = x;                                                  // x haut gauche du rectangle
		  rectangle.y = y;                                                  // y haut gauche du rectangle
		  rectangle.w = aleat(TAILLEW/2);                                     // sa largeur (w = width)
		  rectangle.h = aleat(TAILLEH/2);
		  if (SDL_RenderFillRect (renderer, &rectangle) != 0)
			{
				SDL_ExitError("Erreur lors du tracé du rectangle");
			}
			break;

		case 3:
			SDL_SetRenderDrawColor(renderer,aleat(255),aleat(255),aleat(255),aleat(255));
			for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000) 
			{      
	    
	    SDL_RenderDrawPoint(renderer,                                   
	                        x + a * cos(angle),                     // coordonnée en x
	                        y + b * sin(angle));                    //            en y   
	  	}
			break;

		case 0:
			SDL_SetRenderDrawColor(renderer,aleat(255),aleat(255),aleat(255),aleat(255));
			for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000) 
			{      
	    SDL_RenderDrawPoint(renderer,                                   
	                        x + a* cos(angle),                     // coordonnée en x
	                        y + a* sin(angle));                    //            en y   
	  	}
			break;

	}

}





//fonction dessinant dans le renderer existant
void draw(SDL_Renderer* renderer) 
{
	SDL_Event event;

	SDL_bool run=SDL_TRUE;

  int x;
		  int y;
		  int type_de_forme;


  while (run) 
  {

    while (SDL_PollEvent(&event))
    {
    	switch(event.type)
    	{
    		case (SDL_KEYDOWN):
    			run =SDL_FALSE;
    			break;

    		case(SDL_MOUSEBUTTONDOWN):
    			
		  		SDL_GetMouseState(&x,&y);	

		  		type_de_forme=aleat(4);
		  		
		  		dessin(type_de_forme, x, y, renderer);

		  		SDL_RenderPresent(renderer);
		  		SDL_Delay(5);
		  		break;
    		
		  	default:
		  		break;
    	}

    }

  } 
}


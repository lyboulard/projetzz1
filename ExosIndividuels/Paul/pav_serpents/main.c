#include <SDL2/SDL.h>
//#include <math.h>
#include <stdio.h>
#include <string.h>

#include "code.h"

int main(int argc, char **argv) 
{
  SDL_Window* window = NULL;
  SDL_Renderer* renderer = NULL;

  SDL_DisplayMode screen;

  srand(time(NULL));

  /*********************************************************************************************************************/  
  /*                            Initialisation de la SDL  + gestion de l'échec possible                                */
  /*********************************************************************************************************************/

  if (SDL_Init(SDL_INIT_VIDEO) != 0)
  {
    end_sdl(0, "ERROR SDL INIT", window, renderer);
  }

  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n",
         screen.w, screen.h);

  
  /* Création de la fenêtre */
  window = SDL_CreateWindow("Premier dessin",SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, TAILLEW, TAILLEH, SDL_WINDOW_OPENGL);
  
  if (window == NULL)
  {
    end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
  }

  
  /* Création du renderer */
  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

  if (renderer == NULL)
  {
    end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
  }


  /*********************************************************************************************************************/
  /*                                        On dessine dans le renderer                                                */
  /*********************************************************************************************************************/

  drawinit(renderer);                                      // appel de la fonction qui crée l'image  
  SDL_RenderPresent(renderer);                         // affichage

  printf("Cliquer pour faire apparaitre une forme\n");
  printf("Appuyer sur une touche pour quitter.\n");
  draw(renderer);                                      //modification de la toile
  


  /* on referme proprement la SDL */
  end_sdl(1, "Normal ending", window, renderer);
  return EXIT_SUCCESS;

}
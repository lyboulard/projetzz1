#ifndef code_h
#define code_h

#include <SDL2/SDL.h>
//#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define TAILLEH 600
#define TAILLEW 1200

void SDL_ExitError (const char * );

void end_sdl(char , char const* , SDL_Window* , SDL_Renderer* );

void drawinit(SDL_Renderer* );

int aleat(int );

void dessin(int , int , int , SDL_Renderer* );

void draw(SDL_Renderer* ) ;




#endif
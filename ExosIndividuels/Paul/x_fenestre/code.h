#ifndef code_h
#define code_h

#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define TAILLE 100 

void end_sdl(char , char const* , SDL_Window* , SDL_Renderer* );


void draw(SDL_Renderer* );


int positions(int , int , int , int );


void mvt(SDL_Window* , SDL_DisplayMode );


int aleat(int );


void aleat_fen(SDL_Window* , SDL_DisplayMode, SDL_Renderer*);



#endif
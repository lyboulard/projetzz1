#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "code.h"

int main(int argc, char **argv) 
{
  

  SDL_Window* window = NULL;
  SDL_Renderer* renderer = NULL;

  SDL_DisplayMode screen;

  srand(time(NULL));

  /*********************************************************************************************************************/  
  /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
  /*********************************************************************************************************************/
  
  if (SDL_Init(SDL_INIT_VIDEO) != 0)
  {
    end_sdl(0, "ERROR SDL INIT", window, renderer);
  }

  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n",
         screen.w, screen.h);

  
  /* Création de la fenêtre */
  //window = SDL_CreateWindow("Premier dessin", 0, 0, TAILLE, TAILLE, SDL_WINDOW_OPENGL);
  //if (window == NULL)
  //{
  //  end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
  //}

  
   /* Création du renderer */
  //renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

  //if (renderer == NULL)
  //{
  // end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
  //}


  /*********************************************************************************************************************/
  /*                                     On dessine dans le renderer                                                   */
  /*********************************************************************************************************************/

  draw(renderer);                                      // appel de la fonction qui crée l'image  
  SDL_RenderPresent(renderer);                         // affichage

  //SDL_Delay(100*60*1);                                 // Pause exprimée en ms


  //fonction faisant bouger une fenetre en diagonale et rebondissant sur les bords
  //il y a des problèmes en fonction de la taille de l'écran utilisé ainsi que des problèmes de rebonds
  //la fonctionne fait actuellement que 4 rebonds
  //mvt(window,screen);  

  //fonction qui fait apparaitre une fenetre aléatoirement
  aleat_fen(window, screen, renderer);

  //SDL_Delay(100*60*2); //deux minutes

  /* on referme proprement la SDL */
  end_sdl(1, "Normal ending", window, renderer);
  return EXIT_SUCCESS;
}

#include "code.h"

/*********************************************************************************************************************/
/*                              Programme d'exemple de création de rendu + dessin                                    */
/*********************************************************************************************************************/
void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
             char const* msg,                                       // message à afficher
             SDL_Window* window,                                    // fenêtre à fermer
             SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];                                                         
  int l;                                                                          

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  }                                                                               

  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();                                                                     

  if (!ok) {                                                        // On quitte si cela ne va pas                
    exit(EXIT_FAILURE);                                                           
  }                                                                               
}                                                                                 


void draw(SDL_Renderer* renderer) {                                 // Je pense que vous allez faire moins laid :)
  SDL_Rect rectangle;                                                             

  SDL_SetRenderDrawColor(renderer,                                                
                         50, 0, 0,                                  // mode Red, Green, Blue (tous dans 0..255)
                         255);                                      // 0 = transparent ; 255 = opaque
  rectangle.x = 0;                                                  // x haut gauche du rectangle
  rectangle.y = 0;                                                  // y haut gauche du rectangle
  rectangle.w = 400;                                                // sa largeur (w = width)
  rectangle.h = 400;                                                // sa hauteur (h = height)

  SDL_RenderFillRect(renderer, &rectangle);                         

  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);                   

}

int positions(int w, int h, int maxw, int maxh)
{
	int pos=-1;

	if (w==0)
	{
		pos=1;
	}
	else
	{
		if(h==0)
		{
			pos=4;
		}
		else
		{
			if(w==maxw)
			{
				pos=3;
			}
			else
			{
				if(h==maxh)
				{
					pos=2;
				}
			}
		}
	}

	return pos;
}


//fonction changeant le mouvement de la fenetre
void mvt(SDL_Window* window, SDL_DisplayMode screen)
{
	//initialisation des variables de position
	int h=0;
	int w=0;
	SDL_SetWindowPosition(window, w, h);
	//printf("TAILLE %d\n", TAILLE);
	int x=21;
	int t=25;

	int position=1; //1 gauche, 2 bas, 3 droit, 4 haut

	while (position>0)
	{
		switch (position)
		{
			case 1:
				//boucle tant que la fenetre n'est pas au bout de l'écran (en bas)
				while( (h+TAILLE+31*2)<=screen.h)
				{
					//récupération de la position de la fenetre
					//SDL_GetWindowPosition(window, &w, &h);
					//printf("valeur %d %d %d\n",screen.h,w, h);

					//incrémentation de h et w
					h=h+x;
					w=w+x;
					//printf("valeur2 %d %d %d\n",screen.h,w, h);
					//changement de la position
					SDL_SetWindowPosition(window, w, h);

					SDL_Delay(t);                                 // Pause exprimée en ms
				}
				position=positions(w,h,screen.w-156,screen.h-156);
				break;
			
			case 2:
				//boucle tant que la fenetre n'est pas au bout de l'écran (à droite)
				while( (w+TAILLE)<=screen.w)
				{
					//récupération de la position de la fenetre
					//SDL_GetWindowPosition(window, &w, &h);

					//incrémentation de h et w
					h=h-x;
					w=w+x;

					//changement de la position
					SDL_SetWindowPosition(window, w, h);
				
				SDL_Delay(t);                                 // Pause exprimée en ms
				
				}
				position=positions(w+93,h,screen.w,screen.h);
				break;
							
			case 3:
				//boucle tant que la fenetre n'est pas au bout de l'écran (en haut)
				while(h>=0)
				{
					//récupération de la position de la fenetre
					//SDL_GetWindowPosition(window, &w, &h);

					//incrémentation de h et w
					h=h-x;
					w=w-x;

					//changement de la position
					SDL_SetWindowPosition(window, w, h);

				SDL_Delay(t);                                 // Pause exprimée en ms

				}
				position=positions(w,h+21,screen.w,screen.h);
				break;
			
			case 4:
				//boucle tant que la fenetre n'est pas au bout de l'écran (à gauche)
				while(h+2*TAILLE<screen.h)
				{
					//récupération de la position de la fenetre
					//SDL_GetWindowPosition(window, &w, &h);

					//incrémentation de h et w
					h=h+x;
					w=w-x;

					//changement de la position
					SDL_SetWindowPosition(window, w, h);

				SDL_Delay(t);                                 // Pause exprimée en ms

				}
				position=positions(w,h,screen.w,screen.h-198);
				//printf("position %d", position);
				//printf("valeur %d %d\n",w, h);
				//int a;
				//int b;
				//SDL_GetWindowPosition(window, &a, &b);
				//printf("valeur %d %d \n",a, b);	
				break;
				
			
			default:
				break;
		}
	}	
				

}

//fonction renvoyant un nombre aléatoire
int aleat(int lim)
{
	//initialisation de l'aléatoire
	//srand(time(NULL));

	//génération du nb
	int a = rand()%lim;

	return a;
}


void aleat_fen(SDL_Window* window, SDL_DisplayMode screen, SDL_Renderer* renderer)
{
	int i=100;

	while (i>0)
	{
		int h=aleat(screen.h);
		int w=aleat(screen.w);

		window = SDL_CreateWindow("Premier dessin", w, h, TAILLE, TAILLE, SDL_WINDOW_OPENGL);
	  if (window == NULL)
	  {
	    end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
	  }

	  
	  /* Création du renderer */
	  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	  if (renderer == NULL)
	  {
	    end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
	  }

	  draw(renderer);                                      // appel de la fonction qui crée l'image  
	  SDL_RenderPresent(renderer);                         // affichage

	  SDL_Delay(2);

	  i--;
	}
	
}

#include <SDL2/SDL.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <SDL2/SDL_image.h>

void SDL_ExitErrorPlus (const char * msg, SDL_Renderer * rend, SDL_Window * win)
{
  SDL_DestroyRenderer(rend);
  SDL_DestroyWindow(win);    
  SDL_Log( "%s - %s\n", msg, SDL_GetError() );                
  SDL_Quit() ;       
  exit(EXIT_FAILURE);
}


int main(int argc, char **argv) 
{
  /* Initialisation */

  (void)argc;
  (void)argv;

  SDL_Window * window = NULL;
  SDL_Renderer * renderer = NULL;

  srand(time(NULL));

  /* Initialisation de la SDL */

  if (SDL_Init(SDL_INIT_VIDEO) != 0)
  {SDL_ExitErrorPlus("Problème d'initialisation de la SDL\n",renderer, window);}

  /* Récupération dimensions écran */

  SDL_DisplayMode screen;
  SDL_GetCurrentDisplayMode(0, &screen);

  /* Création de la fenêtre */

  window = SDL_CreateWindow("Combat", SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, 500, 500, 0);
  if (window == NULL)
  {SDL_ExitErrorPlus("Erreur Fenetre\n",renderer, window);}


  /* Création du renderer */

  renderer = SDL_CreateRenderer(window, -1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) 
  {SDL_ExitErrorPlus("Erreur Renderer\n",renderer, window);}


  /* Initialisation de la texture */

  SDL_Texture * perso, * background;
  
  perso = IMG_LoadTexture(renderer, "./img/Luffy.png");
  if(perso == NULL)
  {SDL_ExitErrorPlus("Echec de chargement d'image",renderer,window);} 

  background =  IMG_LoadTexture(renderer, "./img/background.png");
  if(perso == NULL)
  {SDL_ExitErrorPlus("Echec de chargement d'image",renderer,window);}  
  
   /* On sépare en petit carreau le sprite */
  
  SDL_Rect rectbg, rectperso, rectperso2, rectperso3, rectperso4, rect;

  rectbg.x = 0;
  rectbg.y = 0;
  rectbg.w = 500;
  rectbg.h = 500;
  
  rectperso.x = 20;
  rectperso.y = 840;
  rectperso.w = 180;
  rectperso.h = 200;

  rectperso2.x = 300;
  rectperso2.y = 840;
  rectperso2.w = 180;
  rectperso2.h = 200;

  rectperso3.x = 550;
  rectperso3.y = 500;
  rectperso3.w = 180;
  rectperso3.h = 200;

  rectperso4.x = 30;
  rectperso4.y = 260;
  rectperso4.w = 160;
  rectperso4.h = 200;

  rect.w = 180;
  rect.h = 200;
  rect.x = rectbg.w /2 - rect.w;
  rect.y = rectbg.h /2;
  
  /* On charge les textures */
  if (SDL_QueryTexture(background, NULL, NULL, &rectbg.w, &rectbg.h) != 0)
  {SDL_ExitErrorPlus("Erreur lors du chargement de la texture en mémoire", renderer, window);}

  if (SDL_QueryTexture(perso, NULL, NULL, NULL,NULL) != 0)
  {SDL_ExitErrorPlus("Erreur lors du chargement de la texture en mémoire", renderer, window);}

  SDL_bool run = SDL_TRUE;
  int i = 0;

  while(run)
  {
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
      switch(event.type)
      {
        case SDL_QUIT :
          run = SDL_FALSE;
        break;
      }
    }

    SDL_RenderClear(renderer);
    /* On colle les texture sur le renderer */
    SDL_RenderCopy(renderer, background, &rectbg, NULL);
    
    switch(i)
    {
      case 0 :
        SDL_RenderCopy(renderer, perso, &rectperso3, &rect);
      break;

      case 1 :
        SDL_RenderCopy(renderer, perso, &rectperso, &rect);
      break;

      case 2 :
        SDL_RenderCopy(renderer, perso, &rectperso2, &rect);
      break;
      
      case 3 :
        while (rect.x != rectbg.w)
        {
          rect.x += 2;
          SDL_RenderClear(renderer);
          SDL_RenderCopy(renderer, background, &rectbg, NULL);
          SDL_RenderCopy(renderer, perso, &rectperso4, &rect);
          SDL_RenderPresent(renderer);
        }
      break;

      default:
      break;
    }

    rect.x = rectbg.w /2;
    i++;
    if (i == 4)
    {
      i = 0;
    }

    /* Affichage renderer */
    SDL_RenderPresent(renderer);
    SDL_Delay(250);
  }

  SDL_Delay(3000);


  SDL_DestroyTexture(perso);
  IMG_Quit();  
  return 0;
}
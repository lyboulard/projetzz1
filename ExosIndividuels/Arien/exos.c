#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/********************************************/
/* Vérification de l'installation de la SDL */
/********************************************/


//gcc exos.c -Wall -Wextra -o prog $(sdl2-config --cflags --libs)

void SDL_ExitError (const char * msg)
{
    SDL_Log( "%s - %s\n", msg, SDL_GetError() );                
    SDL_Quit() ;       
    exit(EXIT_FAILURE);
}

void SDL_ExitErrorPlus (const char * msg, SDL_Renderer * rend, SDL_Window * win)
{
    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(win);    
    SDL_Log( "%s - %s\n", msg, SDL_GetError() );                
    SDL_Quit() ;       
    exit(EXIT_FAILURE);
}


void dessiner()
{   
    SDL_DisplayMode dim;
    SDL_GetCurrentDisplayMode(0, &dim);
    int window_x, window_y;
    window_x = 0.75*dim.w;
    window_y = 0.75*dim.h;
    SDL_Window * win = SDL_CreateWindow("Fenetre", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_x, window_y, 0);
    
    int i, l, h, x, y, choice, c1, c2, c3, compteur, pas, limite;
    SDL_bool run = SDL_TRUE;
    SDL_GetWindowSize(win, &l, &h);
    x = l/2;
    y = h/2;
    c1 = 255;
    c2 = c3 = 0;
    compteur = 0;
    pas = 7;
    limite = 20000;

    SDL_Renderer * renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {SDL_ExitError("Erreur de rendu");}


    SDL_SetRenderDrawColor(renderer,c1,c2,c3,255);
    
    srand(time(NULL));

    while (compteur < limite && run == SDL_TRUE)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT :
                run = SDL_FALSE;
                break;
            }
        }

        if (compteur == limite)
        {
            printf("C'est gagné\n");
            run = SDL_FALSE;
        }
        choice = rand() % 4;
        compteur++;
        switch(choice)
        {
            case 0 :
                for(i=0; i < pas; i++)
                {
                    x++;
                }    
            break;

            case 1 :
                for(i=0; i < pas; i++)
                {
                    y++;
                }
            break;

            case 2 :
                for(i=0; i < pas; i++)
                {
                    x--;
                }    
            break;

            case 3 :
                for(i=0; i < pas; i++)
                {
                    y--;
                }
            break;

            default :
            break;
        }
        
        if (x < 0)
        {
            x += window_x;
        }

        else if (x > window_x)
        {
            x -= window_x;
        }

        if (y < 0)
        {
            y += window_y;
        }

        else if (y > window_y)
        {
            y -= window_y;
        }

        if (SDL_RenderDrawPoint (renderer, x, y) !=0)
        {SDL_ExitError("Erreur lors du tracé de la ligne");}
        
        if (c1 == 255 && c2 < 255)
        {
            c2++;
        }
        else if(c1 > 0 && c2 == 255)
        {
            c1--;
        }
        else if (c1 == 0 && c2 == 255 && c3 < 255)
        {
            c3++;
        }
        else if(c1 == 0 && c2 > 0 && c3 == 255)
        {
            c2--;
        }
        else if (c1 < 255 && c2 == 0 && c3 == 255)
        {
            c1++;
        }

        else if (c1 == 255 && c3 == 255 && c2 == 0)
        {
            c1 = 255;
            c2 = c3 = 0;
        }

        SDL_RenderPresent(renderer);
        SDL_Delay(1);
        SDL_SetRenderDrawColor(renderer,c1,c2,c3,255);
    }
    
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow (win);
}

void exoFenetre ()
{
    SDL_Window * Win[8];
    SDL_Renderer * renderer[8];    
    SDL_DisplayMode dimensions;
    SDL_GetCurrentDisplayMode(0, &dimensions);
    int h,l,i,c1,c2,c3;
    int tab[8][2];
    l = dimensions.w / 5; //640
    h = dimensions.h / 5; //1340
    printf("Dimensions de l'écran : %d * %d\n", dimensions.w, dimensions.h);
    
    Win[0] = SDL_CreateWindow("F1", 0, 0, l, h, 0);
    Win[1] = SDL_CreateWindow("F2", (dimensions.w - l)/ 2, 0, l, h, 0);
    Win[2] = SDL_CreateWindow("F3", dimensions.w - l, 0, l, h, 0);
    Win[3] = SDL_CreateWindow("F4", 0, (dimensions.h - h)/ 2, l, h, 0);
    Win[4] = SDL_CreateWindow("F5", dimensions.w - l, (dimensions.h - h)/2, l, h, 0);
    Win[5] = SDL_CreateWindow("F6", 0, dimensions.h - h, l, h, 0);
    Win[6] = SDL_CreateWindow("F7", (dimensions.w - l)/ 2, (dimensions.h - h), l, h, 0);
    Win[7] = SDL_CreateWindow("F8", dimensions.w - l, (dimensions.h - h), l, h, 0);

    for (i = 0; i < 8; i++)
    {
        renderer[i] = SDL_CreateRenderer(Win[i], -1, SDL_RENDERER_ACCELERATED);
        if (renderer[i] == NULL) {SDL_ExitError("Erreur de rendu");}
    }

    SDL_Rect rectangle [8];
    for (i = 0; i < 8; i++)
    {
        rectangle[i].x = 0;
        rectangle[i].y = 0;
        rectangle[i].w = l;
        rectangle[i].h = h;
        c1 = rand() % 256;
        c2 = rand() % 256;
        c3 = rand() % 256;

        if (SDL_SetRenderDrawColor (renderer[i], c1,c2,c3,255) != 0)
        {SDL_ExitError("Erreur lors de la mise à jour de la couleur du tracé");}

        if (SDL_RenderFillRect (renderer[i], &rectangle[i]) != 0)
        {SDL_ExitError("Erreur lors du tracé du rectangle");}

        SDL_RenderPresent(renderer[i]);
    }

    SDL_bool run = SDL_TRUE;
    while (run)
    {
        for (i=0; i < 8; i++) //On récupère la position des fenetres
        {
            SDL_GetWindowPosition(Win[i], &tab[i][0], &tab[i][1]);
        }
        
        //Déplacement F1
        if (tab[0][0] < dimensions.w - l) 
        {
            SDL_SetWindowPosition(Win[0], tab[0][0] + 1, tab[0][1]);
        }

        //Déplacement F2
        if (tab[1][0] < dimensions.w - l)
        {
            SDL_SetWindowPosition(Win[1], tab[1][0] + 1, tab[1][1]);
        }

        if (tab[1][0] == dimensions.w - l)
        {
            if (tab[1][1] < (dimensions.h - h)/2)
            {
                SDL_SetWindowPosition(Win[1], tab[1][0], tab[1][1] + 1);
            }
        }

        //Déplacement F3
        if (tab[2][1] < dimensions.h - h)
        {
            SDL_SetWindowPosition(Win[2], tab[2][0], tab[2][1] + 1);
        }

        //Déplacement F4
        if (tab[3][1] > 0)
        {
            SDL_SetWindowPosition(Win[3], tab[3][0], tab[3][1] - 1);
        }

        if (tab[3][1] == 0)
        {
            if (tab[3][0] < (dimensions.w - l)/2)
            {
                SDL_SetWindowPosition(Win[3], tab[3][0] + 1, tab[3][1]);
            }
        }

        //Déplacement F5
        if (tab[4][1] < dimensions.h - h)
        {
            SDL_SetWindowPosition(Win[4], tab[4][0], tab[4][1] + 1);
        }

        if (tab[4][1] == dimensions.h - h)
        {
            if (tab[4][0] > (dimensions.w - l)/2)
            {
                SDL_SetWindowPosition(Win[4], tab[4][0] - 1, tab[4][1]);
            }
        }

        //Déplacement F6
        if (tab[5][1] > 0)
        {
            SDL_SetWindowPosition(Win[5], tab[5][0], tab[5][1] - 1);
        }

        //Déplacement F7
        if (tab[6][0] > 0)
        {
            SDL_SetWindowPosition(Win[6], tab[6][0] - 1, tab[6][1]);
        }

        if (tab[6][0] == 0)
        {
            if (tab[6][1] > (dimensions.h - h)/2)
            {
                SDL_SetWindowPosition(Win[6], tab[6][0], tab[6][1] - 1);
            }
        }        

        //Déplacement F8
        if (tab[7][0] > 0)
        {
            SDL_SetWindowPosition(Win[7], tab[7][0] - 1, tab[7][1]);
        }

        SDL_Delay(3);
        SDL_GetWindowPosition(Win[0], &tab[0][0], &tab[0][1]);

        if (tab[0][0] == dimensions.w - l)
        {
            run = SDL_FALSE;
        }
    }

    /* On ferme tous les renderer */
    for (i = 0; i < 8; i++)
    {
        SDL_DestroyRenderer(renderer[i]);
    }

    /* On ferme toutes les fenetres */
    for(i=0; i < 8; i++)
    {
        SDL_DestroyWindow(Win[i]);
    }         
}

int main() 
{ 
    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {SDL_ExitError("Erreur d'initialisation de la SDL");}

    SDL_version v;
    SDL_VERSION(&v);
    printf("Version de la SDL : %d.%d.%d\n", v.major, v.minor, v.patch);

    int choice;
    printf("Veuillez choisir quelle fonction exécutée :\n\n1. exoFenetre\n2. dessiner\n\n");
    scanf("%d", &choice);

    switch(choice)
    {
        case 1 :
            exoFenetre ();
        break;

        case 2 :
            dessiner();
        break;

        default :
        break;
    }
    SDL_Quit();  
    return 0;
}
/* -------------------------------------------

Projet ZZ1 - ISIMA - Juin 2022
    - Manipulation de fenêtres : fenêtres tournantes -
    Quatre fenêtres sont créées en haut/bas/gauche/droite et tournent
    en cercle pendant un demi-tour.
Lyloo Boulard

-------------------------------------------- */

#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>

#define PI 3.14159265358979323846

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    int i,j;
    
    /* --------------------------------------
    Vérification de l'installation de la SDL */
    
    SDL_version nb;
    SDL_VERSION(&nb);
    
    printf("Version de la SDL : %d.%d.%d\n", nb.major, nb.minor, nb.patch);

    /* Fin de la vérification de l'installation de la SDL
    -------------------------------------- */


    if(SDL_Init(SDL_INIT_VIDEO) != 0) // Initialisation de la SDL
    {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode screen;
    SDL_GetCurrentDisplayMode(0, &screen);


    /* --------------------------------------
    Initialisation des fenêtres */
    
    SDL_Window** TabWindows = (SDL_Window**) malloc(4*sizeof(SDL_Window*));

    for(i=0;i<4;i++)
    {
        TabWindows[i] = SDL_CreateWindow(
        "Fenêtre",
        (screen.w)/2 + (screen.h / 2)*cos(i*PI/2) - 200,
        (screen.h / 2)*sin(i*PI/2) + (screen.h)/2 - 150,
        400, 300,
        0
        );

        if(TabWindows[i] == NULL)
        {
            SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());
            SDL_Quit();
            exit(EXIT_FAILURE);
        }
    }

    /* Fin d'initialisation des fenêtres
    -------------------------------------- */


    for(i=0;i<200;i++) // Boucle de rotation
    {
        for(j=0;j<4;j++)
        {
            SDL_SetWindowPosition(TabWindows[j],
            (screen.w)/2 + (screen.h / 2)*cos(j*PI/2 + i*PI/200) - 200,
            (screen.h / 2)*sin(j*PI/2 + i*PI/200) + (screen.h)/2 - 150);
        }
        SDL_Delay(10);
    }


    /* --------------------------------------
    Destruction des fenêtres et fermeture de la SDL */
    
    SDL_Delay(1000);
    
    for(i=0;i<4;i++)
    {
        SDL_DestroyWindow(TabWindows[i]);
    }
    free(TabWindows);

    SDL_Quit();

    /* Fin de la destruction des fenêtres et fermeture de la SDL
    -------------------------------------- */
    

    return 0;
}
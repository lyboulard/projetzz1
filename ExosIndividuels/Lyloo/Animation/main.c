/* ------------------------------------------------------------------------------

Projet ZZ1 - ISIMA - Juin 2022
    - Animation simple : pong like -
    Deux rectangles se déplacent de haute en bas et se "renvoient" une balle
Lyloo Boulard

------------------------------------------------------------------------------ */

#include <SDL2/SDL.h>
#include <stdio.h>
#include "anim.h"

int main(int argc, char** argv)
{
    (void)argc;
    (void)argv;
	int i = 0; int j;

  	SDL_Window* window = NULL;
  	SDL_Renderer* renderer = NULL;
	int wwindow; int hwindow;

  	if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer); 						// Initialisation de la SDL

  	SDL_DisplayMode screen;
	SDL_GetCurrentDisplayMode(0, &screen);


  	/* ---------------Création de la fenêtre--------------- */

  	window = SDL_CreateWindow("Fenêtre",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                            screen.h * 0.66,
                            SDL_WINDOW_OPENGL);
  	if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

	SDL_GetWindowSize(window, &wwindow, &hwindow);


  	/* ---------------Création du renderer--------------- */

  	renderer = SDL_CreateRenderer(window, -1,
                                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  	if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

	
	int** couleurs = initCouleurs();
	j = 0;																									// La couleur initiale est le rouge


  	/*  Appel de la fonction alternative. Dans ce cas, la boucle d'event doit être supprimée */

	// draw(renderer, wwindow, hwindow);


	SDL_bool on = SDL_TRUE;
	SDL_Event event;

	while(on)
	{
		if(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_QUIT:
					on = SDL_FALSE;
					break;
				
				case SDL_MOUSEBUTTONDOWN: 																	// La couleur est modifiée
					if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
					{
						j = (j+1)%6;
					}
					break;
				
				default :
					break;
			}
		}

		/*  Les 200 premières itérations sont celles de la première diagonale
			Les 200 suivantes celles de l'animation horizontale
	  		Les 200 dernières celles de la dernière diagonale */
			
		if(i<200)
		{
			mouv1(renderer, hwindow, wwindow, i, couleurs[j][0], couleurs[j][1], couleurs[j][2]);
		}
		else
		{
			if (i<400)
			{
				mouv2(renderer, hwindow, wwindow, i-200, couleurs[j][0], couleurs[j][1], couleurs[j][2]);
			}
			else
			{
				mouv3(renderer, hwindow, wwindow, i-400, couleurs[j][0], couleurs[j][1], couleurs[j][2]);
			}
		}
		i++;

		if(i == 599)
		{
			on = SDL_FALSE;
		}
	}
	
  	SDL_Delay(5000);


	libereCouleurs(couleurs);
  	end_sdl(1, "Normal ending", window, renderer);

  	return EXIT_SUCCESS;
}
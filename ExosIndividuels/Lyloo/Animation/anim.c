#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/* ------------------------------------------------------------------------------
Termine l'utilisation de la SDL
Paramètres : 	ok 			- booléen indiquant si le programme doit se terminer
							à cause d'une erreur ou de façon normale
				msg 		- message à afficher si erreur
				window 		- pointeur sur la fenêtre à supprimer
				renderer 	- pointeur sur le SDL_Renderer à supprimer
------------------------------------------------------------------------------ */

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer)
{
  	char msg_formated[255];                                                         
  	int l;                                                                          

  	if (!ok)
	{                                                        												// Cas d'erreur
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  	}                                                                               

  	if (renderer != NULL)
	{                                           															// Destruction si nécessaire du renderer
    	SDL_DestroyRenderer(renderer);                     													// Attention : on suppose que les NULL sont maintenus !!
    	renderer = NULL;
  	}
  	if (window != NULL)  
	{                                           															// Destruction si nécessaire de la fenêtre
    	SDL_DestroyWindow(window);                          												// Attention : on suppose que les NULL sont maintenus !!
    	window= NULL;
  	}

    	SDL_Quit();                                                                     

  	if (!ok)
	{                                                        												// On quitte si cela ne va pas                
    	exit(EXIT_FAILURE);                                                           
  	}                                                                               
}                                                                                 




/* ------------------------------------------------------------------------------
Effectue le mouvement diagonal haut-gauche -> bas-droite du cercle
Paramètres : 	renderer 	- pointeur sur le SDL_Renderer sur lequel vont être
						   	situés nos objets
				h 			- hauteur de l'écran
				w 			- largeur de l'écran
				i 			- simule l'indice de boucle, où nous en sommes dans
							l'animation
				r 			- composante rouge de la couleur des formes
				g 			- composante verte de la couleur des formes
				b 			- composante bleue de la couleur des formes
------------------------------------------------------------------------------ */

void mouv1(SDL_Renderer* renderer, int h, int w, int i, int r, int g, int b)
{
	SDL_Rect rec1;
  	SDL_Rect rec2;

	rec1.x = 0.1*w; rec1.y = 0.1*h;                                                                         // Placement des rectangles en haut à gauche/droite
  	rec1.w = 100; rec1.h = 200;
	rec2.x = 0.85*w; rec2.y = 0.1*h + 0.6*h*i/200;                                                          // rec2 se déplace verticalement
  	rec2.w = 100; rec2.h = 200;

	SDL_SetRenderDrawColor(renderer,   																		// Effacement de l'écran                                             
                     0, 0, 0,
                     255);
						 
	SDL_RenderClear(renderer);

	SDL_SetRenderDrawColor(renderer,                                                
                         r, g, b,
                         255);


	for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000)                                           // Tracé de la balle
	{
		SDL_RenderDrawLine(renderer,                                   
                0.1*w + 120 + (0.8*w - 200)*i/200,
				0.1*h + 120 + (0.8*h - 200)*i/200,
				0.1*w + 120 + (0.8*w - 200)*i/200 + 20 * cos(angle),
                0.1*h + 120 + (0.8*h - 200)*i/200 + 20 * sin(angle));
	}

	SDL_RenderFillRect(renderer, &rec2);                                                                    // Tracé des rectangles
	SDL_RenderFillRect(renderer, &rec1);

	SDL_RenderPresent(renderer);
}




/* ------------------------------------------------------------------------------
Effectue le mouvement horizontal bas-droite -> bas-gauche du cercle
Paramètres : 	renderer 	- pointeur sur le SDL_Renderer sur lequel vont être
						   	situés nos objets
				h 			- hauteur de l'écran
				w 			- largeur de l'écran
				i 			- simule l'indice de boucle, où nous en sommes dans
							l'animation
				r 			- composante rouge de la couleur des formes
				g 			- composante verte de la couleur des formes
				b 			- composante bleue de la couleur des formes
------------------------------------------------------------------------------ */

void mouv2(SDL_Renderer* renderer, int h, int w, int i, int r, int g, int b)
{
	SDL_Rect rec1;
  	SDL_Rect rec2;
	
	rec1.x = 0.1*w; rec1.y = 0.1*h + 0.6*h*i/200;
  	rec1.w = 100; rec1.h = 200;
	rec2.x = 0.85*w; rec2.y = 0.7*h;
  	rec2.w = 100; rec2.h = 200;
	
	SDL_SetRenderDrawColor(renderer,                                                
                         0, 0, 0,
                         255);
						 
	SDL_RenderClear(renderer);

	SDL_SetRenderDrawColor(renderer,                                                
                         r, g, b,
                         255);


	for(float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000)
	{
		SDL_RenderDrawLine(renderer,                                   
                0.9*w - 80 - (0.8*w - 200)*i/200,
				0.9*h - 80,
				0.9*w - 80 - (0.8*w - 200)*i/200 + 20 * cos(angle),
                0.9*h - 80 + 20 * sin(angle));
  	}

	SDL_RenderFillRect(renderer, &rec2);
	SDL_RenderFillRect(renderer, &rec1);

	SDL_RenderPresent(renderer);
}




/* ------------------------------------------------------------------------------
Effectue le mouvement diagonal bas-gauche -> haut-droite du cercle
Paramètres : 	renderer 	- pointeur sur le SDL_Renderer sur lequel vont être
						   	situés nos objets
				h 			- hauteur de l'écran
				w 			- largeur de l'écran
				i 			- simule l'indice de boucle, où nous en sommes dans
							l'animation
				r 			- composante rouge de la couleur des formes
				g 			- composante verte de la couleur des formes
				b 			- composante bleue de la couleur des formes
------------------------------------------------------------------------------ */

void mouv3(SDL_Renderer* renderer, int h, int w, int i, int r, int g, int b)
{
	SDL_Rect rec1;
  	SDL_Rect rec2;
	
	rec1.x = 0.1*w; rec1.y = 0.7*h;
  	rec1.w = 100; rec1.h = 200;
	rec2.x = 0.85*w; rec2.y = 0.7*h;
  	rec2.w = 100; rec2.h = 200;
	
	SDL_SetRenderDrawColor(renderer,                                                
                     0, 0, 0,
                     255);
					 
	SDL_RenderClear(renderer);

	SDL_SetRenderDrawColor(renderer,                                                
                     r, g, b,
                     255);


	for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000)
	{
		SDL_RenderDrawLine(renderer,                                   
                0.1*w + 120 + (w - 200)*i/200,
				0.9*h - 80 - (0.9*h - 200)*i/200,
				0.1*w + 120 + (w - 200)*i/200 + 20 * cos(angle),
                0.9*h - 80 - (0.9*h - 200)*i/200 + 20 * sin(angle));
  	}

	SDL_RenderFillRect(renderer, &rec2);
	SDL_RenderFillRect(renderer, &rec1);

	SDL_RenderPresent(renderer);
}




/* ------------------------------------------------------------------------------
Initialise le tableau à 2 dimensions contenant le code rgb des couleurs possibles.
Retourne un double pointeur représentant le tableau
------------------------------------------------------------------------------ */

int** initCouleurs()
{
	int** couleurs = (int**) malloc(6*sizeof(int*));
    int i;
    
	for(i=0;i<6;i++)
	{
		couleurs[i] = (int*) malloc(3*sizeof(int));
	}

	couleurs[0][0] = 255;
	couleurs[0][1] = 0;
	couleurs[0][2] = 0;

	couleurs[1][0] = 255;
	couleurs[1][1] = 127;
	couleurs[1][2] = 0;

	couleurs[2][0] = 255;
	couleurs[2][1] = 255;
	couleurs[2][2] = 0;

	couleurs[3][0] = 0;
	couleurs[3][1] = 255;
	couleurs[3][2] = 0;

	couleurs[4][0] = 0;
	couleurs[4][1] = 0;
	couleurs[4][2] = 255;

	couleurs[5][0] = 155;
	couleurs[5][1] = 38;
	couleurs[5][2] = 182;

	return couleurs;
}




/* ------------------------------------------------------------------------------
Libère le tableau des couleurs
Paramètres : couleurs - pointeur sur le tableau à libérer
------------------------------------------------------------------------------ */

void libereCouleurs(int** couleurs)
{
	int i;
	for(i=0;i<6;i++)
	{
		free(couleurs[i]);
	}
	free(couleurs);
}




/* ------------------------------------------------------------------------------
Effectue l'animation des formes
Paramètres : 	renderer 	- le SDL_Renderer sur lequel vont être situés nos
							objets
				h 			- hauteur de l'écran
				w 			- largeur de l'écran
Version alternative en une seule fonction, mais qui ne permet pas de manipuler
les couleurs : une fois que la fonction est lancée on ne retourne pas dans le
gestionnaire d'évènements avant sa fin. L'autre version est composée de fonctions
représentant un seul tour de boucle de cette fonction-ci, afin de retourner dans
le main après chaque mouvement
------------------------------------------------------------------------------ */

/*void draw(SDL_Renderer* renderer, int w, int h)
{                                                            
  	SDL_Rect rec1;
  	SDL_Rect rec2;
	int i;

	rec1.x = 0.1*w; rec1.y = 0.1*h;
  	rec1.w = 100; rec1.h = 200;
	rec2.x = 0.85*w; rec2.y = 0.1*h;
  	rec2.w = 100; rec2.h = 200;

	for(i=0;i<200;i++)																						// Premier mouvement
	{
		SDL_SetRenderDrawColor(renderer,   																	// Effacement de l'écran                                             
                         0, 0, 0,
                         255);
						 
		SDL_RenderClear(renderer);

		SDL_SetRenderDrawColor(renderer,                                                
                         255, 255, 255,
                         255);


		for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000)
  		{
    		SDL_RenderDrawLine(renderer,                                   
                    0.1*w + 120 + (0.8*w - 200)*i/200,
					0.1*h + 120 + (0.8*h - 200)*i/200,
					0.1*w + 120 + (0.8*w - 200)*i/200 + 20 * cos(angle),
                    0.1*h + 120 + (0.8*h - 200)*i/200 + 20 * sin(angle));
  		}

		rec2.y = 0.1*h + 0.6*h*i/200;
		SDL_RenderFillRect(renderer, &rec2);
		SDL_RenderFillRect(renderer, &rec1);

		SDL_RenderPresent(renderer);
	}

	
	for(i=0;i<200;i++)																						// Deuxième mouvement
	{
		SDL_SetRenderDrawColor(renderer,                                                
                         0, 0, 0,
                         255);
						 
		SDL_RenderClear(renderer);

		SDL_SetRenderDrawColor(renderer,                                                
                         255, 255, 255,
                         255);


		for(float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000)
  		{
    		SDL_RenderDrawLine(renderer,                                   
                    0.9*w - 80 - (0.8*w - 200)*i/200,
					0.9*h - 80,
					0.9*w - 80 - (0.8*w - 200)*i/200 + 20 * cos(angle),
                    0.9*h - 80 + 20 * sin(angle));
  		}

		rec1.y = 0.1*h + 0.6*h*i/200;
		SDL_RenderFillRect(renderer, &rec2);
		SDL_RenderFillRect(renderer, &rec1);

		SDL_RenderPresent(renderer);
	}

	for(i=0;i<200;i++)																						// Troisième mouvement
	{
		SDL_SetRenderDrawColor(renderer,                                                
                         0, 0, 0,
                         255);
						 
		SDL_RenderClear(renderer);

		SDL_SetRenderDrawColor(renderer,                                                
                         255, 255, 255,
                         255);


		for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000)
  		{
    		SDL_RenderDrawLine(renderer,                                   
                    0.1*w + 120 + (w - 200)*i/200,
					0.9*h - 80 - (0.9*h - 200)*i/200,
					0.1*w + 120 + (w - 200)*i/200 + 20 * cos(angle),
                    0.9*h - 80 - (0.9*h - 200)*i/200 + 20 * sin(angle));
  		}

		SDL_RenderFillRect(renderer, &rec2);
		SDL_RenderFillRect(renderer, &rec1);

		SDL_RenderPresent(renderer);
	}
}*/

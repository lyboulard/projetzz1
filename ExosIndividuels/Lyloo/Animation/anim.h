#ifndef ANIM_H
#define ANIM_H

void mouv1(SDL_Renderer*, int, int, int, int, int, int);
void mouv2(SDL_Renderer*, int, int, int, int, int, int);
void mouv3(SDL_Renderer*, int, int, int, int, int, int);
int** initCouleurs();
void libereCouleurs(int**);
void end_sdl(char, char const*, SDL_Window*, SDL_Renderer*);
//void draw(SDL_Renderer*, int, int);

#endif
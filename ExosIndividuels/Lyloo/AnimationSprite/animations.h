#ifndef ANIMATIONS_H
#define ANIMATIONS_H

#define MIN(x, y) ((x<y)? x:y)

void animCourse(SDL_Renderer*, SDL_Rect*, SDL_Rect*, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, int, int);
void animEpee(SDL_Renderer*, SDL_Rect*, SDL_Rect*, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, int, int);
void animChute(SDL_Renderer*, SDL_Rect*, SDL_Rect*, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, int, int);

#endif
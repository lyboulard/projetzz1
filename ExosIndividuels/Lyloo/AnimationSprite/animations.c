#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include "animations.h"

/* ------------------------------------------------------------------------------
1ere partie de l'animation : course
Paramètres : 	renderer    - pointeur sur le renderer actif
				tabC        - tableau de SDL_Rect représentant les différents sprites de
                            l'animation de course
				tabK        - tableau de SDL_Rect représentant les différents sprites de
                            l'animation de l'ennemi
				decor       - SDL_Rect représentant le décor
                plateforme  - SDL_Rect représentant la plateforme sur laquelle
                            vont se déplacer les personnages
                nuage1      - SDL_Rect représentant le 1er nuage
                nuage2      - SDL_Rect représentant le 2e nuage
                textureC    - pointeur sur la texture contenant les sprites des
                            personnages
                textureK    - pointeur sur la texture contenant les sprites de
                            l'ennemi
                bg          - pointeur sur la texture contenant les sprites du décor, de
                            la plateforme et des nuages
                w           - largeur de la fenêtre
                h           - hauteur de la fenêtre
------------------------------------------------------------------------------ */

void animCourse(SDL_Renderer* renderer,
                SDL_Rect* tabC,
                SDL_Rect* tabK,
                SDL_Rect decor,
                SDL_Rect plateforme,
                SDL_Rect nuage1,
                SDL_Rect nuage2,
                SDL_Texture* textureC,
                SDL_Texture* textureK,
                SDL_Texture* bg,
                int w,
                int h)
{
    int i;
    SDL_Rect zoneWindowC;                                                                                   // choix du placement des éléments sur la fenêtre
    zoneWindowC.y = h-114;                                                                                  // = h-64-50, le personnage est sur la plateforme
    zoneWindowC.w = 54; zoneWindowC.h = 50;

    SDL_Rect zoneWindowK;
    zoneWindowK.x = 3*w/5; zoneWindowK.y = h-150;
    zoneWindowK.w = 95; zoneWindowK.h = 80;

    SDL_Rect zoneWindowBg;
    zoneWindowBg.x = 0; zoneWindowBg.y = -300;
    zoneWindowBg.w = 1120; zoneWindowBg.h = 800;

    SDL_Rect zoneWindowPlat;
    zoneWindowPlat.x = 0; zoneWindowPlat.y = h-64;
    zoneWindowPlat.w = 1120; zoneWindowPlat.h = 64;

    SDL_Rect zoneWindowN1;
    zoneWindowN1.y = 0;
    zoneWindowN1.w = 600; zoneWindowN1.h = 70;

    SDL_Rect zoneWindowN2;
    zoneWindowN2.y = 20;
    zoneWindowN2.w = 250; zoneWindowN2.h = 75;

    for(i=0;i<12;i++)
    {
        zoneWindowC.x = w/5 + 10*i;                                                                         // Le personnage et les nuages avancent horizontalement
        zoneWindowN1.x = 5*i;
        zoneWindowN2.x = 10*i;
        SDL_RenderClear(renderer);                                                                          // Effacement de l'écran
        SDL_RenderCopy(renderer, bg, &nuage1, &zoneWindowN1);
        SDL_RenderCopy(renderer, bg, &nuage2, &zoneWindowN2);
        SDL_RenderCopy(renderer, bg, &decor, &zoneWindowBg);
        SDL_RenderCopy(renderer, bg, &plateforme, &zoneWindowPlat);
        SDL_RenderCopy(renderer, textureC, &tabC[i], &zoneWindowC);
        SDL_RenderCopy(renderer, textureK, &tabK[i%6], &zoneWindowK);                                       // L'ennemi bouge en continu mais n'a que 6 sprites
        SDL_RenderPresent(renderer);
        SDL_Delay(100);
    }
}




/* ------------------------------------------------------------------------------
2e partie de l'animation : coup d'épée
Paramètres : 	renderer    - pointeur sur le renderer actif
				tabC        - tableau de SDL_Rect représentant les différents sprites de
                            l'animation de course
				tabK        - tableau de SDL_Rect représentant les différents sprites de
                            l'animation de l'ennemi
				decor       - SDL_Rect représentant le décor
                plateforme  - SDL_Rect représentant la plateforme sur laquelle
                            vont se déplacer les personnages
                nuage1      - SDL_Rect représentant le 1er nuage
                nuage2      - SDL_Rect représentant le 2e nuage
                textureC    - pointeur sur la texture contenant les sprites des
                            personnages
                textureK    - pointeur sur la texture contenant les sprites de
                            l'ennemi
                bg          - pointeur sur la texture contenant les sprites du décor, de
                            la plateforme et des nuages
                w           - largeur de la fenêtre
                h           - hauteur de la fenêtre
------------------------------------------------------------------------------ */

void animEpee(  SDL_Renderer* renderer,
                SDL_Rect* tabC,
                SDL_Rect* tabK,
                SDL_Rect decor,
                SDL_Rect plateforme,
                SDL_Rect nuage1,
                SDL_Rect nuage2,
                SDL_Texture* textureC,
                SDL_Texture* textureK,
                SDL_Texture* bg,
                int w,
                int h)
{
    int i;
    SDL_Rect zoneWindowC;
    zoneWindowC.x = w/5 + 100; zoneWindowC.y = h-119;                                                       // = h-64-55
    zoneWindowC.w = 110; zoneWindowC.h = 55;

    SDL_Rect zoneWindowK;
    zoneWindowK.x = 3*w/5; zoneWindowK.y = h-150;
    zoneWindowK.w = 100; zoneWindowK.h = 80;

    SDL_Rect zoneWindowBg;
    zoneWindowBg.x = 0; zoneWindowBg.y = -300;
    zoneWindowBg.w = 1120; zoneWindowBg.h = 800;

    SDL_Rect zoneWindowPlat;
    zoneWindowPlat.x = 0; zoneWindowPlat.y = h-64;
    zoneWindowPlat.w = 1120; zoneWindowPlat.h = 64;

    SDL_Rect zoneWindowN1;
    zoneWindowN1.y = 0;
    zoneWindowN1.w = 600; zoneWindowN1.h = 70;

    SDL_Rect zoneWindowN2;
    zoneWindowN2.y = 20;
    zoneWindowN2.w = 250; zoneWindowN2.h = 75;

    for(i=0;i<10;i++)
    {
        zoneWindowN1.x = 5*i + 60;
        zoneWindowN2.x = 10*i + 120;
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bg, &nuage1, &zoneWindowN1);
        SDL_RenderCopy(renderer, bg, &nuage2, &zoneWindowN2);
        SDL_RenderCopy(renderer, bg, &decor, &zoneWindowBg);
        SDL_RenderCopy(renderer, bg, &plateforme, &zoneWindowPlat);
        SDL_RenderCopy(renderer, textureC, &tabC[i], &zoneWindowC);
        SDL_RenderCopy(renderer, textureK, &tabK[i%6], &zoneWindowK);
        SDL_RenderPresent(renderer);
        SDL_Delay(100);
    }
}




/* ------------------------------------------------------------------------------
3e partie de l'animation : chute
Paramètres : 	renderer    - pointeur sur le renderer actif
				tabC        - tableau de SDL_Rect représentant les différents sprites de
                            l'animation de course
				tabK        - tableau de SDL_Rect représentant les différents sprites de
                            l'animation de l'ennemi
				decor       - SDL_Rect représentant le décor
                plateforme  - SDL_Rect représentant la plateforme sur laquelle
                            vont se déplacer les personnages
                nuage1      - SDL_Rect représentant le 1er nuage
                nuage2      - SDL_Rect représentant le 2e nuage
                textureC    - pointeur sur la texture contenant les sprites des
                            personnages
                textureK    - pointeur sur la texture contenant les sprites de
                            l'ennemi
                bg          - pointeur sur la texture contenant les sprites du décor, de
                            la plateforme et des nuages
                w           - largeur de la fenêtre
                h           - hauteur de la fenêtre
------------------------------------------------------------------------------ */

void animChute( SDL_Renderer* renderer,
                SDL_Rect* tabC,
                SDL_Rect* tabK,
                SDL_Rect decor,
                SDL_Rect plateforme,
                SDL_Rect nuage1,
                SDL_Rect nuage2,
                SDL_Texture* textureC,
                SDL_Texture* textureK,
                SDL_Texture* bg,
                int w,
                int h)
{
    int i;
    SDL_Rect zoneWindowC;
    zoneWindowC.x = w/5 + 95; zoneWindowC.y = h-113;                                                        // h-64-49
    zoneWindowC.w = 73; zoneWindowC.h = 49;

    SDL_Rect zoneWindowK;
    zoneWindowK.x = 3*w/5; zoneWindowK.y = h-150;
    zoneWindowK.w = 100; zoneWindowK.h = 80;

    SDL_Rect zoneWindowBg;
    zoneWindowBg.x = 0; zoneWindowBg.y = -300;
    zoneWindowBg.w = 1120; zoneWindowBg.h = 800;

    SDL_Rect zoneWindowPlat;
    zoneWindowPlat.x = 0; zoneWindowPlat.y = h-64;
    zoneWindowPlat.w = 1120; zoneWindowPlat.h = 64;

    SDL_Rect zoneWindowN1;
    zoneWindowN1.y = 0;
    zoneWindowN1.w = 600; zoneWindowN1.h = 70;

    SDL_Rect zoneWindowN2;
    zoneWindowN2.y = 20;
    zoneWindowN2.w = 250; zoneWindowN2.h = 75;

    for(i=0;i<30;i++)
    {
        zoneWindowN1.x = 5*i + 110;
        zoneWindowN2.x = 10*i + 220;
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bg, &nuage1, &zoneWindowN1);
        SDL_RenderCopy(renderer, bg, &nuage2, &zoneWindowN2);
        SDL_RenderCopy(renderer, bg, &decor, &zoneWindowBg);
        SDL_RenderCopy(renderer, bg, &plateforme, &zoneWindowPlat);
        SDL_RenderCopy(renderer, textureC, &tabC[MIN(i, 6)], &zoneWindowC);                                 // Le personnage reste au sol un moment, le dernier sprite dure plus longtemps
        SDL_RenderCopy(renderer, textureK, &tabK[i%6], &zoneWindowK);
        SDL_RenderPresent(renderer);
        SDL_Delay(100);
    }
}
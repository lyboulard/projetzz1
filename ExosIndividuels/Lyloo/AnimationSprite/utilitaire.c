#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include "utilitaire.h"

/* ------------------------------------------------------------------------------
Termine l'utilisation de la SDL
Paramètres : 	ok          - booléen indiquant si le programme doit se terminer
                            à cause d'une erreur ou de façon normale
				msg         - message à afficher si erreur
				window      - pointeur sur la fenêtre à supprimer
				renderer    - pointeur sur le SDL_Renderer à supprimer
------------------------------------------------------------------------------ */

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer)
{
  	char msg_formated[255];                                                         
  	int l;                                                                          

  	if (!ok)
	{                                                        												// Cas d'erreur
        strncpy(msg_formated, msg, 250);                                              
        l = strlen(msg_formated);                                                     
        strcpy(msg_formated + l, " : %s\n");                                          

        SDL_Log(msg_formated, SDL_GetError());                                        
  	}                                                                               

  	if (renderer != NULL)
	{                                           															// Destruction si nécessaire du renderer
    	SDL_DestroyRenderer(renderer);                     													// Attention : on suppose que les NULL sont maintenus !!
    	renderer = NULL;
  	}
  	if (window != NULL)  
	{                                           															// Destruction si nécessaire de la fenêtre
    	SDL_DestroyWindow(window);                          												// Attention : on suppose que les NULL sont maintenus !!
    	window= NULL;
  	}

    	SDL_Quit();                                                                     

  	if (!ok)
	{                                                        												// On quitte si cela ne va pas                
    	exit(EXIT_FAILURE);                                                           
  	}                                                                               
}




/* ------------------------------------------------------------------------------
Créé les SDL_Rect au bon endroit pour sélectionner les sprites de la course sur
la texture
------------------------------------------------------------------------------ */

SDL_Rect* creerTableauCourse()
{
    SDL_Rect* tab = (SDL_Rect*) malloc(12*sizeof(SDL_Rect));
    int i;
    for(i=0;i<12;i++)
    {
        tab[i].x = 54*i;
        tab[i].y = 860;
        tab[i].w = 54;
        tab[i].h = 50;
    }

    return tab;
}




/* ------------------------------------------------------------------------------
Créé les SDL_Rect au bon endroit pour sélectionner les sprites du coup d'épée sur
la texture
------------------------------------------------------------------------------ */

SDL_Rect* creerTableauEpee()
{
    SDL_Rect* tab = (SDL_Rect*) malloc(10*sizeof(SDL_Rect));
    int i;
    for(i=0;i<10;i++)
    {
        tab[i].x = 110*i;
        tab[i].y = 2970;
        tab[i].w = 110;
        tab[i].h = 55;
    }

    return tab;
}




/* ------------------------------------------------------------------------------
Créé les SDL_Rect au bon endroit pour sélectionner les sprites de la chute sur
la texture
------------------------------------------------------------------------------ */

SDL_Rect* creerTableauChute()
{
    SDL_Rect* tab = (SDL_Rect*) malloc(10*sizeof(SDL_Rect));
    int i;
    for(i=0;i<7;i++)
    {
        tab[i].x = 73*(i+1);
        tab[i].y = 2075;
        tab[i].w = 73;
        tab[i].h = 49;
    }

    return tab;
}




/* ------------------------------------------------------------------------------
Créé les SDL_Rect au bon endroit pour sélectionner les sprites de l'ennemi sur
la texture
------------------------------------------------------------------------------ */

SDL_Rect* creerTableauKing(SDL_Texture* texture)
{
    int w, h;
    SDL_QueryTexture(texture, NULL, NULL, &w, &h);
    SDL_Rect* tab = (SDL_Rect*) malloc(6*sizeof(SDL_Rect));
    int i;
    for(i=0;i<6;i++)
    {
        tab[i].x = w - 95*(i+1);
        tab[i].y = 180;
        tab[i].w = 95;
        tab[i].h = 80;
    }

    return tab;
}
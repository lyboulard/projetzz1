#ifndef UTILITAIRE_H
#define UTILITAIRE_H

void end_sdl(char, char const*, SDL_Window*, SDL_Renderer*);
SDL_Rect* creerTableauCourse();
SDL_Rect* creerTableauEpee();
SDL_Rect* creerTableauChute();
SDL_Rect* creerTableauKing(SDL_Texture*);

#endif
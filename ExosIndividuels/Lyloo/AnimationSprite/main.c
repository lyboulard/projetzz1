/* ------------------------------------------------------------------------------

Projet ZZ1 - ISIMA - Juin 2022
    - Animation de sprite : Adventure Time
    Les personnages Finn et Jake attaquent le Roi des Glaces et tombent
Lyloo Boulard

------------------------------------------------------------------------------ */

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include "utilitaire.h"
#include "animations.h"

int main(int argc, char** argv)
{
    (void)argc;
    (void)argv;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer); 						// Initialisation de la SDL

  	SDL_DisplayMode screen;
	SDL_GetCurrentDisplayMode(0, &screen);


  	/* ---------------Création de la fenêtre--------------- */

  	window = SDL_CreateWindow("Fenêtre",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 1120,
                            390,
                            SDL_WINDOW_OPENGL);
  	if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);


  	/* ---------------Création du renderer--------------- */

  	renderer = SDL_CreateRenderer(window, -1,
                                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  	if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    SDL_SetRenderDrawColor(renderer, 192, 223, 239, 255);


    /* ---------------Chargement des sprites-------------- */

    SDL_Texture* bg = IMG_LoadTexture(renderer, "./data/SpriteAnimationLylooBackground.png");
    if(bg == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }
    SDL_Texture* characters = IMG_LoadTexture(renderer, "./data/SpriteAnimationLylooFinnAndJake.png");
    if(characters == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }
    SDL_Texture* enemy = IMG_LoadTexture(renderer, "./data/SpriteAnimationLylooIceKing.png");
    if(enemy == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    SDL_Rect* tabCourse = creerTableauCourse();
    SDL_Rect* tabEpee = creerTableauEpee();
    SDL_Rect* tabChute = creerTableauChute();
    SDL_Rect* tabKing = creerTableauKing(enemy);
    SDL_Rect decor, plateforme, nuage1, nuage2;
    decor.x = 0; decor.y = 375;
    decor.w = 1120; decor.h = 390;
    plateforme.x = 100; plateforme.y = 960;
    plateforme.w = 1120; plateforme.h = 64;
    nuage1.x = 100; nuage1.y = 255;
    nuage1.w = 600; nuage1.h = 70;
    nuage2.x = 775; nuage2.y = 325;
    nuage2.w = 250; nuage2.h = 75;


    

    animCourse(renderer, tabCourse, tabKing, decor, plateforme, nuage1, nuage2, characters, enemy, bg, 1120, 390);
    animEpee(renderer, tabEpee, tabKing, decor, plateforme, nuage1, nuage2,  characters, enemy, bg, 1120, 390);
    animChute(renderer, tabChute, tabKing, decor, plateforme, nuage1, nuage2, characters, enemy, bg, 1120, 390);




    SDL_DestroyTexture(bg);
    SDL_DestroyTexture(characters);
    SDL_DestroyTexture(enemy);

    end_sdl(1, "Terminaison normale", window, renderer);

    return EXIT_SUCCESS;
}
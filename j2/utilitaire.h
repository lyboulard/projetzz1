#ifndef UTILITAIRE_H
#define UTILITAIRE_H

#include "init.h"


void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer);
void afficherTableau(SDL_Renderer* renderer, cellule** tableau, int taille);
int estStable(cellule** tableau, cellule** tableauSuiv, int taille, char* message);
int extinction(cellule** tableauSuiv, int taille, char* message);
void save(cellule** tableauSuiv, int taille, char* nom);

#endif
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "utilitaire.h"
#include "init.h"

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer)
{
  	char msg_formated[255];                                                         
  	int l;                                                                          

  	if (!ok)
	{                                                        												// Cas d'erreur
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  	}                                                                               

  	if (renderer != NULL)
	{                                           															// Destruction si nécessaire du renderer
    	SDL_DestroyRenderer(renderer);                     													// Attention : on suppose que les NULL sont maintenus !!
    	renderer = NULL;
  	}
  	if (window != NULL)  
	{                                           															// Destruction si nécessaire de la fenêtre
    	SDL_DestroyWindow(window);                          												// Attention : on suppose que les NULL sont maintenus !!
    	window= NULL;
  	}

    	SDL_Quit();                                                                     

  	if (!ok)
	{                                                        												// On quitte si cela ne va pas                
    	exit(EXIT_FAILURE);                                                           
  	}                                                                               
}


void afficherTableau(SDL_Renderer* renderer, cellule** tableau, int taille)
{
    SDL_RenderClear(renderer);
    
    int i,j;
    for(i=0;i<taille;i++)
    {
        for(j=0;j<taille;j++)
        {
            SDL_SetRenderDrawColor(renderer,                                                
                     255, 255, 255,
                     255);
            if(tableau[i][j].val == 1)
            {
                SDL_SetRenderDrawColor(renderer,                                                
                     0, 0, 0,
                     255);
            }
            SDL_RenderFillRect(renderer, &tableau[i][j].rect);
        }
    }

    SDL_RenderPresent(renderer);
}

int estStable(cellule** tableau, cellule** tableauSuiv, int taille, char* message)
{
    int i, j;
    for(i=0;i<taille;i++)
    {
        for(j=0;j<taille;j++)
        {
            if(tableau[i][j].val != tableauSuiv[i][j].val)
            {
                return 0;
            }
        }
    }

    strcpy(message, "Population stable");
    return 1;
}

int extinction(cellule** tableauSuiv, int taille, char* message)
{
    int i, j;
    for(i=0;i<taille;i++)
    {
        for(j=0;j<taille;j++)
        {
            if(tableauSuiv[i][j].val != 0)
            {
                return 0;
            }
        }
    }
    strcpy(message, "Population eteinte");
    return 1;
}


void save(cellule** tableauSuiv, int taille, char* nom)
{
    char c;
    FILE* f = fopen(nom, "w+");
    if(f != NULL)
    {
        int i,j;
        for(i=0;i<taille;i++)
        {
            for(j=0;j<taille;j++)
            {
                c = tableauSuiv[i][j].val + '0';
                fputc(c, f);
            }
        }
        fclose(f);
    }
}
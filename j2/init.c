#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "init.h"
#include "utilitaire.h"
#include "jeu.h"

cellule** initTableau(int taille, int wwindow, int hwindow)
{
    int i, j;
    cellule** tab = (cellule**) malloc(taille*sizeof(cellule*));
    for(i=0;i<taille;i++)
    {
        tab[i] = (cellule*) malloc(taille*sizeof(cellule));
        for(j=0;j<taille;j++)
        {
            tab[i][j].rect.x = j*(wwindow/taille);
            tab[i][j].rect.y = i*(hwindow/taille);
            tab[i][j].rect.w = wwindow/taille;
            tab[i][j].rect.h = hwindow/taille;
            tab[i][j].val = 0;
        }
    }

    return tab;
}


void remplissageManuel(SDL_Renderer* renderer, cellule** tableau, int taille, int wwindow, int hwindow)
{
    SDL_bool on = SDL_TRUE;
    SDL_Event event;

    while(on)
    {
        if(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
               case SDL_KEYDOWN :
                    switch(event.key.keysym.sym)
                    {
                        case 13 : //Barre espace
                        case 32 :
                            on = SDL_FALSE;
                            break;

                        default :
                            break;
                    }
                break;

                case SDL_MOUSEBUTTONDOWN:
                    if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
                    {
                        tableau[event.motion.y / (hwindow/taille)][event.motion.x / (wwindow/taille)].val = (tableau[event.motion.y / (hwindow/taille)][event.motion.x / (wwindow/taille)].val + 1)%2;
                    }
                    break;

                default :
                break;
            }
        }
        afficherTableau(renderer, tableau, taille);
    }
}

void creerTableau(SDL_Renderer* renderer, char* nomFichier, cellule** tableau, int taille, int wwindow, int hwindow)
{
    if(strlen(nomFichier) != 0)
    {
        FILE * f = fopen(nomFichier, "r");
        int i = 0;
        int j = 0;
        int compteur = 0;
        char c;
   
        while((c = fgetc(f)) != EOF)
        {
            if(c == '1')
            {
                tableau[i][j].val = 1;
            }
            compteur ++;
            i = compteur/taille;
            j = compteur%taille;
        }

        fclose(f);
        afficherTableau(renderer, tableau, taille);
    }
    else
    {
        remplissageManuel(renderer, tableau, taille, wwindow, hwindow);
    }
}



void menu(SDL_Renderer* renderer, SDL_Window* window, int wwindow, int hwindow, int taille, liste b, liste s, char* nomFichier)
{
    SDL_Rect postitre, postore, posgrille, postaille, posplus, posmoins;                                    // Position des éléments
    SDL_Rect recttore, rectgrille, recttaille, rectplus, rectmoins;                                         // Cadres autour des éléments

    TTF_Font* font = TTF_OpenFont("./Nuvel.ttf", 50);
    if (font == NULL) {end_sdl(0, "Can't load font", window, renderer);}

    SDL_Color color = {255, 255, 255, 255};                                                                 // On écrit en blanc

    SDL_Surface * textetitre, * textetore, * textegrille, * textetaille, * texteplus, * textemoins;         // Chaque élément a sa surface/texture
    SDL_Texture * text_texturetitre, * text_texturetore, * text_texturegrille, * text_texturetaille,        // On ne réutilise pas la même car ça rendrait la boucle évènementielle trop longue
                * text_textureplus, * text_texturemoins;

    /* ---------------1er texte--------------- */
    
    textetitre = TTF_RenderText_Blended(font, "Choisir les parametres du jeu", color);
    if (textetitre == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    text_texturetitre = SDL_CreateTextureFromSurface(renderer, textetitre);                                 // Transfert de la surface à la texture
    if (text_texturetitre == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    
    SDL_QueryTexture(text_texturetitre, NULL, NULL, &postitre.w, &postitre.h);
    postitre.x = 0.5*wwindow - 0.5*postitre.w;
    postitre.y = 0;

    /* ---------------2e texte--------------- */
    
    textetore = TTF_RenderText_Blended(font, "Tore", color);
    text_texturetore = SDL_CreateTextureFromSurface(renderer, textetore);
    SDL_QueryTexture(text_texturetore, NULL, NULL, &postore.w, &postore.h);
    postore.x = 0.5*(wwindow - postore.w);                                                                  // Choix de la position du choix "Tore"
    postore.y = 0.25*hwindow;

    recttore.x = postore.x - 15;                                                                            // Un cadre est positionné autour du choix "Tore"
    recttore.y = postore.y - 15;                                                                       
  	recttore.w = postore.w + 30; 
    recttore.h = postore.h + 30;

    /* ---------------3e texte--------------- */
            
    textegrille = TTF_RenderText_Blended(font, "Grille", color);
    text_texturegrille = SDL_CreateTextureFromSurface(renderer, textegrille);
    SDL_QueryTexture(text_texturegrille, NULL, NULL, &posgrille.w, &posgrille.h);
    posgrille.x = 0.5*(wwindow - posgrille.w);                                                              // Choix de la position du choix "Grille"
    posgrille.y = 0.75*hwindow;

    rectgrille.x = posgrille.x - 15;                                                                        // Un cadre est positionné autour du choix "Grille"
    rectgrille.y = posgrille.y - 15;                                                                       
  	rectgrille.w = posgrille.w + 30; 
    rectgrille.h = posgrille.h + 30;

    /* ---------------4e texte--------------- */

    char * phrase = (char*) malloc(2*sizeof(char));                                                         // Contient la taille sélectionnée
    sprintf(phrase, "Taille : %d", taille);

    textetaille = TTF_RenderText_Blended(font, phrase, color);
    text_texturetaille = SDL_CreateTextureFromSurface(renderer, textetaille);
    SDL_QueryTexture(text_texturetaille, NULL, NULL, &postaille.w, &postaille.h);
    postaille.x = 0.5*(wwindow - postaille.w);                                                              // Choix de la position du choix de la taille
    postaille.y = 0.5*hwindow;

    recttaille.x = postaille.x - 15;                                                                        // Un cadre est positionné autour du choix de la taille
    recttaille.y = postaille.y - 15;                                                                       
  	recttaille.w = postaille.w + 30; 
    recttaille.h = postaille.h + 30;

    /* ---------------5e texte--------------- */

    texteplus = TTF_RenderText_Blended(font, "+", color);
    text_textureplus = SDL_CreateTextureFromSurface(renderer, texteplus);
    SDL_QueryTexture(text_textureplus, NULL, NULL, &posplus.w, &posplus.h);
    rectplus.x = recttaille.x + recttaille.w;                                                               // Choix de la position du plus 
    rectplus.y = recttaille.y;                                                                       
  	
    posplus.x = rectplus.x + 15;                                                                            // Un cadre est positionné autour du choix de la taille
    posplus.y = rectplus.y + 20;
    rectplus.w = posplus.w + 30; 
    rectplus.h = posplus.h + 30;

    /* ---------------6e texte--------------- */

    textemoins = TTF_RenderText_Blended(font, "-", color);
    text_texturemoins = SDL_CreateTextureFromSurface(renderer, textemoins);
    SDL_QueryTexture(text_texturemoins, NULL, NULL, &posmoins.w, &posmoins.h);
    rectmoins.x = recttaille.x - rectplus.w;                                                                // Choix de la position du plus
    rectmoins.y = recttaille.y;                                                                       
  	
    posmoins.x = rectmoins.x + 15;                                                                          // Un cadre est positionné autour du choix de la taille
    posmoins.y = rectmoins.y + 15;
    rectmoins.w = rectplus.w; 
    rectmoins.h = rectplus.h;

    /* ---------------Boucle Event--------------- */

    cellule** tableau;
    cellule** tableauSuiv;


    

    SDL_Event event;
    SDL_bool on = SDL_TRUE;
    while(on)
    {
        if(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT:
                    on = SDL_FALSE;
                break;

                case SDL_MOUSEBUTTONDOWN:
                    if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
                    {
                        if(event.motion.x > recttore.x &&                                                   // Clic sur le bouton "Tore"
                            event.motion.x < recttore.x + recttore.w &&
                            event.motion.y > recttore.y &&
                            event.motion.y < recttore.y + recttore.h)
                        {
                            tableau = initTableau(taille, wwindow, hwindow);
                            tableauSuiv = initTableau(taille, wwindow, hwindow);
                            copie(tableau, tableauSuiv, taille);
                            creerTableau(renderer, nomFichier, tableau, taille, wwindow, hwindow);
                            deroule(renderer, window, tableau, tableauSuiv, b, s, taille, wwindow, 1);
                            on = SDL_FALSE;
                        }

                        if(event.motion.x > rectgrille.x &&                                                 // Clic sur le bouton "Grille"
                            event.motion.x < rectgrille.x + rectgrille.w &&
                            event.motion.y > rectgrille.y &&
                            event.motion.y < rectgrille.y + rectgrille.h)
                        {
                            tableau = initTableau(taille, wwindow, hwindow);
                            tableauSuiv = initTableau(taille, wwindow, hwindow);
                            copie(tableau, tableauSuiv, taille);
                            creerTableau(renderer, nomFichier, tableau, taille, wwindow, hwindow);
                            deroule(renderer, window, tableau, tableauSuiv, b, s, taille, wwindow, 0);
                            on = SDL_FALSE;
                        }

                        if(event.motion.x > rectplus.x &&                                                   // Clic sur le + -> augmentation de la taille
                            event.motion.x < rectplus.x + rectplus.w &&
                            event.motion.y > rectplus.y &&
                            event.motion.y < rectplus.y + rectplus.h)
                        {
                            taille++;
                        }

                        if(event.motion.x > rectmoins.x &&                                                  // Clic sur le - -> diminution de la taille
                            event.motion.x < rectmoins.x + rectmoins.w &&
                            event.motion.y > rectmoins.y &&
                            event.motion.y < rectmoins.y + rectmoins.h)
                        {
                            taille = MAX(taille-1, 5);                                                      // Taille minimale de 5
                        }
                    }
                    break;
            }
        }
        /* Phase d'affichage */

        SDL_RenderClear(renderer);
        
        SDL_SetRenderDrawColor(renderer, 0, 127, 255, 255);                                                 // Le cadre autour de "Tore" est bleu
        SDL_RenderDrawRect(renderer, &recttore);
        SDL_RenderCopy(renderer, text_texturetore, NULL, &postore);
        
        
        SDL_SetRenderDrawColor(renderer, 255, 162, 0, 255);                                                 // Le cadre autour de "Grille" est orange
        SDL_RenderDrawRect(renderer, &rectgrille);
        SDL_RenderCopy(renderer, text_texturegrille, NULL, &posgrille);
        
        sprintf(phrase, "Taille : %d", taille);
        textetaille = TTF_RenderText_Blended(font, phrase, color);                                          // On met à jour la taille affichée
        text_texturetaille = SDL_CreateTextureFromSurface(renderer, textetaille);
        
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);                                                   // Le cadre autour de "Taille" est rouge
        SDL_QueryTexture(text_texturetaille, NULL, NULL, &postaille.w, &postaille.h);
        SDL_RenderDrawRect(renderer, &recttaille);
        SDL_RenderCopy(renderer, text_texturetaille, NULL, &postaille);

        
        SDL_QueryTexture(text_textureplus, NULL, NULL, &posplus.w, &posplus.h);
        SDL_RenderDrawRect(renderer, &rectplus);
        SDL_RenderCopy(renderer, text_textureplus, NULL, &posplus);

        SDL_QueryTexture(text_texturemoins, NULL, NULL, &posmoins.w, &posmoins.h);
        SDL_RenderDrawRect(renderer, &rectmoins);
        SDL_RenderCopy(renderer, text_texturemoins, NULL, &posmoins);

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);                                                     // Le fond est noir
        SDL_RenderCopy(renderer, text_texturetitre, NULL, &postitre);

        SDL_RenderPresent(renderer);
    }

//Phase de libération 
    free(phrase);

    SDL_FreeSurface(textetitre);
    SDL_FreeSurface(textetore);
    SDL_FreeSurface(textegrille);
    SDL_FreeSurface(textetaille);
    SDL_FreeSurface(texteplus);
    SDL_FreeSurface(textemoins);

    SDL_DestroyTexture(text_texturetitre);
    SDL_DestroyTexture(text_texturetore);
    SDL_DestroyTexture(text_texturegrille);
    SDL_DestroyTexture(text_texturetaille);
    SDL_DestroyTexture(text_textureplus);
    SDL_DestroyTexture(text_texturemoins);
}
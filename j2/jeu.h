#ifndef JEU_H
#define JEU_H

#include "init.h"

int voisinsGrille(cellule** tableau, int taille, int i, int j);
int voisinsTore(cellule** tableau, int taille, int i, int j);
void copie(cellule** tableau, cellule** tableauSuiv, int taille);
void majTableau(cellule** tableau, cellule** tableauSuiv, int taille, liste b, liste s, int type);
void deroule(SDL_Renderer* renderer, SDL_Window* window, cellule** tableau, cellule** tableauSuiv, liste b, liste s, int taille, int wwindow, int type);

#endif
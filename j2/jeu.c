#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "jeu.h"
#include "init.h"
#include "utilitaire.h"



int voisinsGrille(cellule** tableau, int taille, int i, int j)
{
    int compteur = 0;
    int ii, jj;
    for(ii=-1;ii<2;ii++)
    {
        for(jj=-1;jj<2;jj++)
        {
            if((i+ii)<taille && (i+ii)>-1 && (j+jj)<taille && (j+jj)>-1 && (ii != 0 || jj != 0))
            {
                compteur += tableau[i+ii][j+jj].val;
            }
        }
    }

    return compteur;
}

int voisinsTore(cellule** tableau, int taille, int i, int j)
{
    int compteur = 0;
    int ii, jj;

    for(ii=-1;ii<2;ii++)
    {
        for(jj=-1;jj<2;jj++)
        {
            if(ii != 0 || jj != 0)
            {
                compteur += tableau[(i+ii+taille)%taille][(j+jj+taille)%taille].val;
            }
        }
    }

    return compteur;
}


void copie(cellule** tableau, cellule** tableauSuiv, int taille)
{
    int i, j;
    for(i=0;i<taille;i++)
    {
        for(j=0;j<taille;j++)
        {
            tableau[i][j].val = tableauSuiv[i][j].val;
        }
    }
}

void majTableau(cellule** tableau, cellule** tableauSuiv, int taille, liste b, liste s, int type)
{
    int i, j, nbv;
    for(i=0;i<taille;i++)
    {
        for(j=0;j<taille;j++)
        {
            if(type)
            {
                nbv = voisinsTore(tableau, taille, i, j);
            }
            else
            {
                nbv = voisinsGrille(tableau, taille, i, j);
            }
            if(tableau[i][j].val == 0)
            {
                if(nbv <= b.fin && nbv >= b.dbt)
                {
                    tableauSuiv[i][j].val = 1;
                }
                else
                {
                    tableauSuiv[i][j].val = 0;
                }
            }
            if(tableau[i][j].val == 1)
            {
                if(nbv < s.dbt || nbv > s.fin)
                {
                    tableauSuiv[i][j].val = 0;
                }
                else
                {
                    tableauSuiv[i][j].val = 1;
                }
            }
        }
    }
}



void deroule(SDL_Renderer* renderer, SDL_Window* window, cellule** tableau, cellule** tableauSuiv, liste b, liste s, int taille, int wwindow, int type)
{
    TTF_Font* font;
    SDL_Surface* surface;
    SDL_Texture* texture;
    SDL_Rect posMessage;
    SDL_Color color = {255, 255, 0, 255}; 
    
    int speed = 5;
    
    SDL_Event event;
    SDL_bool on = SDL_TRUE;
    
    while (on)
    {
        while (SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT :
                    on = SDL_FALSE;
                break;

                case SDL_KEYDOWN :
                    switch(event.key.keysym.sym)
                    {
                        case 32 : //Barre espace
                        case 13:
                            on = SDL_FALSE;
                        break;

                        case SDLK_DOWN:
                            speed = MIN(speed+1, 10);
                        break;

                        case SDLK_UP:
                            speed = MAX(speed-1, 0);
                        break;

                        case SDLK_s:
                            save(tableauSuiv, taille, "save.txt");

                        default :
                        continue;
                    }
                break;

                default :
                break;
            }
        }
        majTableau(tableau, tableauSuiv, taille, b, s, type);

        afficherTableau(renderer, tableauSuiv, taille);
        SDL_Delay(50+95*speed);

        char* message = (char*) malloc(30*sizeof(char));
            
        if (extinction(tableauSuiv, taille, message) || estStable(tableau, tableauSuiv, taille, message))
        {
            font = TTF_OpenFont("./Nuvel.ttf", 50);
            if (font == NULL) {end_sdl(0, "Can't load font", window, renderer);}

            surface = TTF_RenderText_Blended(font, message, color);
            if (surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

            texture = SDL_CreateTextureFromSurface(renderer, surface);                                 // Transfert de la surface à la texture
            if (texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    
            SDL_QueryTexture(texture, NULL, NULL, &posMessage.w, &posMessage.h);
            posMessage.x = 0.5*wwindow - 0.5*posMessage.w; posMessage.y = 0;

            SDL_RenderCopy(renderer,texture, NULL, &posMessage);
            SDL_RenderPresent(renderer);
            SDL_Delay(5000);
            on = SDL_FALSE;
        }

        copie(tableau, tableauSuiv, taille);
    }
}
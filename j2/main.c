#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "init.h"
#include "utilitaire.h"
#include "jeu.h"


int main(int argc, char** argv)
{                                                                  // Utilisation de grille préexistante ou non
    char* nomFichier = (char*) malloc(30*sizeof(char));
    if(argc > 1)
    {
        strcpy(nomFichier, argv[1]);
    }

    liste birth;
    liste survie;

    birth.dbt = 3;                                                                                          // Règles de naissance
    birth.fin = 3;
    survie.dbt = 2;                                                                                         // Règles de survie 
    survie.fin = 3;

    int wwindow, hwindow;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    

    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);                      // Initialisation de la SDL
    if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);


  	SDL_DisplayMode screen;
	SDL_GetCurrentDisplayMode(0, &screen);


  	/* ---------------Création de la fenêtre--------------- */

  	window = SDL_CreateWindow("Fenêtre",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 990,
                            990,
                            SDL_WINDOW_OPENGL);
  	if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    SDL_GetWindowSize(window, &wwindow, &hwindow);

  	/* ---------------Création du renderer--------------- */

  	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  	if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);




    /* ---------------Gestion du menu--------------- */


    int taille = 20;                                                                                         // Taille de la grille (par défaut, 30)
    menu(renderer, window, wwindow, hwindow, taille, birth, survie, nomFichier);

    TTF_Quit();
    end_sdl(1, "fermeture normale", window, renderer);

    return EXIT_SUCCESS;
}
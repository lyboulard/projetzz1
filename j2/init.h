#ifndef INIT_H
#define INIT_H

#define MIN(x, y) ((x<y)? x:y)
#define MAX(x, y) ((x>y)? x:y)

typedef struct cellule
{
    SDL_Rect rect;
    short int val;
}cellule;

typedef struct liste
{
    int dbt;
    int fin;
}liste;

cellule** initTableau(int taille, int wwindow, int hwindow);
void remplissageManuel(SDL_Renderer* renderer, cellule** tableau, int taille, int wwindow, int hwindow);
void creerTableau(SDL_Renderer* renderer, char* nomFichier, cellule** tableau, int taille, int wwindow, int hwindow);
void menu(SDL_Renderer* renderer, SDL_Window* window, int wwindow, int hwindow, int taille, liste b, liste s, char* nomFichier);

#endif
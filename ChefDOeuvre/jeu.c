#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <time.h>
#include "jeu.h"
#include "initialisation.h"

/* ------------------------------------------------------------------------------
Déplace le Mii d'une case vers le bas
Retourne la position actuelle du Mii
Paramètres : 	renderer    - pointeur sur le renderer actif
				grille      - pointeur sur un tableau de SDL_Rect qui représentent
                            tous les carreaux existant sur la grille
                posMii      - SDL_Rect représentant la position actuelle du Mii
                            sur la grille
				oeilD       - SDL_Rect représentant l'oeil droit de l'ennemi
				oeilG       - SDL_Rect représentant l'oeil gauche de l'ennemi
                visage1     - SDL_Rect représentant le 1er visage volé
                visage2     - SDL_Rect représentant le 2e visage volé
                visage3     - SDL_Rect représentant le 3e visage volé
                mii         - SDL_Rect représentant le Mii
                TextMii     - pointeur sur la texture contenant les sprites du mii
                TextOeilD   - pointeur sur la texture contenant les sprites de
                            l'oeil droit de l'ennemi
                TextOeilG   - pointeur sur la texture contenant les sprites de
                            l'oeil gauche de l'ennemi
                TextVisages - pointeur sur la texture contenant les sprites des
                            visages
------------------------------------------------------------------------------ */

void afficheMap(SDL_Renderer * renderer, carreau ** grille, SDL_Rect posMii, SDL_Rect oeilD, SDL_Rect oeilG, SDL_Rect visage1, 
                SDL_Rect visage2, SDL_Rect visage3, SDL_Rect tabMii[], SDL_Rect bloc, SDL_Rect bombe, SDL_Texture* TextMii, SDL_Texture* TextOeilD,
                SDL_Texture* TextOeilG, SDL_Texture* TextVisages, SDL_Texture * TextBloc, SDL_Texture * TextBombe, int a)
{
    SDL_Rect rect;
    int i,j;
    rect.w = rect.h = 66;
    SDL_RenderClear(renderer);
    for(i = 0; i < 15; i++)
    {
        for (j = 0; j < 15; j++)
        {
            rect.x = grille[i][j].rect.x;
            rect.y = grille[i][j].rect.y;
            switch(grille[i][j].type)
            {
                case LIBRE :
                    SDL_SetRenderDrawColor(renderer, 0, 200, 0, 255);
                break;

                case BLOC :
                    SDL_RenderCopy(renderer, TextBloc, &bloc, &grille[i][j].rect);
                break;

                case VISAGE :
                    SDL_SetRenderDrawColor(renderer, 0, 200, 0, 255);
                break;

                case MECHANT :
                    SDL_SetRenderDrawColor(renderer, 0, 200, 0, 255);
                break;

                case PREBOMBE :
                    SDL_SetRenderDrawColor(renderer, 0, 100, 0, 255);
                break;

                case BOMBE :
                    SDL_RenderCopy(renderer, TextBombe, &bombe, &grille[i][j].rect);
                break;

                default:
                break;
            }
            if (grille[i][j].type != BLOC && grille[i][j].type != BOMBE)
            {SDL_RenderFillRect(renderer, &rect);}
        }
        
        
        SDL_RenderCopy(renderer, TextOeilG, &oeilG, &grille[0][7].rect);
        SDL_RenderCopy(renderer, TextOeilD, &oeilD, &grille[0][8].rect);
        SDL_RenderCopy(renderer, TextVisages, &visage1, &grille[5][2].rect);
        SDL_RenderCopy(renderer, TextVisages, &visage2, &grille[5][12].rect);
        SDL_RenderCopy(renderer, TextVisages, &visage3, &grille[12][7].rect); 
        SDL_RenderCopy(renderer, TextMii, &tabMii[a], &posMii);
    }
    SDL_RenderPresent(renderer);
}

SDL_Rect animBas(   SDL_Renderer* renderer,
                    carreau** grille,
                    SDL_Rect posMii,
                    SDL_Rect oeilD,
                    SDL_Rect oeilG,
                    SDL_Rect visage1,
                    SDL_Rect visage2,
                    SDL_Rect visage3,
                    SDL_Rect tabMii[],
                    SDL_Rect bloc,
                    SDL_Rect bombe,
                    SDL_Texture* TextMii,
                    SDL_Texture* TextOeilD,
                    SDL_Texture* TextOeilG,
                    SDL_Texture* TextVisages,
                    SDL_Texture* TextBloc,
                    SDL_Texture* TextBombe)
{
    int newpos,i,j;
    if(posMii.y != 924)                                                                                     // Vérifie qu'on n'est pas déjà tout en bas
    {
        if (grille[(posMii.y + 66)/66][(posMii.x )/66].type == LIBRE || 
            grille[(posMii.y + 66)/66][(posMii.x )/66].type == BOMBE || 
            grille[(posMii.y + 66)/66][(posMii.x )/66].type == PREBOMBE)
        {
            newpos = posMii.y + 66;
            i=j=0;
            while(posMii.y < newpos)
            {
                if (posMii.y > newpos)
                {
                    posMii.y = newpos;
                }
                if (i == 2) {i = 0;}

                posMii.y += 1;

                afficheMap(renderer, grille, posMii, oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe, i);

                j++;
                if(j%(66/2) == 0){i++;}
            }       
            
        }
    }

    return posMii;
}



/* ------------------------------------------------------------------------------
Déplace le Mii d'une case vers le haut
Retourne la position actuelle du Mii
Paramètres : 	renderer    - pointeur sur le renderer actif
				grille      - pointeur sur un tableau de SDL_Rect qui représentent
                            tous les carreaux existant sur la grille
                posMii      - SDL_Rect représentant la position actuelle du Mii
                            sur la grille
				oeilD       - SDL_Rect représentant l'oeil droit de l'ennemi
				oeilG       - SDL_Rect représentant l'oeil gauche de l'ennemi
                visage1     - SDL_Rect représentant le 1er visage volé
                visage2     - SDL_Rect représentant le 2e visage volé
                visage3     - SDL_Rect représentant le 3e visage volé
                mii         - SDL_Rect représentant le Mii
                bloc        - SDL_Rect représentant les blocs
                bombe        - SDL_Rect représentant les bombes
                TextMii     - pointeur sur la texture contenant les sprites du mii
                TextOeilD   - pointeur sur la texture contenant les sprites de
                            l'oeil droit de l'ennemi
                TextOeilG   - pointeur sur la texture contenant les sprites de
                            l'oeil gauche de l'ennemi
                TextVisages - pointeur sur la texture contenant les sprites des
                              visages
                TextBloc    - pointeur sur la texture contenant les sprites des
                              blocs
                TextBombe    - pointeur sur la texture contenant les sprites des
                               bombes
------------------------------------------------------------------------------ */

SDL_Rect animHaut(  SDL_Renderer* renderer,
                    carreau** grille,
                    SDL_Rect posMii,
                    SDL_Rect oeilD,
                    SDL_Rect oeilG,
                    SDL_Rect visage1,
                    SDL_Rect visage2,
                    SDL_Rect visage3,
                    SDL_Rect tabMii[],
                    SDL_Rect bloc,
                    SDL_Rect bombe,
                    SDL_Texture* TextMii,
                    SDL_Texture* TextOeilD,
                    SDL_Texture* TextOeilG,
                    SDL_Texture* TextVisages,
                    SDL_Texture* TextBloc,
                    SDL_Texture* TextBombe)
{
    int i,j,newpos;
    if(posMii.y != 0)                                                                                       // On vérifie qu'on n'est pas déjà tout en haut
    {
        if (grille[(posMii.y - 66)/66][(posMii.x)/66].type == LIBRE || 
            grille[(posMii.y - 66)/66][(posMii.x)/66].type == BOMBE ||
            grille[(posMii.y - 66)/66][(posMii.x)/66].type == PREBOMBE)
        {
            newpos = posMii.y - 66;
            i=j=0;
            while(posMii.y > newpos)
            {
                if (posMii.y < newpos)
                {
                    posMii.y = newpos;
                }
                if (i == 2) {i = 0;}

                posMii.y -= 1;

                afficheMap(renderer, grille, posMii, oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe, i);
                j++;
                if(j%(66/2) == 0){i++;}
            }  
        }
    }

    return posMii;
}




/* ------------------------------------------------------------------------------
Déplace le Mii d'une case vers la gauche
Retourne la position actuelle du Mii
Paramètres : 	renderer    - pointeur sur le renderer actif
				grille      - pointeur sur un tableau de SDL_Rect qui représentent
                            tous les carreaux existant sur la grille
                posMii      - SDL_Rect représentant la position actuelle du Mii
                            sur la grille
				oeilD       - SDL_Rect représentant l'oeil droit de l'ennemi
				oeilG       - SDL_Rect représentant l'oeil gauche de l'ennemi
                visage1     - SDL_Rect représentant le 1er visage volé
                visage2     - SDL_Rect représentant le 2e visage volé
                visage3     - SDL_Rect représentant le 3e visage volé
                mii         - SDL_Rect représentant le Mii
                TextMii     - pointeur sur la texture contenant les sprites du mii
                TextOeilD   - pointeur sur la texture contenant les sprites de
                            l'oeil droit de l'ennemi
                TextOeilG   - pointeur sur la texture contenant les sprites de
                            l'oeil gauche de l'ennemi
                TextVisages - pointeur sur la texture contenant les sprites des
                            visages
------------------------------------------------------------------------------ */

SDL_Rect animGauche(SDL_Renderer* renderer,
                    carreau** grille,
                    SDL_Rect posMii,
                    SDL_Rect oeilD,
                    SDL_Rect oeilG,
                    SDL_Rect visage1,
                    SDL_Rect visage2,
                    SDL_Rect visage3,
                    SDL_Rect tabMii[],
                    SDL_Rect bloc,
                    SDL_Rect bombe,
                    SDL_Texture* TextMii,
                    SDL_Texture* TextOeilD,
                    SDL_Texture* TextOeilG,
                    SDL_Texture* TextVisages,
                    SDL_Texture* TextBloc,
                    SDL_Texture* TextBombe)
{
    int i,j,newpos;
    if(posMii.x != 0)                                                                                       // On vérifie qu'on n'est pas déjà tout à gauche
    {
        if (grille[posMii.y/66][(posMii.x - 66)/66].type == LIBRE || 
            grille[posMii.y/66][(posMii.x - 66)/66].type == BOMBE ||
            grille[posMii.y/66][(posMii.x - 66)/66].type == PREBOMBE)
        {
            newpos = posMii.x - 66;
            i=6;
            j=0;
            while(posMii.x > newpos)
            {
                if (posMii.x < newpos)
                {
                    posMii.x = newpos;
                }
                if (i == 10) {i = 6;}

                posMii.x -= 1;

                afficheMap(renderer, grille, posMii, oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe, i);
                j++;
                if(j%(66/4) == 0){i++;}
            }   
        }
    }

    return posMii;
}




/* ------------------------------------------------------------------------------
Déplace le Mii d'une case vers la droite
Retourne la position actuelle du Mii
Paramètres : 	renderer    - pointeur sur le renderer actif
				grille      - pointeur sur un tableau de SDL_Rect qui représentent
                            tous les carreaux existant sur la grille
                posMii      - SDL_Rect représentant la position actuelle du Mii
                            sur la grille
				oeilD       - SDL_Rect représentant l'oeil droit de l'ennemi
				oeilG       - SDL_Rect représentant l'oeil gauche de l'ennemi
                visage1     - SDL_Rect représentant le 1er visage volé
                visage2     - SDL_Rect représentant le 2e visage volé
                visage3     - SDL_Rect représentant le 3e visage volé
                mii         - SDL_Rect représentant le Mii
                TextMii     - pointeur sur la texture contenant les sprites du mii
                TextOeilD   - pointeur sur la texture contenant les sprites de
                            l'oeil droit de l'ennemi
                TextOeilG   - pointeur sur la texture contenant les sprites de
                            l'oeil gauche de l'ennemi
                TextVisages - pointeur sur la texture contenant les sprites des
                            visages
------------------------------------------------------------------------------ */

SDL_Rect animDroite(SDL_Renderer* renderer,
                    carreau** grille,
                    SDL_Rect posMii,
                    SDL_Rect oeilD,
                    SDL_Rect oeilG,
                    SDL_Rect visage1,
                    SDL_Rect visage2,
                    SDL_Rect visage3,
                    SDL_Rect tabMii[],
                    SDL_Rect bloc,
                    SDL_Rect bombe,
                    SDL_Texture* TextMii,
                    SDL_Texture* TextOeilD,
                    SDL_Texture* TextOeilG,
                    SDL_Texture* TextVisages,
                    SDL_Texture* TextBloc,
                    SDL_Texture* TextBombe)
{
    int i,j,newpos;
    if(posMii.x != 924)                                                                                     // On vérifie qu'on n'est pas déjà tout à droite
    {
        if (grille[posMii.y/66][(posMii.x + 66)/66].type == LIBRE || 
            grille[posMii.y/66][(posMii.x + 66)/66].type == BOMBE || 
            grille[posMii.y/66][(posMii.x + 66)/66].type == PREBOMBE)
        {
            newpos = posMii.x + 66;
            i=6;
            j=0;
            while(posMii.x < newpos)
            {
                if (posMii.x > newpos)
                {
                    posMii.x = newpos;
                }
                if (i == 6) {i = 2;}

                posMii.x += 1;

                afficheMap(renderer, grille, posMii, oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe, i);
                j++;
                if(j%(66/4) == 0){i++;}
            }   
        }
    }

    return posMii;
}





void poseBloc(carreau ** grille)
{
    int roll,i,j;
    roll = rand()% 3; //Pour savoir autour de quel visage on pose le bloc
    i = rand()% 3; //0 alors on pose à droite, 1 on pose à gauche
    j = rand()% 3; //0 alors on pose en haut, 1 on pose en bas

    if (i == 2) {i = -1;}
    if (j == 2) {j = -1;}

    switch(roll)
    {
        case 0:

            while(grille[5+i][2+j].type == BLOC || grille[5+i][2+j].type == VISAGE || (i == 0 && j == 0))
            {
                i = rand()% 3;
                j = rand()% 3;

                if (i == 2) {i = -1;}
                if (j == 2) {j = -1;}
            }
            grille[5+i][2+j].type = BLOC;

        break;

        case 1:

            while(grille[5+i][12+j].type == BLOC || grille[5+i][2+j].type == VISAGE || (i == 0 && j == 0))
            {
                i = rand()% 3;
                j = rand()% 3;

                if (i == 2) {i = -1;}
                if (j == 2) {j = -1;}
            }
            grille[5+i][12+j].type = BLOC;
               
        break;

        case 2:

            while(grille[12+i][7+j].type == BLOC || grille[5+i][2+j].type == VISAGE ||(i == 0 && j == 0))
            {
                i = rand()% 3;
                j = rand()% 3;

                if (i == 2) {i = -1;}
                if (j == 2) {j = -1;}
            }
            grille[12+i][7+j].type = BLOC;
           
        break;

        default :
        break;
    }   
}



void pluieBombes(carreau ** grille)
{
    int i,j, aleat;
    for(i=0;i<15;i++)
    {
        for(j=0;j<15;j++)
        {
            if(grille[i][j].type == LIBRE)
            {
                aleat = rand()%10;
                {
                    if(!aleat)
                    {
                        grille[i][j].type = PREBOMBE; // En moyenne une case sur 10 devient une bombe
                    }
                }
            }
        }
    }
}



void explosion(SDL_Window * window, SDL_Renderer * renderer, carreau ** grille, SDL_Rect posMii, SDL_Rect oeilD, SDL_Rect oeilG, SDL_Rect visage1, 
                SDL_Rect visage2, SDL_Rect visage3, SDL_Rect tabMii[], SDL_Rect bloc, SDL_Rect bombe, SDL_Texture* TextMii, SDL_Texture* TextOeilD,
                SDL_Texture* TextOeilG, SDL_Texture* TextVisages, SDL_Texture * TextBloc, SDL_Texture * TextBombe,SDL_bool * on)
{
    int i, j;
    for(i=0; i<15; i++)
    {
        for(j=0; j<15; j++)
        {
            if(grille[i][j].type == PREBOMBE)
            {
                grille[i][j].type = BOMBE;
            }
        }
    }
    if(grille[posMii.y/66][(posMii.x)/66].type == BOMBE) //On vérifie si on est sur une bombe
    {
        *on = SDL_FALSE;
        afficheMap(renderer, grille, posMii, oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe, 0);
        defaite(window, renderer, "Mechant vous a tue avec une bombe");
    }
}

void enleveBombe (carreau ** grille)
{
    int i, j;
    for(i=0; i<15; i++)
    {
        for(j=0; j<15; j++)
        {
            if(grille[i][j].type == BOMBE)
            {
                grille[i][j].type = LIBRE;
            }
        }
    }
}

int combat(SDL_Window* window, SDL_Renderer * renderer, carreau ** grille, SDL_Rect posMii, SDL_Rect oeilD, SDL_Rect oeilG, SDL_Rect visage1, 
            SDL_Rect visage2, SDL_Rect visage3, SDL_Rect tabMii[], SDL_Rect bloc, SDL_Rect bombe, SDL_Texture* TextMii, SDL_Texture* TextOeilD,
            SDL_Texture* TextOeilG, SDL_Texture* TextVisages, SDL_Texture * TextBloc, SDL_Texture * TextBombe, int* temps)
{                                                                                      

    SDL_Rect barre;                                     // Barre de vie de l'ennemi
    barre.x = 462; barre.y = 10;
    barre.h = 20;
    barre.w = *temps;

    SDL_RenderClear(renderer);
    afficheMap(renderer, grille, posMii, oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe, 0);
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &barre);
    SDL_RenderPresent(renderer);

    SDL_Delay(100);
    (*temps)--;

    if(*temps == 0)
    {
        victoire(window, renderer);
        end_sdl(1, "terminaison normale", window, renderer);
        return 1;
    }

    return 0;
}

SDL_bool visageEntoure (carreau ** grille)
{
    int compteur = 0;
    SDL_bool res = SDL_FALSE;

    //Vérification Visage 1 entouré ou non
    if (grille[4][1].type == BLOC) {compteur++;}
    if (grille[4][2].type == BLOC) {compteur++;}
    if (grille[4][3].type == BLOC) {compteur++;}
    if (grille[5][1].type == BLOC) {compteur++;}
    if (grille[5][3].type == BLOC) {compteur++;}
    if (grille[6][1].type == BLOC) {compteur++;}
    if (grille[6][2].type == BLOC) {compteur++;}
    if (grille[6][3].type == BLOC) {compteur++;}

    if(compteur == 8) {res = SDL_TRUE;}
    
    compteur = 0;

    //Vérification Visage 2 entouré ou non
    if (grille[4][11].type == BLOC) {compteur++;}
    if (grille[4][12].type == BLOC) {compteur++;}
    if (grille[4][13].type == BLOC) {compteur++;}
    if (grille[5][11].type == BLOC) {compteur++;}
    if (grille[5][13].type == BLOC) {compteur++;}
    if (grille[6][11].type == BLOC) {compteur++;}
    if (grille[6][12].type == BLOC) {compteur++;}
    if (grille[6][13].type == BLOC) {compteur++;}

    if(compteur == 8) {res = SDL_TRUE;}
    
    compteur = 0;

    //Vérification Visage 3 entouré ou non
    if (grille[11][6].type == BLOC) {compteur++;}
    if (grille[11][7].type == BLOC) {compteur++;}
    if (grille[11][8].type == BLOC) {compteur++;}
    if (grille[12][6].type == BLOC) {compteur++;}
    if (grille[12][8].type == BLOC) {compteur++;}
    if (grille[13][6].type == BLOC) {compteur++;}
    if (grille[13][7].type == BLOC) {compteur++;}
    if (grille[13][8].type == BLOC) {compteur++;}

    if(compteur == 8) {res = SDL_TRUE;}

    compteur = 0;

    return res;
}

/* ------------------------------------------------------------------------------
Corps du jeu
Paramètres : 	renderer    - pointeur sur le renderer actif
				grille      - pointeur sur un tableau de SDL_Rect qui représentent
                            tous les carreaux existant sur la grille
                posMii      - SDL_Rect représentant la position actuelle du Mii
                            sur la grille
				oeilD       - SDL_Rect représentant l'oeil droit de l'ennemi
				oeilG       - SDL_Rect représentant l'oeil gauche de l'ennemi
                visage1     - SDL_Rect représentant le 1er visage volé
                visage2     - SDL_Rect représentant le 2e visage volé
                visage3     - SDL_Rect représentant le 3e visage volé
                mii         - SDL_Rect représentant le Mii
                bloc        - SDL_Rect représentant les blocs
                bombe        - SDL_Rect représentant les bombes
                TextMii     - pointeur sur la texture contenant les sprites du mii
                TextOeilD   - pointeur sur la texture contenant les sprites de
                            l'oeil droit de l'ennemi
                TextOeilG   - pointeur sur la texture contenant les sprites de
                            l'oeil gauche de l'ennemi
                TextVisages - pointeur sur la texture contenant les sprites des
                            visages
                TextBloc    - pointeur sur la texture contenant les sprites des
                              blocs
                TextBombe    - pointeur sur la texture contenant les sprites des
                               bombes
------------------------------------------------------------------------------ */

void jeu(   SDL_Window* window,
            SDL_Renderer* renderer,
            carreau** grille,
            SDL_Rect posMii,
            SDL_Rect oeilD,
            SDL_Rect oeilG,
            SDL_Rect visage1,
            SDL_Rect visage2,
            SDL_Rect visage3,
            SDL_Rect tabMii[],
            SDL_Rect bloc,
            SDL_Rect bombe,
            SDL_Texture* TextMii,
            SDL_Texture* TextOeilD,
            SDL_Texture* TextOeilG,
            SDL_Texture* TextVisages,
            SDL_Texture* TextBloc,
            SDL_Texture* TextBombe, 
            int action)
{
    int temps = 30;                     // Temps restant pour battre l'ennemi
    
    int nextAction;
    long int cooldown;
    SDL_Event event;
    SDL_bool on;
    on = SDL_TRUE;
    short int bomb = -1;
    
    cooldown = 1;

    while(on)
    {
        nextAction = rand()%100;                                                                        // On détermine l'action suivante selon les pourcentages
        if (cooldown >= 2)
        {
            cooldown = 1;
            if(action == POSER_BLOC)                                                                            // On met à jour l'action actuelle et on lance la fonction concernée
            {
                if(nextAction < 40)
                {
                    poseBloc(grille);
                }
                else if (nextAction < 80)
                {
                    action = DORMIR;
                }
                else
                {
                    pluieBombes(grille);
                    bomb = 0;
                    action = PLUIE_BOMBE;
                }
            }
            if(action == PLUIE_BOMBE)
            {
                if(nextAction < 50)
                {
                    poseBloc(grille);
                    action = POSER_BLOC;
                }
                else
                {
                    action = DORMIR;
                }
            }
            if(action == DORMIR)
            {
                if (nextAction > 20 && nextAction < 45)
                {
                    poseBloc(grille);
                    action = POSER_BLOC;
                }
                else
                {
                    pluieBombes(grille);
                    bomb = 0;
                    action = PLUIE_BOMBE;
                }
            }
        }

        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT:
                    on = SDL_FALSE;
                    break;

                case SDL_KEYDOWN:                                                                           // On a appuyé sur une touche
                    cooldown++;
                    if (bomb > -1 ) {bomb++;}
                    switch (event.key.keysym.sym)
                    {
                        case SDLK_DOWN:
                            posMii = animBas(renderer, grille, posMii,                                
                                            oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc,bombe,
                                            TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe);
                        break;
                        
                        case SDLK_UP:
                            posMii = animHaut(renderer, grille, posMii,                                
                                            oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc,bombe,
                                            TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe);
                        break;

                        case SDLK_LEFT:
                            posMii = animGauche(renderer, grille, posMii,                                
                                            oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc,bombe,
                                            TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe);
                        break;
                        
                        case SDLK_RIGHT:
                            posMii = animDroite(renderer, grille, posMii,                                
                                            oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc,bombe,
                                            TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe);
                        break;

                        case SDLK_RETURN:
                            on = SDL_FALSE;
                        break;

                        case SDLK_q: //détruit bloc à gauche
                            if (posMii.x != 0 && grille[posMii.y/66][(posMii.x - 66)/66].type == BLOC)
                            {
                                grille[posMii.y/66][(posMii.x - 66)/66].type = LIBRE;
                            }
                        break;

                        case SDLK_d: //détruit bloc à droite
                            if (posMii.x != 924 && grille[posMii.y/66][(posMii.x + 66)/66].type == BLOC)
                            {
                                grille[posMii.y/66][(posMii.x + 66)/66].type = LIBRE;
                            }
                        break;

                        case SDLK_z: //détruit bloc en haut
                            if (posMii.y != 0 && grille[(posMii.y - 66)/66][(posMii.x)/66].type == BLOC)
                            {
                                grille[(posMii.y - 66)/66][(posMii.x)/66].type = LIBRE;
                            }
                        break;

                        case SDLK_s: //détruit bloc en bas
                            if (posMii.y != 924 && grille[(posMii.y + 66)/66][(posMii.x )/66].type == BLOC)
                            {
                                grille[(posMii.y + 66)/66][(posMii.x )/66].type = LIBRE;
                            }
                        break;

                        default :
                        break;
                    }
                    break;
            }
        }

        if(posMii.y == 66 && (posMii.x == 462 || posMii.x == 528))
        {
           if(combat(window, renderer, grille, posMii, oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe, &temps))
           {
            on = SDL_FALSE;
           }
        }

        if(visageEntoure(grille))
        {
            on = SDL_FALSE;
            afficheMap(renderer, grille, posMii, oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe, 0);
            defaite(window, renderer, "Un visage est emprisonne...");
        }

        if(bomb == 1)
        {
            explosion (window, renderer, grille, posMii, oeilD, oeilG, visage1, 
                        visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD,
                        TextOeilG, TextVisages, TextBloc, TextBombe, &on);
        }

        if(bomb == 2)
        {
            enleveBombe(grille);
            bomb = -1;
        }

        afficheMap(renderer, grille, posMii, oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe, 0);
        
    }
}
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include "initialisation.h"

/* ------------------------------------------------------------------------------
Termine l'utilisation de la SDL
Paramètres : 	ok          - booléen indiquant si le programme doit se terminer
                            à cause d'une erreur ou de façon normale
				msg         - message à afficher si erreur
				window      - pointeur sur la fenêtre à supprimer
				renderer    - pointeur sur le SDL_Renderer à supprimer
------------------------------------------------------------------------------ */

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer)
{
  	char msg_formated[255];                                                         
  	int l;                                                                          

  	if (!ok)
	{                                                        												// Cas d'erreur
        strncpy(msg_formated, msg, 250);                                              
        l = strlen(msg_formated);                                                     
        strcpy(msg_formated + l, " : %s\n");                                          

        SDL_Log(msg_formated, SDL_GetError());                                        
  	}                                                                               

  	if (renderer != NULL)
	{                                           															// Destruction si nécessaire du renderer
    	SDL_DestroyRenderer(renderer);                     													// Attention : on suppose que les NULL sont maintenus !!
    	renderer = NULL;
  	}
  	if (window != NULL)  
	{                                           															// Destruction si nécessaire de la fenêtre
    	SDL_DestroyWindow(window);                          												// Attention : on suppose que les NULL sont maintenus !!
    	window= NULL;
  	}

    	SDL_Quit();                                                                     

  	if (!ok)
	{                                                        												// On quitte si cela ne va pas                
    	exit(EXIT_FAILURE);                                                           
  	}                                                                               
}




/* ------------------------------------------------------------------------------
Créé les SDL_Rect qui découpent la fenêtre en différents carreaux
Retourne le tableau à 2 dimensions de SDL_Rect
------------------------------------------------------------------------------ */

carreau ** creerGrille()
{
    carreau ** grille = (carreau**) malloc(15*sizeof(carreau*));
    int i, j;
    for(i=0;i<15;i++)                                                                                       // Le plateau est de taille 15*15
    {
        grille[i] = (carreau*) malloc(15*sizeof(carreau));
        for(j=0;j<15;j++)
        {
            grille[i][j].rect.x = j*66; grille[i][j].rect.y = i*66;                                         // Les cases seront des carrés de 66*66
            grille[i][j].rect.w = 66; grille[i][j].rect.h = 66;
            grille[i][j].type = LIBRE;                                                                      // Au début toutes les cases sont libres
        }
    }

    return grille;
}




/* ------------------------------------------------------------------------------
Remplit la grille avec les positions initiales des objets
Paramètres : 	renderer    - pointeur sur le renderer actif
				grille      - pointeur sur un tableau de SDL_Rect qui représentent
                            tous les carreaux existant sur la grille
				oeilD       - SDL_Rect représentant l'oeil droit de l'ennemi
				oeilG       - SDL_Rect représentant l'oeil gauche de l'ennemi
                visage1     - SDL_Rect représentant le 1er visage volé
                visage2     - SDL_Rect représentant le 2e visage volé
                visage3     - SDL_Rect représentant le 3e visage volé
                mii         - SDL_Rect représentant le Mii
                TextMii     - pointeur sur la texture contenant les sprites du mii
                TextOeilD   - pointeur sur la texture contenant les sprites de
                            l'oeil droit de l'ennemi
                TextOeilG   - pointeur sur la texture contenant les sprites de
                            l'oeil gauche de l'ennemi
                TextVisages - pointeur sur la texture contenant les sprites des
                            visages
------------------------------------------------------------------------------ */

void remplirGrille( SDL_Renderer* renderer,
                    carreau ** grille,
                    SDL_Rect oeilD,
                    SDL_Rect oeilG,
                    SDL_Rect visage1,
                    SDL_Rect visage2,
                    SDL_Rect visage3,
                    SDL_Rect mii,
                    SDL_Texture* TextMii,
                    SDL_Texture* TextOeilD,
                    SDL_Texture* TextOeilG,
                    SDL_Texture* TextVisages)
{
    SDL_RenderClear(renderer);

    // Placement de l'ennemi
    grille[0][7].type = MECHANT;
    SDL_RenderCopy(renderer, TextOeilG, &oeilG, &grille[0][7].rect); 

    grille[0][8].type = MECHANT;
    SDL_RenderCopy(renderer, TextOeilD, &oeilD, &grille[0][8].rect);
    
    //Placement de la barrière de l'ennemi
    grille[0][6].type = BLOC;
    grille[0][9].type = BLOC;
    grille[1][6].type = BLOC;
    grille[1][7].type = BLOC;
    grille[1][8].type = BLOC;
    grille[1][9].type = BLOC;
    
     
    // Placement des visages
    grille[5][2].type = VISAGE;
    SDL_RenderCopy(renderer, TextVisages, &visage1, &grille[5][2].rect);                                        

    grille[5][12].type = VISAGE;
    SDL_RenderCopy(renderer, TextVisages, &visage2, &grille[5][12].rect);

    grille[12][7].type = VISAGE;
    SDL_RenderCopy(renderer, TextVisages, &visage3, &grille[12][7].rect);
    
    
    
    // Placement du Mii
    grille[14][0].type = LIBRE;
    SDL_RenderCopy(renderer, TextMii, &mii, &grille[14][0].rect);                                              

    SDL_RenderPresent(renderer);
}




/* ------------------------------------------------------------------------------
Affiche pendant 5 secondes un écran d'explication des régles
Paramètres : 	renderer    - pointeur sur le renderer actif
				window      - pointeur sur la fenêtre active
				wwindow     - largeur de la fenêtre
                hwindow     - hauteur de la fenêtre
------------------------------------------------------------------------------ */

void affichageRegles(SDL_Renderer* renderer, SDL_Window* window, int wwindow, int hwindow)
{
    SDL_Rect pos,cadre;
    TTF_Font* font = TTF_OpenFont("./data/Nuvel.ttf", 25);
    if (font == NULL) {end_sdl(0, "Can't load font", window, renderer);}
    SDL_Color color = {255, 170, 0, 255};
    

    // ---------------1er texte--------------- 

    SDL_Surface* texte = TTF_RenderText_Blended(font, "Bienvenue dans Face Thief !", color);
    if (texte == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, texte);                              // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.01*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);
    cadre.x = cadre.y = 0; cadre.w = wwindow;
    cadre.h = 2*pos.h; 
    SDL_SetRenderDrawColor(renderer,255,255,255,255);
    
    SDL_RenderDrawRect(renderer,&cadre);

    //---------------2e texte--------------- 
    color.r = 255; color.g = 255; color.b = 255;
    texte = TTF_RenderText_Blended(font, "Mechant a vole tous les visages des Mii !!!", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.2*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);

    //---------------3e texte--------------- 
    
    texte = TTF_RenderText_Blended(font, "Orange doit detruire Mechant pour recuperer les visages.", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.3*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);

    //---------------4e texte--------------- 
    
    texte = TTF_RenderText_Blended(font, "Attention ! Mechant ne se laissera pas faire et fera pleuvoir des bombes !", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.4*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);

    //---------------5e texte--------------- 

    texte = TTF_RenderText_Blended(font, "Detruis les blocs que Mechant place autour des visages et bats le !", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.5*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);

    //---------------6e texte--------------- 

    texte = TTF_RenderText_Blended(font, "Utilise les fleches directionnelles pour te deplacer", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.6*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);

    //---------------7e texte--------------- 

    texte = TTF_RenderText_Blended(font, "Tu peux detruire les blocs poses par Mechant avec q,z,d,s", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.7*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);

    //---------------8e texte--------------- 

    color.r = 150; color.g = 0; color.b = 0;
    texte = TTF_RenderText_Blended(font, "Pret ? Alors lance la partie en appuyant sur espace", color);
    text_texture = SDL_CreateTextureFromSurface(renderer, texte);
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow - pos.w); pos.y = 0.8*hwindow;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);


    //--Debut de la boucle évènementielle--

    SDL_bool run = SDL_TRUE;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);
    
    SDL_RenderPresent(renderer);
    while(run)
    {
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT :
                    run = SDL_FALSE;
                break;
                case SDL_KEYDOWN :

                    if (event.key.keysym.sym == 13) 
                    {
                        run = SDL_FALSE;
                    }
                    
                break;

                default :
                break;    
            }
        }
    }

}




/* ------------------------------------------------------------------------------
Créé le tableau de SDL_Rect contenant tous les sprites du Mii
------------------------------------------------------------------------------ */

SDL_Rect* creerTableauMii()
{
    SDL_Rect* tab = (SDL_Rect*) malloc(12*sizeof(SDL_Rect));
    
    //Rectangles Animation Haut/Descendre
    tab[0].x = 145; tab[0].y = 435;
    tab[0].w = 59; tab[0].h = 105;

    tab[1].x = 500; tab[1].y = 430;
    tab[1].w = 59; tab[1].h = 105;

    //Rectangles Animation Droite
    tab[2].x = 19; tab[2].y = 215;
    tab[2].w = 59; tab[2].h = 108;

    tab[3].x = 16; tab[3].y = 322;
    tab[3].w = 59; tab[3].h = 110;

    tab[4].x = 75; tab[4].y = 215;
    tab[4].w = 59; tab[4].h = 110;

    tab[5].x = 78; tab[5].y = 325;
    tab[5].w = 59; tab[5].h = 107;

    //Rectangles Animation Gauche
    tab[6].x = 445; tab[6].y = 106;
    tab[6].w = 59; tab[6].h = 110;

    tab[7].x = 508; tab[7].y = 0;
    tab[7].w = 59; tab[7].h = 105;

    tab[8].x = 436; tab[8].y = 0;
    tab[8].w = 59; tab[8].h = 105;

    tab[9].x = 502; tab[9].y = 108;
    tab[9].w = 70; tab[9].h = 105;
    


    return tab;
}




void victoire(SDL_Window* window, SDL_Renderer* renderer)
{
    SDL_Rect pos;
    int wwindow, hwindow;
    SDL_GetWindowSize(window, &wwindow, &hwindow);
    TTF_Font* font = TTF_OpenFont("./data/Nuvel.ttf", 40);
    if (font == NULL) {end_sdl(0, "Can't load font", window, renderer);}
    SDL_Color color = {255, 255, 255, 255};
    

    // ---------------1er texte--------------- 

    SDL_Surface* texte = TTF_RenderText_Blended(font, "Bravo, vous avez vaincu Mechant !", color);
    if (texte == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, texte);                              // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 0.5*(wwindow-pos.w); pos.y = 0.5*(hwindow - pos.h);
    
    SDL_bool run = SDL_TRUE;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);
    SDL_RenderPresent(renderer);
    while(run)
    {
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT :
                    run = SDL_FALSE;
                break;
                case SDL_KEYDOWN :

                    if (event.key.keysym.sym == 13) 
                    {
                        run = SDL_FALSE;
                    }
                    
                break;

                default :
                break;    
            }
        }
    }
}

void defaite(SDL_Window* window, SDL_Renderer* renderer, char * msg)
{
    SDL_bool run = SDL_TRUE;
    SDL_Rect pos;
    TTF_Font* font = TTF_OpenFont("./data/Nuvel.ttf", 40);
    if (font == NULL) {end_sdl(0, "Can't load font", window, renderer);}
    SDL_Color color = {255, 255, 255, 255};
    

    // ---------------1er texte--------------- 

    SDL_Surface* texte = TTF_RenderText_Blended(font, msg, color);
    if (texte == NULL) end_sdl(0, "Can't create text surface", window, renderer);

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, texte);                              // transfert de la surface à la texture
    if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
    
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    pos.x = 100; pos.y = 450;
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);
    
    SDL_RenderPresent(renderer);
    while(run)
    {
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT :
                    run = SDL_FALSE;
                break;
                case SDL_KEYDOWN :

                    if (event.key.keysym.sym == 13) 
                    {
                        run = SDL_FALSE;
                    }
                    
                break;

                default :
                break;    
            }
        }
    }
}
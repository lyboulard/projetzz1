#ifndef JEU_H
#define JEU_H

#include "initialisation.h"

enum actions {POSER_BLOC, PLUIE_BOMBE, DORMIR};

void afficheMap(SDL_Renderer*, carreau**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect [], SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture *, SDL_Texture *, int);
SDL_Rect animBas(SDL_Renderer*, carreau**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect [], SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*);
SDL_Rect animHaut(SDL_Renderer*, carreau**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect [], SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture *);
SDL_Rect animGauche(SDL_Renderer*, carreau**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect [], SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture *);
SDL_Rect animDroite(SDL_Renderer*, carreau**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect [], SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture *);
int combat(SDL_Window*, SDL_Renderer*, carreau**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect [], SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture *, SDL_Texture *, int*);
SDL_bool visageEntoure (carreau ** );
void jeu(SDL_Window*, SDL_Renderer*, carreau**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect[], SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture *, SDL_Texture *, int);

void pluieBombes(carreau **);
void explosion(SDL_Window *, SDL_Renderer*, carreau**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect [], SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture *, SDL_Texture *, SDL_bool *);
void enleveBombe (carreau ** );

#endif
/* ------------------------------------------------------------------------------

Projet ZZ1 - ISIMA - Juin 2022
    - Chef d'oeuvre
    Jeu implémentant des chaînes de Markov : un personnage évoluant sur une grille
    doit détruire l'antagoniste. Celui-ci cherche à entourer les visages volés
    disposés sur la grille avec des blocs qui peuvent être cassés par le
    protagoniste. Des pluies de bombes tombent parfois sur la grille. Le jeu se
    gagne quand l'ennemi est mort ; il se perd si les trésors sont tous entourés
    de blocs ou si une bombe tombe sur le personnage.
Lyloo Boulard - Arien Mungongo - Paul Marcelet

------------------------------------------------------------------------------ */

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <time.h>
#include "initialisation.h"
#include "jeu.h"

int main(int argc, char** argv)
{
    (void)argc;
    (void)argv;

    srand(time(NULL));

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    int wwindow; int hwindow;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer); 						// Initialisation de la SDL
    if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);

  	/* ---------------Création de la fenêtre--------------- */

  	window = SDL_CreateWindow("Fenêtre",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 990,
                            990,
                            SDL_WINDOW_OPENGL);
  	if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    SDL_GetWindowSize(window, &wwindow, &hwindow);

  	/* ---------------Création du renderer--------------- */

  	renderer = SDL_CreateRenderer(window, -1,
                                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  	if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    SDL_SetRenderDrawColor(renderer, 0, 200, 0, 255);


    /* ---------------Chargement des textures--------------- */

    SDL_Texture* TextMii = IMG_LoadTexture(renderer, "./data/Mii.png");
    if(TextMii == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }
    SDL_Texture* TextOeilD = IMG_LoadTexture(renderer, "./data/YeuxD.png");
    if(TextOeilD == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }
    SDL_Texture* TextOeilG = IMG_LoadTexture(renderer, "./data/YeuxG.png");
    if(TextOeilG == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }
    SDL_Texture* TextBloc = IMG_LoadTexture(renderer, "./data/Bloc.png");
    if(TextBloc == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }
    SDL_Texture* TextVisages = IMG_LoadTexture(renderer, "./data/Visages.png");
    if(TextVisages == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }
    SDL_Texture* TextBombe = IMG_LoadTexture(renderer, "./data/Bombe.png");
    if(TextBombe == NULL)
    {
        end_sdl(0, "Echec de chargement d'image", window, renderer);
    }

    carreau** grille = creerGrille();
    affichageRegles(renderer, window, wwindow, hwindow);
    
    SDL_Rect* tabMii = creerTableauMii();
    
    SDL_Rect oeilD, oeilG, bloc, visage1, visage2, visage3, bombe, mii;
    oeilD.x = 220; oeilD.y = 370;
    oeilD.w = 40; oeilD.h = 30;
    oeilG.x = 65; oeilG.y = 370;
    oeilG.w = 40; oeilG.h = 30;
    bloc.x = 1020; bloc.y = 63;
    bloc.w = 68; bloc.h = 68;
    visage1.x = 5; visage1.y = 60;
    visage1.w = 50; visage1.h = 50;
    visage2.x = 5; visage2.y = 125;
    visage2.w = 50; visage2.h = 50;
    visage3.x = 5; visage3.y = 185;
    visage3.w = 50; visage3.h = 50;
    bombe.x = 250; bombe.y = 37;
    bombe.w = 50; bombe.h = 50;
    mii.x = 0; mii.y = 107;
    mii.w = 67; mii.h = 106;


    SDL_Rect posMii; // Position actuelle du personnage
    posMii.x = 0; posMii.y = 924;
    posMii.w = 66; posMii.h = 66;

    int action = DORMIR;
    

    remplirGrille(renderer, grille, oeilD, oeilG, visage1, visage2, visage3, mii, TextMii, TextOeilD, TextOeilG, TextVisages);

    jeu(window, renderer, grille, posMii, oeilD, oeilG, visage1, visage2, visage3, tabMii, bloc, bombe, TextMii, TextOeilD, TextOeilG, TextVisages, TextBloc, TextBombe, action);

    TTF_Quit();
    end_sdl(1, "Terminaison normale", window, renderer);

    return EXIT_SUCCESS;
}
#ifndef INITIALISATION_H
#define INITIALISATION_H

enum type {LIBRE, BLOC, BLOC_LIBRE, VISAGE, MECHANT, PREBOMBE, BOMBE};

typedef struct carreau
{
    SDL_Rect rect;
    short int  type;
}carreau;

void end_sdl(char, char const*, SDL_Window*, SDL_Renderer*);
carreau** creerGrille();
SDL_Rect* creerTableauMii();
void remplirGrille(SDL_Renderer*, carreau**, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Rect, SDL_Texture*, SDL_Texture*, SDL_Texture*, SDL_Texture*);
void affichageRegles(SDL_Renderer*, SDL_Window*, int, int);
void victoire(SDL_Window*, SDL_Renderer*);
void defaite(SDL_Window*, SDL_Renderer*, char*);

#endif